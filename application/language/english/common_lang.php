<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['Slogan'] = 'Our success is in the quality of products!';
$lang['AccountLink'] = 'Personal Account';
$lang['Logout'] = 'Logout';
$lang['AllreadySubscribed'] = 'Email allready subscribed';
$lang['SubscribeSuccess'] = 'Email subscribed successfully';
$lang['Catalog'] = 'Catalog';
$lang['UserAccount'] = 'Account';
$lang['Users'] = 'Users';
$lang['NotConfirmed'] = 'Not Confirmed';
$lang['RegDate'] = 'Registration Date';
$lang['DataSaved'] = 'Data Saved';
$lang['Managers'] = 'Managers';

$lang['Menu'] = 'Menu';
$lang['Menus'] = 'Menus';
$lang['MenuItems'] = 'Menu items';
$lang['Feedback'] = 'Feedback';
$lang['TopRoutes'] = 'Top Routes';

$lang['UserEmail'] = 'User E-mail';
$lang['Password'] = 'Password';
$lang['ConfirmationPassword'] = 'Confirm Password';
$lang['Login'] = 'Login';
$lang['LostPassword'] = 'Lost Password?';
$lang['Email'] = 'E-mail';
$lang['Send'] = 'Send';
$lang['PasswordRecovery'] = 'Password Recovery';
$lang['PasswordRecoveryInfo'] = 'Enter e-mail address indicated by registration';
$lang['InvalidLogin'] = 'Email or password is wrong';
$lang['FeedbackSended'] = 'Feedback sent';

$lang['Registration'] = 'Registration';
$lang['PasswordConfirmation'] = 'Confirm Password';
$lang['ClientType'] = 'Client type';
$lang['Retail'] = 'Retail';
$lang['Angro'] = 'Wholesale';
$lang['SelectClientType'] = 'Select client type';
$lang['EmailAllreadyExist'] = 'Email address is already registered';
$lang['WaitConfirmation'] = 'Wait for account confirmation';
$lang['WaitConfirmationText'] = 'Wait for confirmation from the admin account.';

$lang['RecoveryMessageSubject'] = 'Recovering password www.agm.md';
$lang['EmailNotRegistered'] = 'E-mail you entered was not registered';
$lang['ResetPassword'] = 'Password reset';
$lang['RecoveryToken'] = 'Recovery Token';
$lang['InvalidRecoveryToken'] = 'Invalid Recovery Token';
$lang['LoginWithNewPassword'] = 'The password has been changed successfully. Login using your new password.';
$lang['WishRegEnterPassword'] = 'If you want register, enter the password';

$lang['IDNO'] = 'IDNO';
$lang['BankName'] = 'Bank Name';
$lang['IBAN'] = 'IBAN';
$lang['Accept'] = 'Accept';
$lang['Client'] = 'Client';
$lang['Transporter'] = 'Transporter';
$lang['Terms'] = 'Terms & Conditions';
$lang['SelectCity'] = 'Select City';


$lang['AdminAccount'] = 'Admin Account';
$lang['ProfileSettings'] = 'Profile Settings';
$lang['Wellcome'] = 'Welcome';

$lang['ConfirmCategoryDelete'] = 'Category will be deleted with subcategories and its products!';
$lang['CategoryName'] = 'Name of Category';
$lang['Categories'] = 'Categories';
$lang['AddCategory'] = 'Add Category';
$lang['AddContent'] = 'Add Content';

$lang['CategoryTitle'] = 'Category Title';
$lang['CategoryText'] = 'Category Text';
$lang['Keywords'] = 'Keywords';
$lang['Description'] = 'Description';
$lang['Save'] = 'Save';
$lang['CategoryParent'] = 'Parent category';
$lang['InvalidForm'] = 'Form is invalid. See errors.';
$lang['CategorySaved'] = 'Category Saved';
$lang['CategoryImage'] = 'Category Image';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Category Status';
$lang['CatStatusActive'] = 'Active';
$lang['CatStatusDisabled'] = 'Disabled';
$lang['ShowIn'] = 'Show In';
$lang['ShowInAll'] = 'All';
$lang['ShowInAngro'] = 'Wholesale';
$lang['ShowInRetail'] = 'Retail';
$lang['CategoryDeleted'] = 'Category Deleted';

$lang['Filters'] = 'Filters';
$lang['AddFilter'] = 'Save filter';
$lang['FilterName'] = 'Filter Name';
$lang['FilterType'] = 'Filter Type';
$lang['String'] = 'String';
$lang['Number'] = 'Number';
$lang['FilterSaved'] = 'Filter Saved';
$lang['DeleteFilter'] = 'Delete Filter?';
$lang['FilterDeleted'] = 'Filter Deleted';

$lang['Products'] = 'Products';
$lang['AddProduct'] = 'Add Product';
$lang['ProductName'] = 'Product Name';
$lang['Price'] = 'Price';
$lang['Sorting'] = 'Sorting';
$lang['IsPromo'] = 'Promo';
$lang['IsPromoUpper'] = 'PROMO';
$lang['Filter'] = 'Filter';
$lang['SKU'] = 'SKU';
$lang['Stock'] = 'Stock';
$lang['Image'] = 'Image';
$lang['GeneralData'] = 'General Data';
$lang['Images'] = 'Images';
$lang['Link'] = 'Link';
$lang['ImagePhoto'] = 'Image Photo';
$lang['GeneralPhoto'] = 'General Photo';
$lang['PriceAndStock'] = 'Price And Stock';
$lang['EditProduct'] = 'Edit Product';
$lang['ProductCategory'] = 'Product Category';
$lang['DiscountPrice'] = 'Discount Price';
$lang['DiscountPriceStart'] = 'Valid from date';
$lang['DiscountPriceEnd'] = 'Available up to date';
$lang['HasPromo'] = 'Promo ';
$lang['AddImages'] = 'Add Images';
$lang['ProductStatus'] = 'Product Status';
$lang['ProdStatusActive'] = 'Active';
$lang['ProdStatusDisabled'] = 'Disabled';
$lang['ProductSaved'] = 'Product Saved';
$lang['ClearFilters'] = 'Clear Filters';
$lang['AddProductFilter'] = 'Add Filter';
$lang['FilterName'] = 'Filter';
$lang['FilterAllreadySelected'] = 'Filter Allready Selected';
$lang['DeleteSmth'] = 'Delete';
$lang['ConfirmSmth'] = 'Confirm?';
$lang['NoImageUploaded'] = 'No Photos Uploaded';
$lang['NoFileUploaded'] = 'No files uploaded';

$lang['Slider'] = 'Slider';
$lang['AddPromotion'] = 'Add promotion';
$lang['PromotionType'] = 'Promotion type';
$lang['Value'] = 'Value';
$lang['Clear'] = 'Clear';



$lang['News'] = 'News';
$lang['NewsUpperCase'] = 'NEWS';
$lang['AddNews'] = 'Add news';
$lang['Title'] = 'Title';
$lang['Text'] = 'Text';
$lang['Date'] = 'Date';
$lang['Status'] = 'Status';
$lang['NewsSaved'] = 'News Saved';
$lang['FileName'] = 'File Name';
$lang['SeeAllNews'] = 'See all news';

$lang['Pages'] = 'Pages';
$lang['AddPage'] = 'Add Page';
$lang['PageSaved'] = 'Page Saved';

$lang['HomePage'] = 'Home Page';
$lang['buy'] = 'Buy';
$lang['AddedToCart'] = 'Added to cart';
$lang['OutOfStock'] = 'Out Of Stock';
$lang['UserInfo'] = 'User Info';
$lang['CartChanged'] = 'Cart changed';
$lang['CartProductDeleted'] = 'Cart product deleted';
$lang['CartEmpty'] = 'Cart empty';
$lang['CartTotal'] = 'Total';
$lang['ShoppingCart'] = 'Shopping Cart';
$lang['ContinueShopping'] = 'Continue Shopping';
$lang['Region'] = 'Region';
$lang['Address'] = 'Address';
$lang['Name'] = 'Name';
$lang['BackToCart'] = 'Back To Cart';
$lang['PaymentType'] = 'Payment Type';
$lang['CardPayment'] = 'Card Payment';
$lang['CashPayment'] = 'Cash Payment';
$lang['Zip'] = 'Zip / Postal Code';
$lang['Phone'] = 'Phone';
$lang['CompanyName'] = 'Company Name';
$lang['ContactPerson'] = 'Contact Person';
$lang['Activitation'] = 'Sphere of activity';
$lang['Files'] = 'Files (documents attached)';
$lang['Thankyou'] = 'Thank you';
$lang['ThankyouTitle'] = 'Thanks for your order';
$lang['ThankyouText'] = 'Your order has been created. Our manager will be contacted with you soon.';
$lang['ThankyouText2'] = 'Your order ID #';
$lang['DownloadInvoice'] = 'Download bill';

$lang['Dashboard'] = 'Dashboard';
$lang['OrderHistory'] = 'Order History';
$lang['Amount'] = 'Amount';
$lang['AccountBalance'] = 'Account Balance';

$lang['Order'] = 'Order';
$lang['Orders'] = 'Orders';
$lang['SeeOrder'] = 'See Order';
$lang['SeeAllOrders'] = 'See all orders';
$lang['TotalOrders'] = 'Total orders';//Всего заказов
$lang['NewOrders'] = 'New orders';//Всего заказов
$lang['Finance'] = 'Finance';
$lang['RequestForWithdrawal'] = 'Request for withdrawal';


$lang['BackToList'] = 'BACK TO LIST';
$lang['OrderDelails'] = 'Order Delails';
$lang['OrderProducts'] = 'Order Products';
$lang['Quantity'] = 'Quantity';


$lang['Settings'] = 'Settings';
$lang['AddToCart'] = 'To Cart';
$lang['Checkout'] = 'Checkout';
$lang['CurrencyName'] = 'LEI' ;
$lang['CurrencyNameLowerCase'] = 'lei' ;
$lang['ArticlesPerPage'] = 'Articles Per Page';
$lang['SortBy'] = 'Sort by';
$lang['Date'] = 'Date';
$lang['PlaceOrder'] = 'Place Order';
$lang['SeeAll'] = 'See all';
$lang['NewProducts'] = 'New Products';
$lang['NewLabel'] = 'New!';
$lang['ContentAdditional'] = 'Additional Content';
$lang['Country'] = 'Country';
$lang['City'] = 'City';
$lang['NotExistsCity'] = 'None city';

$lang['WhereFrom'] = 'Wherefrom';
$lang['Whereto'] = 'Whereto';
$lang['Departure'] = 'Departure';
$lang['Arrival'] = 'Arrival';
$lang['Adults'] = 'Adults';
$lang['Kids'] = 'Kids';



$lang['Route'] = 'Route';//Маршрут
$lang['RoutesPopular'] = 'The most popular directions';
$lang['EditRoute'] = 'Edit route';//Редактирование маршрута
$lang['SearchRouteAuto'] = 'Search for routes';
$lang['Search'] = 'Search';


$lang['Transport'] = 'Transport';
$lang['Cost'] = 'Cost';

$lang['AddStation'] = 'Add Station';//Добавить остановку
$lang['Close'] = 'Close';
$lang['Message'] = 'Message';
$lang['DeleteFeedbackQ'] = 'Delete feedback?';
$lang['LastTranzactions'] = 'Latest transactions';

$lang['Carriers'] = 'Сarriers';
$lang['LastCarriers'] = 'Last carriers';
$lang['SeeAllCarriers'] = 'View all carriers';
$lang['JPGorPNG'] = '( JPG or PNG )';
$lang['SortSaved'] = 'Sort Saved';
$lang['LastOrders'] = 'Last orders';
$lang['OutputOfResults'] = 'Displaying';
$lang['Results'] = 'results';
$lang['Seats'] = 'Seats';
$lang['SeatsLower'] = 'seats';
$lang['PersonalData'] = 'Personal Data';
$lang['FirstNLastName'] = 'Firstname, Lastname';
$lang['ChooseYourSeat'] = 'Choose your seat';
$lang['Add'] = 'Add';
$lang['SendEmail'] = 'Send е-mail';

$lang['CityFrom'] = 'City From';
$lang['CityTo'] = 'City To';

















