<?php defined('BASEPATH') OR exit('No direct script access allowed');


// HEADER
$lang['MyCabinet'] = 'Мой <br />Кабинет';
$lang['SignIn'] = 'Вход';

// FOOTER
$lang['Informations'] = 'Информация';
$lang['Services'] = 'Услуги';
$lang['Partners'] = 'Партнеры';
$lang['Support'] = 'Поддержка';
$lang['Socials'] = 'Соц. сети';
$lang['WeAccept'] = 'Принимаем';


// SEARCH FORM
$lang['SearchFormText'] = 'Электронный билет на международный автобус';

// SEARCH DETAILS
$lang['Company'] = 'Компания';
$lang['Bus'] = 'Автобус';
$lang['ViewTransporterPage'] = 'посмотреть страницу перевозчика';

// Qualities
$lang['VerifiedTransporters'] = 'Проверенные перевозчики';
$lang['BestPrices'] = 'Саммы выгодные цены';
$lang['BusConfortable'] = 'Комфортабельные автобусы';
$lang['Support24'] = 'Поддержка 24/7';

// News
$lang['LastNews'] = 'Последние новости';

// Home Page
$lang['HomeTitle'] = 'Международные автобусы';
$lang['HomeThanks'] = 'Спасибо что выбрали нас!';

// Modals
$lang['Close'] = 'Закрыть';
$lang['SignIn'] = 'Вход';
$lang['SignIn'] = 'Вход';





$lang['Slogan'] = 'Наш успех в качестве нашей продукции!';
$lang['AccountLink'] = 'Личный кабинет';
$lang['Logout'] = 'Выход';
$lang['AllreadySubscribed'] = 'Электронный адрес уже подписан.Введите другой email';
$lang['SubscribeSuccess'] = 'Подписка оформлена';
$lang['Catalog'] = 'Каталог';
$lang['UserAccount'] = 'Аккаунт';
$lang['Users'] = 'Пользователи';
$lang['NotConfirmed'] = 'Не подтвержден(а)';
$lang['RegDate'] = 'Дата регистрации';
$lang['DataSaved'] = 'Данные сохранены';
$lang['Manager'] = 'Менеджер';
$lang['Managers'] = 'Менеджеры';

$lang['Menu'] = 'Меню';
$lang['Menus'] = 'Меню';
$lang['MenuItems'] = 'Пункты меню';
$lang['Seo'] = 'SEO';
$lang['Feedback'] = 'Feedback';
$lang['Content'] = 'Контент';
$lang['TopRoutes'] = 'Топ Маршруты';

$lang['UserEmail'] = 'E-mail пользователя';
$lang['Password'] = 'Пароль';
$lang['ConfirmationPassword'] = 'Подтвердите пароль';
$lang['Login'] = 'Logare';
$lang['LostPassword'] = 'Забыли пароль?';
$lang['Email'] = 'Адрес e-mail';
$lang['Send'] = 'Отправить';
$lang['PasswordRecovery'] = 'Восстановление пароля';
$lang['PasswordRecoveryInfo'] = 'Введите адрес e-mail указанный при регистрации';
$lang['InvalidLogin'] = 'Email или пароль введены неверно';
$lang['FeedbackSended'] = 'Feedback отправлен';

$lang['Registration'] = 'Регистрация';
$lang['PasswordConfirmation'] = 'Подтвердите пароль';
$lang['ClientType'] = 'Тип клиента';
$lang['Retail'] = 'Розница';
$lang['Angro'] = 'Оптом';
$lang['SelectClientType'] = 'Выберите тип клиента';
$lang['SelectPaymentType'] = 'Выберите способ оплаты';
$lang['EmailAllreadyExist'] = 'Адрес email уже зарегистрирован';
$lang['WaitConfirmation'] = 'Дождитесь подтверждения счета';
$lang['WaitConfirmationText'] = 'Дождитесь подтверждения со стороны администратора.';

$lang['RecoveryMessageSubject'] = 'Восстановление пароля!';
$lang['EmailNotRegistered'] = 'Адрес e-mail не был зарегистрирован';
$lang['ResetPassword'] = 'Сбросить пароль';
$lang['RecoveryToken'] = 'Код для сброса пароля';
$lang['InvalidRecoveryToken'] = 'Не верный код сброса пароля';
$lang['LoginWithNewPassword'] = 'Пароль был успешно изменен. Можете войти с его помощью.';
$lang['WishRegEnterPassword'] = 'Если хотите зарегистрироваться, введите пароль';

$lang['IDNO'] = 'IDNO';
$lang['BankName'] = 'Название Банка';
$lang['IBAN'] = 'IBAN';
$lang['Accept'] = 'Принимать';
$lang['Client'] = 'Клиент';
$lang['Transporter'] = 'Перевозчик';
$lang['Terms'] = 'Правила и Условия';
$lang['SelectCity'] = 'Выберите Город';

$lang['AdminAccount'] = 'Аккаунт администратора';
$lang['ProfileSettings'] = 'Настройки профиля';
$lang['Wellcome'] = 'Добро пожаловать';

$lang['ConfirmCategoryDelete'] = 'Категория будет удалена вместе с находящимися в ней продуктами и подкатегориями!';
$lang['CategoryName'] = 'Название категории';
$lang['Categories'] = 'Категории';
$lang['AddCategory'] = 'Добавьте категорию';
$lang['AddContent'] = 'Добавьте содержание';

$lang['CategoryTitle'] = 'Заголовок категории';
$lang['CategoryText'] = 'Текст категории';
$lang['Keywords'] = 'Ключевые слова';
$lang['Description'] = 'Описание';
$lang['Save'] = 'Сохранить';
$lang['CategoryParent'] = 'Родительская категория';
$lang['InvalidForm'] = 'Неверно заполнена форма.см. ошибки';
$lang['CategorySaved'] = 'Категория была сохранена';
$lang['CategoryImage'] = 'Изображение категории';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Статус категории';
$lang['CatStatusActive'] = 'Активен(а)';
$lang['CatStatusDisabled'] = 'Неактивен(а)';
$lang['ShowIn'] = 'Показать в';
$lang['ShowInAll'] = 'Все';
$lang['ShowInAngro'] = 'Оптовые';
$lang['ShowInRetail'] = 'Розничные';
$lang['CategoryDeleted'] = 'Категория удалена';

$lang['Filters'] = 'Фильтры';
$lang['AddFilter'] = 'Сохранить фильтр';
$lang['FilterName'] = 'Название фильтра';
$lang['FilterType'] = 'Тип фильтра';
$lang['String'] = 'строка';
$lang['Number'] = 'число';
$lang['FilterSaved'] = 'Фильтр сохранен';
$lang['DeleteFilter'] = 'Удалить фильтр?';
$lang['FilterDeleted'] = 'Фильтр удален';

$lang['Products'] = 'Продукты';
$lang['AddProduct'] = 'Добавить продукт';
$lang['ProductName'] = 'Название продукта';
$lang['Price'] = 'Цена';
$lang['Sorting'] = 'Сортировка';
$lang['IsPromo'] = 'Promo';
$lang['IsPromoUpper'] = 'PROMO';
$lang['Filter'] = 'Фильтровать';
$lang['SKU'] = 'Учетный идентификатор';
$lang['Stock'] = 'Остаток';
$lang['Image'] = 'Изображение';
$lang['GeneralData'] = 'Общие данные';
$lang['Images'] = 'Изображения';
$lang['Link'] = 'Ссылка';
$lang['ImagePhoto'] = 'Изображение';
$lang['GeneralPhoto'] = 'Главное изображение';
$lang['PriceAndStock'] = 'Цены и наличие';
$lang['EditProduct'] = 'Редактирование продукта';
$lang['ProductCategory'] = 'Категория продукта';
$lang['DiscountPrice'] = 'Скидочная цена';
$lang['DiscountPriceStart'] = 'Доступен от';
$lang['DiscountPriceEnd'] = 'Доступен до';
$lang['HasPromo'] = 'Promo ';
$lang['AddImages'] = 'Добавить изображение';
$lang['ProductStatus'] = 'Состояние продукта';
$lang['ProdStatusActive'] = 'Активен(а)';
$lang['ProdStatusDisabled'] = 'Неактивен(а)';
$lang['ProductSaved'] = 'Продукт сохранен';
$lang['ClearFilters'] = 'Сбросить фильтры';
$lang['AddProductFilter'] = 'Добавить фильтр';
$lang['FilterName'] = 'Фильтр';
$lang['FilterAllreadySelected'] = 'Фильтр уже был выбран';
$lang['DeleteSmth'] = 'Удалить';
$lang['ConfirmSmth'] = 'Подтвердить?';
$lang['NoImageUploaded'] = 'Изображений нет';
$lang['NoFileUploaded'] = 'Нет файлов';

$lang['Slider'] = 'Слайдер';

$lang['AddPromotion'] = 'Добавить акцию';
$lang['PromotionType'] = 'Тип акции';
$lang['Value'] = 'Значение';
$lang['Clear'] = 'Сбросить';



$lang['News'] = 'Новости';
$lang['NewsUpperCase'] = 'НОВОСТИ';
$lang['AddNews'] = 'Добавить новость';
$lang['Title'] = 'Заголовок';
$lang['Text'] = 'Текст';
$lang['Date'] = 'Дата';
$lang['Status'] = 'Состояние';
$lang['NewsSaved'] = 'Новость сохранена';
$lang['FileName'] = 'Название файла';
$lang['SeeAllNews'] = 'Смотреть все новости';

$lang['Pages'] = 'Страницы';
$lang['AddPage'] = 'Добавить страницу';
$lang['PageSaved'] = 'Страница сохранена';

$lang['HomePage'] = 'Главная';
$lang['buy'] = 'Купить';
$lang['AddedToCart'] = 'Добавлено в корзину';
$lang['OutOfStock'] = 'Нет в наличии';
$lang['UserInfo'] = 'О пользователе';
$lang['CartChanged'] = 'Корзина изменена';
$lang['CartProductDeleted'] = 'Продукт удален из корзины';
$lang['CartEmpty'] = 'Корзина пуста';
$lang['CartTotal'] = 'Итого';
$lang['ShoppingCart'] = 'Корзина';
$lang['ContinueShopping'] = 'Продолжить покупки';
$lang['Region'] = 'Регион';
$lang['Address'] = 'Адрес';
$lang['Name'] = 'Имя';
$lang['FamilyName'] = 'Фамилия';

$lang['BackToCart'] = 'Назад к корзине';
$lang['PaymentType'] = 'Тип оплаты';
$lang['CardPayment'] = 'Оплата картой';
$lang['CashPayment'] = 'Оплата наличными';
$lang['Zip'] = 'Zip / Почтовый индекс';
$lang['Phone'] = 'Телефон';
$lang['CompanyName'] = 'Название компании';
$lang['ContactPerson'] = 'Контактное лицо';
$lang['Activitation'] = 'Сфера деятельности';
$lang['Files'] = 'Файлы (для приложения)';
$lang['Thankyou'] = 'Благодарим Вас!';
$lang['ThankyouTitle'] = 'Благодарим Вас за покупку!';
$lang['ThankyouText'] = 'Ваш заказ был принят. Менеджер скоро с Вами свяжется.';
$lang['ThankyouText2'] = 'ID Вашего заказа #';
$lang['DownloadInvoice'] = 'Скачать квитанцию';

$lang['Dashboard'] = 'Панель';
$lang['OrderHistory'] = 'История заказа';
$lang['Amount'] = 'Сумма';
$lang['AccountBalance'] = 'Баланс счета';

$lang['Order'] = 'Заказ';
$lang['Orders'] = 'Заказы';
$lang['SeeOrder'] = 'Посмотреть заказ';
$lang['SeeAllOrders'] = 'Смотреть все заказы';
$lang['TotalOrders'] = 'Всего заказов';
$lang['NewOrders'] = 'Новые заказы';
$lang['Finance'] = 'Финансы';
$lang['RequestForWithdrawal'] = 'Запрос на снятие средств';


$lang['BackToList'] = 'НАЗАД К СПИСКУ';
$lang['OrderDelails'] = 'О заказе';
$lang['OrderProducts'] = 'Заказ продуктов';
$lang['Quantity'] = 'Количество';


$lang['Settings'] = 'Настройки';
$lang['AddToCart'] = 'В корзину';
$lang['Checkout'] = 'Оплата';
$lang['CurrencyName'] = 'ЛЕЕВ' ;
$lang['CurrencyNameLowerCase'] = 'леев' ;
$lang['ArticlesPerPage'] = 'Статей на странице';
$lang['SortBy'] = 'Сортировка по';
$lang['Date'] = 'Дата';
$lang['PlaceOrder'] = 'Порядок';
$lang['SeeAll'] = 'смотреть все';
$lang['NewProducts'] = 'НОВЫЕ ПРОДУКТЫ';
$lang['NewLabel'] = 'Новый!';
$lang['ContentAdditional'] = 'Дополнительное содержание';
$lang['Country'] = 'Страна';
$lang['City'] = 'Город';
$lang['NotExistsCity'] = 'Нет города';

$lang['WhereFrom'] = 'Откуда';
$lang['Whereto'] = 'Куда';
$lang['Departure'] = 'Выезд';
$lang['Arrival'] = 'Прибытие';
$lang['Adult'] = 'Взрослый';
$lang['Adults'] = 'Взрослых';
$lang['Kids'] = 'Детей';
$lang['Child'] = 'Ребенок';



$lang['Route'] = 'Маршрут';
$lang['RoutesPopular'] = 'Самые популярные направления';
$lang['EditRoute'] = 'Редактирование маршрута';
$lang['SearchRouteAuto'] = 'Поиск маршрутов автотранспорта';
$lang['Search'] = 'Поиск';


$lang['Transport'] = 'Транспорт';
$lang['Cost'] = 'Стоимость';
$lang['ContactData'] = 'Контактные данные';

//Admin routes_edit.php
$lang['Station'] = 'Остановка';
$lang['AddStation'] = 'Добавить остановку';
$lang['SeatSelect'] = 'Выберите место';
$lang['Close'] = 'Закрыть';
$lang['iSelect'] = 'Выбрал';
$lang['Message'] = 'Сообщение';
$lang['DeleteFeedbackQ'] = 'Удалить этот отзыв?';
$lang['LastTranzactions'] = 'Последние транзакции';
$lang['Day'] = 'День';
$lang['Time'] = 'Время';
$lang['DayFrom'] = 'День выезда';
$lang['TimeFrom'] = 'Время выезда';
$lang['DayTo'] = 'День приезда';
$lang['TimeTo'] = 'Время приезда';



$lang['Carriers'] = 'Перевозчики';
$lang['LastCarriers'] = 'Последние перевозчики';
$lang['SeeAllCarriers'] = 'Смотреть всех перевозчиков';
$lang['JPGorPNG'] = '( JPG или PNG )';
$lang['SortSaved'] = 'Сортировка сохранена';
$lang['LastOrders'] = 'Последние заказы';
$lang['OutputOfResults'] = 'Найдено';
$lang['Results'] = 'результатов';
$lang['Seats'] = 'Места';
$lang['SeatsLower'] = 'места';
$lang['PersonalData'] = 'Личные данные';
$lang['FirstNLastName'] = 'Фамилия, имя';
$lang['ChooseYourSeat'] = 'Выбрать места';
$lang['ChosenSeats'] = 'Выбранные места';
$lang['Add'] = 'Добавить';
$lang['SendEmail'] = 'Отправить е-mail';


////////////////
$lang['Cities'] = 'Города';
$lang['AddCity'] = 'Добавить город';
$lang['EditCity'] = 'Редактирование города';
$lang['RouteEdit'] = 'Редактирование маршрута';

$lang['CityFrom'] = 'Из Города';
$lang['CityTo'] = 'В Городе';

$lang['Active'] = 'Активен(а)';

//Admin Orders.php
$lang['All'] = 'Все';
$lang['Cash'] = 'Наличными';
$lang['Card'] = 'Карта';
$lang['Angro'] = 'Оптом';
$lang['Retail'] = 'Розница';
$lang['Pending'] = 'в ожидании';
$lang['Paid'] = 'Оплачено';


//Admin routes.php
$lang['ListofTransporters'] ='Список перевозчиков';
$lang['Daily'] = 'Ежедневно';
$lang['DeleteAll'] = 'Удалить все';

//Views tickets/ticket.php
$lang['Passenger'] = 'Пассажир';
$lang['Seat'] = 'Место';
$lang['SeatLower'] = 'место';

//views transporter/buses.php
$lang['BusName'] = 'Наименование';
$lang['BusTemplate'] = 'Шаблон автобуса';
$lang['BusTemplatePreview'] = 'Предосмотр шаблона';

//Views transporter/route_edit.php
$lang['ScheduleSaved'] = 'Расписание сохранено';
$lang['SeatPriceSaved'] = 'сохранено';
$lang['DeleteSeatPrice'] = 'удалена';

//Views card_confirm.php
$lang['CardPaymentRedirect'] = 'Перенаправление на сайт платёжной системы ...';

//Views route_info.php
$lang['TicketsData'] = "Данные билетов";
$lang['ToPayment'] = 'К оплате';

//--------------------------------------------------------------------------//

//Admin buses_list.php
$lang['Edit'] = 'Редактировать';

//Admin edit_bus_form.php
$lang['UploadImages'] = 'Загрузить изображения';

//Admin edit_news.php
$lang['PreviewVersion'] = 'Просмотр версии';
$lang['ToPublicVersion'] = 'К опубликованной версии';
$lang['ArticleNewsVersions'] = 'Версии новости';
$lang['ArticlePageVersions'] = 'Версии страницы';
$lang['SaveHistoryVersions'] = 'Сохранить в историю версий';
$lang['CommentToVersion'] = 'Коментарий к версии';
$lang['Versions'] = 'Версии';
$lang['SaveNPublish'] = 'Сохранить и опубликовать';
$lang['From'] = 'от';


//Admin edit_transporter.php
$lang['Routes'] = 'Маршруты';
$lang['Buses'] = 'Автобусы';
$lang['EditBusData'] = 'Редактирование данных автобуса';
$lang['AddNew'] = 'Добавить новый';

//Admin route_edit.php
$lang['Schedule'] = 'Расписание';



