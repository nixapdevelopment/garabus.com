<?php defined('BASEPATH') OR exit('No direct script access allowed');


// HEADER
$lang['MyCabinet'] = 'Cabinetul <br />meu';
$lang['SignIn'] = 'Întră';

// FOOTER
$lang['Informations'] = 'Informații';
$lang['Services'] = 'Servicii';
$lang['Partners'] = 'Parteneri';
$lang['Support'] = 'Suport';
$lang['Socials'] = 'Rețele sociale';
$lang['WeAccept'] = 'Acceptăm';


// SEARCH FORM
$lang['SearchFormText'] = 'Bilet electronic de autobuz internațional';

// SEARCH DETAILS
$lang['Company'] = 'Companie';
$lang['Bus'] = 'Autobuz';
$lang['ViewTransporterPage'] = 'Vezi pagina transpotatorului';

// Qualities
$lang['VerifiedTransporters'] = 'Transportatori verificați';
$lang['BestPrices'] = 'Cele mai avantajoase prețuri';
$lang['BusConfortable'] = 'Autobuze comfortabile';
$lang['Support24'] = 'Suport 24/7';

// News
$lang['LastNews'] = 'Noutăți recente';

// Home Page
$lang['HomeTitle'] = 'Autobuze internaționale';
$lang['HomeThanks'] = 'Vă mulțumim că ne-ați ales!';

// Modals
$lang['Close'] = 'Închide';
$lang['SignIn'] = 'Întră';






$lang['Slogan'] = 'Succesul nostru este în calitatea produselor noastre!';
$lang['AccountLink'] = 'Cabinetul personal';
$lang['Logout'] = 'Ieșire';
$lang['AllreadySubscribed'] = 'Email este deja subsemnat.Întroduceți oricare altă adresă';
$lang['SubscribeSuccess'] = 'Subsemnare confirmată';
$lang['Catalog'] = 'Catalog';
$lang['UserAccount'] = 'Cont utilizator';
$lang['Users'] = 'Utilizatori';
$lang['NotConfirmed'] = 'Neconfirmat(ă)';
$lang['RegDate'] = 'Data înregistrării';
$lang['DataSaved'] = 'Datele au fost sălvate';
$lang['Manager'] = 'Manager';
$lang['Managers'] = 'Manageri';

$lang['Menu'] = 'Meniu';
$lang['Menus'] = 'Meniuri';
$lang['MenuItems'] = 'Itemii meniului';
$lang['Seo'] = 'SEO';
$lang['Feedback'] = 'Feedback';
$lang['Content'] = 'Content';
$lang['TopRoutes'] = 'Topul Rutelor';

$lang['UserEmail'] = 'E-mail utilizator';
$lang['Password'] = 'Parolă';
$lang['ConfirmationPassword'] = 'Confirmați parola';
$lang['Login'] = 'Logare';
$lang['LostPassword'] = 'Ați uitat parola?';
$lang['Email'] = 'Adresă e-mail';
$lang['Send'] = 'Trimite';
$lang['PasswordRecovery'] = 'Recuperarea parolei';
$lang['PasswordRecoveryInfo'] = 'Întroduceți e-mail, indicat la momentul înregistrării';
$lang['InvalidLogin'] = 'Email sau parola au fost indicate incorect';
$lang['FeedbackSended'] = 'Feedback trimis';

$lang['Registration'] = 'Înregistrare';
$lang['PasswordConfirmation'] = 'Confirmați parola';
$lang['ClientType'] = 'Tip Client';
$lang['Retail'] = 'Retail';
$lang['Angro'] = 'Angro';
$lang['SelectClientType'] = 'Selectați tip client';
$lang['SelectPaymentType'] = 'Selectați metoda de achitare';
$lang['EmailAllreadyExist'] = 'Adresa Email deja înregistrată';
$lang['WaitConfirmation'] = 'Așteptați confirmarea contului';
$lang['WaitConfirmationText'] = 'Așteptați confirmarea din partea administratorului.';

$lang['RecoveryMessageSubject'] = 'Recuperarea parolei!';
$lang['EmailNotRegistered'] = 'Adresa Email deja înregistrată';
$lang['ResetPassword'] = 'Resetează parola';
$lang['RecoveryToken'] = 'Codul de resetare a parolei';
$lang['InvalidRecoveryToken'] = 'Codul de resetare a parolei este incorect';
$lang['LoginWithNewPassword'] = 'Parola a fost schimbată cu succes. Întrați cu parola nouă.';
$lang['WishRegEnterPassword'] = 'Dacă doriți de a vă înregistra, introduceți parola';

$lang['IDNO'] = 'IDNO';
$lang['BankName'] = 'Denumirea Băncii';
$lang['IBAN'] = 'IBAN';
$lang['Accept'] = 'Acceptă';
$lang['Client'] = 'Client';
$lang['Transporter'] = 'Transportator';
$lang['Terms'] = 'Reguli și Condiții';
$lang['SelectCity'] = 'Selectați orașul';

$lang['AdminAccount'] = 'Cont administrator';
$lang['ProfileSettings'] = 'Setările profilului';
$lang['Wellcome'] = 'Bine ați venit';

$lang['ConfirmCategoryDelete'] = 'Categoria va fi ștearsă împreună cu produse și subcategorii conținute!';
$lang['CategoryName'] = 'Denumirea categoriei';
$lang['Categories'] = 'Categoria';
$lang['AddCategory'] = 'Adaugă categorie';
$lang['AddContent'] = 'Adaugă conținut';

$lang['CategoryTitle'] = 'Titlul categoriei';
$lang['CategoryText'] = 'Textul categoriei';
$lang['Keywords'] = 'Cuvinte-cheie';
$lang['Description'] = 'Descriere';
$lang['Save'] = 'Salvează';
$lang['CategoryParent'] = 'Categoria părinte';
$lang['InvalidForm'] = 'Datele introduse în formă - incorecte, vezi erori';
$lang['CategorySaved'] = 'Categoria a fost salvată';
$lang['CategoryImage'] = 'Imaginea categoriei';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Statusul categoriei';
$lang['CatStatusActive'] = 'Activ(ă)';
$lang['CatStatusDisabled'] = 'Inactiv(ă)';
$lang['ShowIn'] = 'Afișează în';
$lang['ShowInAll'] = 'Toate';
$lang['ShowInAngro'] = 'În Angro';
$lang['ShowInRetail'] = 'În Retail';
$lang['CategoryDeleted'] = 'Categorie ștearsă';

$lang['Filters'] = 'Filtre';
$lang['AddFilter'] = 'Salvează filtru';
$lang['FilterName'] = 'Denumirea filtrului';
$lang['FilterType'] = 'Tipul Filtrului';
$lang['String'] = 'string';
$lang['Number'] = 'numeric';
$lang['FilterSaved'] = 'Filtru a fost salvat';
$lang['DeleteFilter'] = 'Ștergeți filtru?';
$lang['FilterDeleted'] = 'Filtru a fost șters';

$lang['Products'] = 'Produse';
$lang['AddProduct'] = 'Adaugă produs';
$lang['ProductName'] = 'Denumire produs';
$lang['Price'] = 'Preț';
$lang['Sorting'] = 'Sortare';
$lang['IsPromo'] = 'Promo';
$lang['IsPromoUpper'] = 'PROMO';
$lang['Filter'] = 'Filrează';
$lang['SKU'] = 'SKU';
$lang['Stock'] = 'Restul';
$lang['Image'] = 'Imagine';
$lang['GeneralData'] = 'Date generale';
$lang['Images'] = 'Imagini';
$lang['Link'] = 'Link';
$lang['ImagePhoto'] = 'Imagine';
$lang['GeneralPhoto'] = 'Imagine generală';
$lang['PriceAndStock'] = 'Prețul și rest-produse';
$lang['EditProduct'] = 'Editare produs';
$lang['ProductCategory'] = 'Categoria produs';
$lang['DiscountPrice'] = 'Prețul cu reducere';
$lang['DiscountPriceStart'] = 'Accesibil de la';
$lang['DiscountPriceEnd'] = 'Accesibil pînă la';
$lang['HasPromo'] = 'Promo ';
$lang['AddImages'] = 'Adaugă poza';
$lang['ProductStatus'] = 'Status produs';
$lang['ProdStatusActive'] = 'Activ(ă)';
$lang['ProdStatusDisabled'] = 'Inactiv(ă)';
$lang['ProductSaved'] = 'Produsul salvat';
$lang['ClearFilters'] = 'Resetează filtre';
$lang['AddProductFilter'] = 'Adaugă filru';
$lang['FilterName'] = 'Filtru';
$lang['FilterAllreadySelected'] = 'Filtru deja selectat';
$lang['DeleteSmth'] = 'Șterge';
$lang['ConfirmSmth'] = 'Confirmă?';
$lang['NoImageUploaded'] = 'Nu sunt imagini';
$lang['NoFileUploaded'] = 'Nu sunt fîșiere';

$lang['Slider'] = 'Slider';

$lang['AddPromotion'] = 'Adaugă promoție';
$lang['PromotionType'] = 'Tipul promoției';
$lang['Value'] = 'Valoare';
$lang['Clear'] = 'resetează';



$lang['News'] = 'Noutăți';
$lang['NewsUpperCase'] = 'NOUTĂȚI';
$lang['AddNews'] = 'Adaugă noutate';
$lang['Title'] = 'Titlu';
$lang['Text'] = 'Text';
$lang['Date'] = 'Data';
$lang['Status'] = 'Status';
$lang['NewsSaved'] = 'Noutatea salvată';
$lang['FileName'] = 'Denumirea fîșierului';
$lang['SeeAllNews'] = 'Vezi toate noutățile';

$lang['Pages'] = 'Pagini';
$lang['AddPage'] = 'Adaugă pagină';
$lang['PageSaved'] = 'Pagină salvată';

$lang['HomePage'] = 'Principala';
$lang['buy'] = 'Cumpără';
$lang['AddedToCart'] = 'Adaugat în coș';
$lang['OutOfStock'] = 'Nu este în stoc';
$lang['UserInfo'] = 'Despre utilizator';
$lang['CartChanged'] = 'Coșul a fost schimbat';
$lang['CartProductDeleted'] = 'Produsul șters din coș';
$lang['CartEmpty'] = 'Coșul e gol';
$lang['CartTotal'] = 'Total';
$lang['ShoppingCart'] = 'Coș';
$lang['ContinueShopping'] = 'Continuă';
$lang['Region'] = 'Regiune';
$lang['Address'] = 'Adresă';
$lang['Name'] = 'Prenume';
$lang['FamilyName'] = 'Nume';

$lang['BackToCart'] = 'Înapoi în coș';
$lang['PaymentType'] = 'Metoda de plată';
$lang['CardPayment'] = 'Achitare card';
$lang['CashPayment'] = 'Achitare cash';
$lang['Zip'] = 'Zip / Cod Poștal';
$lang['Phone'] = 'Telefon';
$lang['CompanyName'] = 'Denumire companie';
$lang['ContactPerson'] = 'Persoană de contact';
$lang['Activitation'] = 'Sfera de activitate';
$lang['Files'] = 'Fîșiere (de anexat)';
$lang['Thankyou'] = 'Vă Mulțumim!';
$lang['ThankyouTitle'] = 'Mulțumim pentru cumpărături!';
$lang['ThankyouText'] = 'Comanda este trimisă. Managerul va face legătura cu Dvs. în scurt timp';
$lang['ThankyouText2'] = 'ID Comandă #';
$lang['DownloadInvoice'] = 'Descarcă chitanța';

$lang['Dashboard'] = 'Panel';
$lang['OrderHistory'] = 'Istoria comenzii';
$lang['Amount'] = 'Sumă';
$lang['AccountBalance'] = 'Soldul contului';

$lang['Order'] = 'Comandă';
$lang['Orders'] = 'Comenzi';
$lang['SeeOrder'] = 'Vezi Comanda';
$lang['SeeAllOrders'] = 'Vezi toate comenzile';
$lang['TotalOrders'] = 'Total comenzi';
$lang['NewOrders'] = 'Noile comenzi';
$lang['Finance'] = 'Finanțe';
$lang['RequestForWithdrawal'] = 'Cererea de retragere a soldului';


$lang['BackToList'] = 'ÎNAPOI LA LISTĂ';
$lang['OrderDelails'] = 'Despre comandă';
$lang['OrderProducts'] = 'Comanda produselor';
$lang['Quantity'] = 'Cantitate';


$lang['Settings'] = 'Setări';
$lang['AddToCart'] = 'În coș';
$lang['Checkout'] = 'Achitare';
$lang['CurrencyName'] = 'ЛЕЕВ' ;
$lang['CurrencyNameLowerCase'] = 'леев' ;
$lang['ArticlesPerPage'] = 'Articole pe pagină';
$lang['SortBy'] = 'Sortare după';
$lang['Date'] = 'Data';
$lang['PlaceOrder'] = 'Ordine';
$lang['SeeAll'] = 'Vezi Toate';
$lang['NewProducts'] = 'НОВЫЕ ПРОДУКТЫ';
$lang['NewLabel'] = 'Новый!';
$lang['ContentAdditional'] = 'Conțitut Opțional';
$lang['Country'] = 'Țară';
$lang['City'] = 'Oraș';
$lang['NotExistsCity'] = 'Nu este oraș';

$lang['WhereFrom'] = 'De unde';
$lang['Whereto'] = 'Spre';
$lang['Departure'] = 'Pornire';
$lang['Arrival'] = 'Sosire';
$lang['Adult'] = 'Adult';
$lang['Adults'] = 'Adulți';
$lang['Kids'] = 'Copii';
$lang['Child'] = 'Copil';



$lang['Route'] = 'Rută';
$lang['RoutesPopular'] = 'Cele mai populare direcții';
$lang['EditRoute'] = 'Editarea rutei';
$lang['SearchRouteAuto'] = 'Caută rute de autocar';
$lang['Search'] = 'Caută';


$lang['Transport'] = 'Transport';
$lang['Cost'] = 'Cost';
$lang['ContactData'] = 'Datele de contact';

//Admin routes_edit.php
$lang['Station'] = 'Stație';
$lang['AddStation'] = 'Adaugă stație';
$lang['SeatSelect'] = 'Selectează loc';
$lang['Close'] = 'Închide';
$lang['iSelect'] = 'A selectat';
$lang['Message'] = 'Mesaj';
$lang['DeleteFeedbackQ'] = 'Șterge acest feedback?';
$lang['LastTranzactions'] = 'Tranzacții recente';
$lang['Day'] = 'Ziua';
$lang['Time'] = 'Timp';
$lang['DayFrom'] = 'Ziua pornirii';
$lang['TimeFrom'] = 'Timpul pornirii';
$lang['DayTo'] = 'Ziua sosirii';
$lang['TimeTo'] = 'Timpul sosirii';



$lang['Carriers'] = 'Transportatori';
$lang['LastCarriers'] = 'Transportatori recenți';
$lang['SeeAllCarriers'] = 'Vezi toți transportatori';
$lang['JPGorPNG'] = '( JPG sau PNG )';
$lang['SortSaved'] = 'Sortare salvată';
$lang['LastOrders'] = 'Comenzile recente';
$lang['OutputOfResults'] = 'Au fost găsite';
$lang['Results'] = 'rezultate';
$lang['Seats'] = 'Locuri';
$lang['SeatsLower'] = 'locuri';
$lang['PersonalData'] = 'Date Personale';
$lang['FirstNLastName'] = 'Nume, prenume';
$lang['ChooseYourSeat'] = 'Selecează locuri';
$lang['ChosenSeats'] = 'Locuri selectate';
$lang['Add'] = 'Adaugă';
$lang['SendEmail'] = 'Trimite е-mail';


////////////////
$lang['Cities'] = 'Orașe';
$lang['AddCity'] = 'Adaugă oraș';
$lang['EditCity'] = 'Editare oraș';
$lang['RouteEdit'] = 'Editarea rutei';

$lang['CityFrom'] = 'din Oraș';
$lang['CityTo'] = 'în Oraș';

$lang['Active'] = 'Activ(ă)';

//Admin Orders.php
$lang['All'] = 'Toate';
$lang['Cash'] = 'Cash';
$lang['Card'] = 'Card';
$lang['Angro'] = 'Angro';
$lang['Retail'] = 'Retail';
$lang['Pending'] = 'în așteptare';
$lang['Paid'] = 'Achitat';

//Admin routes.php
$lang['ListofTransporters'] ='Lista transportatorilor';
$lang['Daily'] = 'Zilnic';
$lang['DeleteAll'] = 'Șterge tot';

//Views tickets/ticket.php
$lang['Passenger'] = 'Pasager';
$lang['Seat'] = 'Locul';
$lang['SeatLower'] = 'locul';

//views transporter/buses.php
$lang['BusName'] = 'Denumire';
$lang['BusTemplate'] = 'Șablonul autobuzului';
$lang['BusTemplatePreview'] = 'Vezi șablon';

//Views transporter/route_edit.php
$lang['ScheduleSaved'] = 'Orarul a fost salvat';
$lang['SeatPriceSaved'] = 'Salvat';
$lang['DeleteSeatPrice'] = 'șters';

//Views card_confirm.php
$lang['CardPaymentRedirect'] = 'Redirecționare spre sistem de achitare...';

//Views route_info.php
$lang['TicketsData'] = "Datele Biletelor";
$lang['ToPayment'] = 'Spre achitare';

//--------------------------------------------------------------------------//

//Admin buses_list.php
$lang['Edit'] = 'Redactează';

//Admin edit_bus_form.php
$lang['UploadImages'] = 'Încarcă poze';

//Admin edit_news.php
$lang['PreviewVersion'] = 'Vezi versiune';
$lang['ToPublicVersion'] = 'Spre versiune publicată';
$lang['ArticleNewsVersions'] = 'Versiunile noutăţii';
$lang['ArticlePageVersions'] = 'Versiunile paginii';
$lang['SaveHistoryVersions'] = 'Salvează istoria versiunilor';
$lang['CommentToVersion'] = 'Comentariu la versiune';
$lang['Versions'] = 'Versiuni';
$lang['SaveNPublish'] = 'Salvează şi publică';
$lang['From'] = 'от';


//Admin edit_transporter.php
$lang['Routes'] = 'Rute';
$lang['Buses'] = 'Autobuze';
$lang['EditBusData'] = 'Redactarea datelor autobuzului';
$lang['AddNew'] = 'Adaugă nou';

//Admin route_edit.php
$lang['Schedule'] = 'Orar';
