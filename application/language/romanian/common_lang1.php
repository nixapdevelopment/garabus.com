<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['Slogan'] = 'Succesul nostru este în calitatea produselor!';
$lang['AccountLink'] = 'Cabinet personal';
$lang['Logout'] = 'Ieșire';
$lang['AllreadySubscribed'] = 'Email este deja abonat';
$lang['SubscribeSuccess'] = 'Email este abonat cu succes';
$lang['Catalog'] = 'Catalog';
$lang['UserAccount'] = 'Account';
$lang['Users'] = 'Users';
$lang['NotConfirmed'] = 'Not Confirmed';
$lang['RegDate'] = 'Data înregistrarii';
$lang['DataSaved'] = 'Date salvate';
$lang['Managers'] = 'Manageri';

$lang['Menu'] = 'Meniu';
$lang['Menus'] = 'Meniuri';
$lang['MenuItems'] = 'Itemii meniului';
$lang['Feedback'] = 'Feedback';
$lang['TopRoutes'] = 'Top Rute';

$lang['UserEmail'] = 'E-mail utilizator';
$lang['Password'] = 'Parola';
$lang['ConfirmationPassword'] = 'Confirmă parola';
$lang['Login'] = 'Logare';
$lang['LostPassword'] = 'Ai uitat parola?';
$lang['Email'] = 'Adresa e-mail';
$lang['Send'] = 'Trimite';
$lang['PasswordRecovery'] = 'Recuperarea parola';
$lang['PasswordRecoveryInfo'] = 'Întroduceți adresa e-mail indicată prin registrare';
$lang['InvalidLogin'] = 'Email sau parola este incorect';
$lang['FeedbackSended'] = 'Feedback trimis';

$lang['Registration'] = 'Înregistrare';
$lang['PasswordConfirmation'] = 'Confirmați parola';
$lang['ClientType'] = 'Tip client';
$lang['Retail'] = 'Retail';
$lang['Angro'] = 'Angro';
$lang['SelectClientType'] = 'Selectați tip client';
$lang['EmailAllreadyExist'] = 'Adresa email este deja înregistrată';
$lang['WaitConfirmation'] = 'Așteptați confirmarea contului';
$lang['WaitConfirmationText'] = 'Așteptați confirmarea contului din partea adminului.';

$lang['RecoveryMessageSubject'] = 'Recuperarea parola la www.agm.md';
$lang['EmailNotRegistered'] = 'Adresa e-mail întrodusă na fost înregistrată';
$lang['ResetPassword'] = 'Resetarea parolei';
$lang['RecoveryToken'] = 'Codul de resetare';
$lang['InvalidRecoveryToken'] = 'Codul de resetare este incorect';
$lang['LoginWithNewPassword'] = 'Parola a fost schimbată cu succes. Logați cu parola nouă.';
$lang['WishRegEnterPassword'] = 'Dacă vrei să te înregistrezi, introdu parola';

$lang['IDNO'] = 'IDNO';
$lang['BankName'] = 'Nume Bancă';
$lang['IBAN'] = 'IBAN';
$lang['Accept'] = 'Acceptă';
$lang['Client'] = 'Client';
$lang['Transporter'] = 'Transportator';
$lang['Terms'] = 'Termeni & Condiții';
$lang['SelectCity'] = 'Selectează Orașul';


$lang['AdminAccount'] = 'Cont administrator';
$lang['ProfileSettings'] = 'Setări profil';
$lang['Wellcome'] = 'Bine ați venit';

$lang['ConfirmCategoryDelete'] = 'Category will be deleted with subcategories and its products!';
$lang['CategoryName'] = 'Denumirea cetegoriei';
$lang['Categories'] = 'Categorii';
$lang['AddCategory'] = 'Adaugă categorie';
$lang['AddContent'] = 'Adaugă content';

$lang['CategoryTitle'] = 'Titlu cetegoriei';
$lang['CategoryText'] = 'Textul cetegoriei';
$lang['Keywords'] = 'Keywords';
$lang['Description'] = 'Description';
$lang['Save'] = 'Salvare';
$lang['CategoryParent'] = 'Parent category';
$lang['InvalidForm'] = 'Form is invalid. See errors.';
$lang['CategorySaved'] = 'Categorie a fost sălvată';
$lang['CategoryImage'] = 'Poza categoriei';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Status categorie';
$lang['CatStatusActive'] = 'Activă';
$lang['CatStatusDisabled'] = 'Dezactivată';
$lang['ShowIn'] = 'Arată în';
$lang['ShowInAll'] = 'Toate';
$lang['ShowInAngro'] = 'Angro';
$lang['ShowInRetail'] = 'Retail';
$lang['CategoryDeleted'] = 'Categorie a fost eliminată';

$lang['Filters'] = 'Filtre';
$lang['AddFilter'] = 'Salvați filtru';
$lang['FilterName'] = 'Denumirea filtru';
$lang['FilterType'] = 'Tip filtru';
$lang['String'] = 'String';
$lang['Number'] = 'Number';
$lang['FilterSaved'] = 'Filtru a fost sălvat';
$lang['DeleteFilter'] = 'Elimita filtru?';
$lang['FilterDeleted'] = 'Filtru a fost eliminat';

$lang['Products'] = 'Produse';
$lang['AddProduct'] = 'Adaugă produs';
$lang['ProductName'] = 'Denumirea produs';
$lang['Price'] = 'Preț';
$lang['Sorting'] = 'Sortarea';
$lang['IsPromo'] = 'Promo';
$lang['IsPromoUpper'] = 'PROMO';
$lang['Filter'] = 'Filtreză';
$lang['SKU'] = 'Articol';
$lang['Stock'] = 'Stoc';
$lang['Image'] = 'Imagine';
$lang['GeneralData'] = 'Date generale';
$lang['Images'] = 'Poze';
$lang['Link'] = 'Link';
$lang['ImagePhoto'] = 'Poza';
$lang['GeneralPhoto'] = 'Poza generală';
$lang['PriceAndStock'] = 'Preț și stocuri';
$lang['EditProduct'] = 'Editare produs';
$lang['ProductCategory'] = 'Categorie produs';
$lang['DiscountPrice'] = 'Preț promoțional';
$lang['DiscountPriceStart'] = 'Valabil de la data';
$lang['DiscountPriceEnd'] = 'Valabil pînă la data';
$lang['HasPromo'] = 'Promo ';
$lang['AddImages'] = 'Adaugă poze';
$lang['ProductStatus'] = 'Status produs';
$lang['ProdStatusActive'] = 'Active';
$lang['ProdStatusDisabled'] = 'Dezactivat';
$lang['ProductSaved'] = 'Produs a fost sălvat';
$lang['ClearFilters'] = 'Reseta filtre';
$lang['AddProductFilter'] = 'Adaugă filtru';
$lang['FilterName'] = 'Filtru';
$lang['FilterAllreadySelected'] = 'Filtru este deja selectat';
$lang['DeleteSmth'] = 'Șterge';
$lang['ConfirmSmth'] = 'Confirmați?';
$lang['NoImageUploaded'] = 'Nici o poza încarcată';
$lang['NoFileUploaded'] = 'Nici un fișier încărcat';

$lang['Slider'] = 'Slider';

$lang['AddPromotion'] = 'Adaugă promoție';
$lang['PromotionType'] = 'Tip promoție';
$lang['Value'] = 'Valoare';
$lang['Clear'] = 'Resetează';



$lang['News'] = 'Noutăți';
$lang['NewsUpperCase'] = 'NOUTĂȚI';
$lang['AddNews'] = 'Adaugă noutate';
$lang['Title'] = 'Titlu';
$lang['Text'] = 'Text';
$lang['Date'] = 'Data';
$lang['Status'] = 'Status';
$lang['NewsSaved'] = 'Noutate a fost sălvată';
$lang['FileName'] = 'Denumirea fișier';
$lang['SeeAllNews'] = 'vezi toate noutățile';

$lang['Pages'] = 'Pagini';
$lang['AddPage'] = 'Adaugă pagina';
$lang['PageSaved'] = 'Pagina a fost sălvată';

$lang['HomePage'] = 'Principală';
$lang['buy'] = 'Cumpără';
$lang['AddedToCart'] = 'Adaugat în coș';
$lang['OutOfStock'] = 'Out Of Stock';
$lang['UserInfo'] = 'User Info';
$lang['CartChanged'] = 'Cart changed';
$lang['CartProductDeleted'] = 'Cart product deleted';
$lang['CartEmpty'] = 'Cart empty';
$lang['CartTotal'] = 'Total';
$lang['ShoppingCart'] = 'Shopping Cart';
$lang['ContinueShopping'] = 'Continue Shopping';
$lang['Region'] = 'Regiunea';
$lang['Address'] = 'Adresa';
$lang['Name'] = 'Nume';
$lang['BackToCart'] = 'Back To Cart';
$lang['PaymentType'] = 'Payment Type';
$lang['CardPayment'] = 'Card Payment';
$lang['CashPayment'] = 'Cash Payment';
$lang['Zip'] = 'Zip / Cod poștal';
$lang['Phone'] = 'Telefon';
$lang['CompanyName'] = 'Nume companie';
$lang['ContactPerson'] = 'Persoana de contact';

$lang['Activitation'] = 'Domeniu de activitate';
$lang['Files'] = 'Fișiere (documente de anexat)';
$lang['Thankyou'] = 'Thankyou';
$lang['ThankyouTitle'] = 'Mulțumim pentru comanda Dumnevoastră';
$lang['ThankyouText'] = 'Your order has been created. Our manager will be contacted with you soon.';
$lang['ThankyouText2'] = 'Yout order ID #';
$lang['DownloadInvoice'] = 'Descarcă factura';

$lang['Dashboard'] = 'Dashboard';
$lang['OrderHistory'] = 'Istoria comenzii';
$lang['Amount'] = 'Suma';
$lang['AccountBalance'] = 'Soldul contului';

$lang['Order'] = 'Comanda';
$lang['Orders'] = 'Comenzii';
$lang['SeeOrder'] = 'Vezi comanda';
$lang['SeeAllOrders'] = 'Vezi toate comenzile';
$lang['TotalOrders'] = 'Comenzi în total';//Всего заказов
$lang['NewOrders'] = 'Noile comenzi';//Всего заказов
$lang['Finance'] = 'Finanțe';
$lang['RequestForWithdrawal'] = 'Cererea de retragere';


$lang['BackToList'] = 'ÎNAPOI LA LISTA';
$lang['OrderDelails'] = 'Order Delails';
$lang['OrderProducts'] = 'Order Products';
$lang['Quantity'] = 'Cantitate';


$lang['Settings'] = 'Setări';
$lang['AddToCart'] = 'În coș';
$lang['Checkout'] = 'Achitare';
$lang['CurrencyName'] = 'LEI' ;
$lang['CurrencyNameLowerCase'] = 'lei' ;
$lang['ArticlesPerPage'] = 'Articole pe pagină';
$lang['SortBy'] = 'Sortează după';
$lang['Date'] = 'Data';
$lang['PlaceOrder'] = 'Ordinea locului';
$lang['SeeAll'] = 'vezi pe toate';
$lang['NewProducts'] = 'PRODUSE NOI';
$lang['NewLabel'] = 'Nou!';
$lang['ContentAdditional'] = 'Content Addițional';
$lang['Country'] = 'Țară';
$lang['City'] = 'Oraș';
$lang['NotExistsCity'] = 'Nu există oraș';

$lang['WhereFrom'] = 'Plecare';
$lang['Whereto'] = 'Sosire';
$lang['Departure'] = 'Data';
$lang['Arrival'] = 'Sosire';
$lang['Adults'] = 'Adulți';
$lang['Kids'] = 'Copii';
$lang['Persoane'] = 'Persoane';



$lang['Route'] = 'Cursa';//Маршрут
$lang['RoutesPopular'] = 'Cele mai populare direcții';
$lang['EditRoute'] = 'Editare traseu';//Редактирование маршрута
$lang['SearchRouteAuto'] = 'Caută curse de autocar';
$lang['Search'] = 'Caută';


$lang['Transport'] = 'Transport';
$lang['Cost'] = 'Cost';

$lang['AddStation'] = 'Adaugă stație';//Добавить остановку
$lang['Close'] = 'Închide';
$lang['Message'] = 'Mesaj';
$lang['DeleteFeedbackQ'] = 'Elimina acest feedback?';
$lang['LastTranzactions'] = 'Ultimele tranzacții';


$lang['Carriers'] = 'Transportatori';
$lang['LastCarriers'] = 'Ultimii transportatori';
$lang['SeeAllCarriers'] = 'Vezi toți transportatori';
$lang['JPGorPNG'] = '( JPG sau PNG )';
$lang['SortSaved'] = 'Sortarea salvată';
$lang['LastOrders'] = 'Ultimele comenzi';
$lang['OutputOfResults'] = 'Sunt afișate';
$lang['Results'] = 'rezultate';
$lang['Seats'] = 'Locuri';
$lang['SeatsLower'] = 'locuri';
$lang['PersonalData'] = 'Date personale';
$lang['FirstNLastName'] = 'Nume, prenume';
$lang['ChooseYourSeat'] = 'Alege locul';
$lang['Add'] = 'Adaugă';
$lang['SendEmail'] = 'Trimite е-mail';

$lang['CityFrom'] = 'Din Oras';
$lang['CityTo'] = 'In Oras';













