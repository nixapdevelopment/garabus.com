<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AppController extends CI_Controller
{
    
    public $header = null;
    
    public $footer = null;
    
    public $langID = 1;
    
    public $breadscrumbs = [];
    
    public $data = [];
    
    public $backend = false;


    public function __construct()
    {
        parent::__construct();
       
        $this->parse_lang();
    }
    
    public function parse_lang()
    {
        $ar = explode('/', $_SERVER['REQUEST_URI']);
        $ar = array_filter($ar);

        $langs = get_instance()->config->item('languages');

        if (isset($ar[1]) && strlen($ar[1]) == 2)
        {
            foreach ($langs as $langID => $lang)
            {
                if ($ar[1] == $lang['Slug'])
                {
                    $this->langID = $langID;
                    $this->config->set_item('language', $lang['LangFile']);
                    $this->lang->load('common', $lang['LangFile']);
                    break;
                }
                else
                {
                    $this->lang->load('common', 'russian');
                }
            }
        }
        else
        {
            $this->langID = 2;
            $this->config->set_item('language', 'russian');
            $this->lang->load('common', 'russian');
            redirect(site_url('ru/' . trim($_SERVER['REQUEST_URI'], '/')));
        }
    }

    public function render($view, $data = [])
    {
        $this->load->view($this->header);
        $this->load->view($view, $data);
        $this->load->view($this->footer);
    }
    
    public function addBreadscrumb($link, $text)
    {
        $this->breadscrumbs[$link] = $text;
    }
    
}

class FrontController extends AppController
{
    
    public $header = 'layouts/header';
    
    public $footer = 'layouts/footer';
    
    public $_user = null;
    
    public $shopType = 'Retail';
    
    public $menu;
    
    public $menuType = '1';
    
    public $accountLink = '';
    
    public $cartAmount = 0.00;
    
    public $seo = [];


    public function menu()
    {
        $this->menu = [
            'Admin' => [
                'admin/index' => lang('Dashboard'),
                //'admin/categories' => lang('Categories'),
                //'admin/products' => lang('Products'),
                //'admin/filters' => lang('Filters'),
                'admin/transporters' => lang('Carriers'),
                'admin/users' => lang('Users'),
                'admin/users/?Type=Manager' => lang('Managers'),
                'admin/pages' => lang('Pages'),
                'admin/news' => lang('News'),
                'admin/toproutes' => lang('TopRoutes'),
                'admin/menus' => lang('Menus'),
                'admin/slider' => lang('Slider'),
                'admin/orders' => lang('Orders'),
                'admin/feedback' => lang('Feedback'),
                'admin/seo' => lang('Seo'),
                'admin/cities' => lang('Cities'),
            ],
            'User' => [
                'user/index' => lang('Dashboard'),
                'user/orders' => lang('Orders'),
                'user/settings' => lang('Settings'),
            ],
            'Manager' => [
                'admin/index' => lang('Dashboard'),
                'admin/users' => lang('Users'),
                'admin/orders' => lang('Orders'),
            ]
        ];
    }

    public function __construct()
    {
        parent::__construct();
        
        $loged_user_id = $this->session->userdata('UserID');
        
        if ($loged_user_id)
        {
            $this->load->model('UserModel');
            $this->_user = $this->UserModel->getByID($loged_user_id);
            $this->shopType = $this->_user->Type;
            
            switch ($this->_user->Type)
            {
                case 'Admin':
                    $this->accountLink = 'admin';
                    break;
                case 'Manager':
                    $this->accountLink = 'admin';
                    break;
                case 'User':
                    $this->accountLink = 'user/settings';
                    break;
                case 'Transporter':
                    $this->accountLink = 'transporter';
                    break;
                default:
                    $this->accountLink = 'login';
                    break;
            }
        }
        
        $this->addBreadscrumb(site_url(), lang('HomePage'));
        
        $this->menu();
        
        $res = $this->db->select('*, m.Type as MenuType')
            ->from('Menu as m')
            ->join('MenuItem as mi', 'mi.MenuID = m.ID')
            ->join('MenuItemLang as mil', 'mil.MenuItemID = mi.ID and mil.LangID = ' . $this->langID)
            ->join('Url as u', 'u.ObjectID = mi.EntityID and u.Type = mi.EntityType')
            ->order_by('mi.MenuID, mi.Position')
            ->get()->result();
        
        foreach ($res as $row)
        {
            $this->menu[$row->MenuType][$row->Link] = $row->Name;
        }
    }
    
}

class AdminController extends FrontController
{
    
    public $header = 'layouts/header';
    
    public $footer = 'layouts/footer';
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user) || !in_array($this->_user->Type, ['Admin', 'Manager', 'Seo']))
        {
            redirect('login');
        }
    }
    
}
class TransporterController extends FrontController
{
    
    public $header = 'layouts/header';
    
    public $footer = 'layouts/footer';
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user) || !in_array($this->_user->Type, ['Admin', 'Transporter']))
        {
            redirect('login');
        }
    }
    
}

class UserController extends FrontController
{
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user))
        {
            redirect('login');
        }
        
        if ($this->_user->Status == 'NotConfirmed')
        {
            redirect('wait-confirmation');
        }
    }
    
}