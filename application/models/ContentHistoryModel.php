<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContentHistoryModel extends CI_Model
{
    
    private function chSelect($langID = false)
    {
        $this->db->select('*');
        $this->db->from('ContentHistory as ch');
        $this->db->join('ContentHistoryLang as chl', 'chl.ContentHistoryID = ch.ID' . ($langID ? " AND chl.LangID = $langID" : ""), 'LEFT');
    }
    
    public function getHistoryList($entityID, $entityType, $currentRoute)
    {
        $this->db->select('ch.*, u.Name');
        $this->db->from('ContentHistory as ch');
        $this->db->join('User as u', 'u.ID = ch.UserID', 'LEFT');
        $this->db->where('ch.EntityID', $entityID);
        $this->db->where('ch.EntityType', $entityType);
        $this->db->order_by('ch.Date', 'DESC');
        $result = $this->db->get()->result();
        
        $this->load->library('table');
        $tmpl = [
            'table_open' => '<table id="history-table" class="table table-hover table-striped table-bordered datatable">'
        ];
        $this->table->set_template($tmpl);
        $this->table->set_heading([
            'ID', 'Дата', 'Изменил', 'Коментарий', [
                'data' => '',
                'style' => 'max-width: 100px;',
                'class' => 'text-center'
            ]
        ]);
        
        foreach ($result as $row)
        {
            $actionColumn = [
                'data' => '<a class="history-view" href="' . site_url($currentRoute, ['history' => $row->ID], true) . '">Просмотреть</a>&nbsp;&nbsp;<a onclick="return confirm(\'Confirm?\')" class="history-delete text-danger" href="' . site_url('admin/delete_history', ['history' => $row->ID]) . '">Удалить</a>',
                'style' => 'max-width: 100px;',
                'class' => 'text-center'
            ];
            $this->table->add_row([
                $row->ID, date('d.m.Y H:i:s', strtotime($row->Date)), $row->Name, $row->Comment, $actionColumn
            ]);
        }
        
        return $this->table->generate() . '<br />';
    }

    public function getHistory($entityID, $entityType, $langID = false)
    {
        $this->chSelect($langID);
        $this->db->where('ch.EntityID', $entityID);
        $this->db->where('ch.EntityType', $entityType);
        $this->db->order_by('ch.Date', 'DESC');
        return $this->db->get()->result();
    }
    
    public function saveHistory($entityID, $entityType, $titles, $texts, $titlesSeo, $keywords, $descriptions, $link = '', $status = 'Disabled', $comment = '')
    {
        $this->db->insert('ContentHistory', [
            'UserID' => $this->session->userdata('UserID'),
            'Date' => date('c'),
            'Comment' => $comment,
            'EntityID' => $entityID,
            'EntityType' => $entityType,
            'Link' => $link,
            'Status' => $status
        ]);
        $ContentHistoryID = $this->db->insert_id();
        
        $active_langs = $this->config->item('languages');
        
        $chl_insert = [];
        foreach ($active_langs as $langID => $lang)
        {
            $chl_insert[] = [
                'ContentHistoryID' => $ContentHistoryID,
                'LangID' => $langID,
                'Title' => isset($titles[$langID]) ? $titles[$langID] : '',
                'Text' => isset($texts[$langID]) ? $texts[$langID] : '',
                'TitleSeo' => isset($titlesSeo[$langID]) ? $titlesSeo[$langID] : '',
                'Keywords' => isset($keywords[$langID]) ? $keywords[$langID] : '',
                'Description' => isset($descriptions[$langID]) ? $descriptions[$langID] : ''
            ];
        }
        $this->db->insert_batch('ContentHistoryLang', $chl_insert);
        
        return $ContentHistoryID;
    }
    
}