<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RouteSeoModel extends CI_Model
{
    
    private function seoSelect($langID = false)
    {
        $this->db->select("*, 
            (SELECT `Name` FROM `CityLang` WHERE `CityID` = rs.CityFrom " . (empty($langID) ? '' : "AND `LangID` = $langID") . " LIMIT 1) as CityFromName, 
            (SELECT `Name` FROM `CityLang` WHERE `CityID` = rs.CityTo " . (empty($langID) ? '' : "AND `LangID` = $langID") . " LIMIT 1) as CityToName 
        ");
        $this->db->from('RouteSeo as rs');
        $this->db->join('RouteSeoLang as rsl', "rsl.RouteSeoID = rs.ID");
        
        if ($langID)
        {
            $this->db->where('rsl.LangID', $langID);
        }
    }
    
    public function getAll($langID = 1, $limit = 10, $offset = 0)
    {
        $this->seoSelect($langID);
        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    public function getByID($id, $langID = 1)
    {
        $this->seoSelect($langID);
        $this->db->where('rs.ID', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function getForEdit($id)
    {
        $this->seoSelect(false);
        $this->db->where('rs.ID', $id);
        $res = $this->db->get()->result();
        
        $return = new stdClass();
        foreach ($res as $row)
        {
            $return->data = $row;
            $return->langs[$row->LangID] = $row;
        }
        
        return $return;
    }
    
    public function getByCity($CityFrom, $CityTo, $langID = 1)
    {
        $this->seoSelect($langID);
        $this->db->where(['rs.CityFrom'=>$CityFrom, 'rs.CityTo'=>$CityTo]);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
}