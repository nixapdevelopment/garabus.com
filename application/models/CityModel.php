<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CityModel extends CI_Model
{
    
    private function citySelect($langID)
    {
        $this->db->select('*, c.ID, cl.Name, cnl.Name as CountryName');
        $this->db->from('City as c');
        $this->db->join('CityLang as cl', "cl.CityID = c.ID and cl.LangID = $langID");
        $this->db->join('CountryLang as cnl', "cnl.CountryID = c.CountryID and cnl.LangID = $langID");
    }
    
    public function getByID($id, $langID = 1)
    {
        $this->citySelect($langID);
        $this->db->where('c.ID', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function searchByName($name, $langID = 1)
    {
        $this->db->select('*');
        $this->db->from('City as c');
        $this->db->join('CityLang as cl', "cl.CityID = c.ID");
        $this->db->like('cl.Name', $name);
        $this->db->limit(150);
        $res = $this->db->get()->result();
        
        $return = [];
        foreach ($res as $row)
        {
            if ($row->LangID == $langID)
            {
                $return[$row->ID] = $row;
            }
        }
        
        return $return;
    }
    
    public function getList($langID = 1, $countryID = false)
    {
        $this->citySelect($langID);
        if ($countryID)
        {
            $this->db->where('c.CountryID', $countryID);
        }
        $this->db->order_by('cl.Name');
        return $this->db->get()->result();
    }
    
    public function getForEdit($id)
    {
        $this->db->select('*');
        $this->db->from('City as c');
        $this->db->join('CityLang as cl', "cl.CityID = c.ID");
        $this->db->where('c.ID', $id);
        $res = $this->db->get()->result();
        
        $return = new stdClass();
        foreach ($res as $row)
        {
            $return->city = $row;
            $return->langs[$row->LangID] = $row;
        }
        
        return $return;
    }
    
    public static function dropdown($langID, $countryID, $postName = 'CityID', $selected = '', $extra = '')
    {
        $_this = new CityModel();
        $res = $_this->getList($langID, $countryID);
        
        $options = [];
        $options[''] = ' - ';
        foreach ($res as $row)
        {
            $options[$row->ID] = $row->Name;
        }
        
        return form_dropdown($postName, $options, $selected, $extra);
    }
    
    public function getListByCountry($countryID, $langID = 1)
    {
        $this->citySelect($langID);
        $this->db->where('c.CountryID', $countryID);
        $this->db->order_by('cl.Name');
        return $this->db->get()->result();
    }
    
}