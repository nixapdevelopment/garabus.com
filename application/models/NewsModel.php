<?php defined('BASEPATH') OR exit('No direct script access allowed');

class NewsModel extends CI_Model
{
    
    private function newsSelect($langID)
    {
        $this->db->select('*');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', "nl.NewsID = n.ID and nl.LangID = $langID");
        $this->db->join('NewsImage as ni', "ni.NewsID = n.ID and ni.IsMain = 1");
        $this->db->join('Url as u', "u.ObjectID = n.ID and u.Type = 'News'");
    }
    
    public function getLast($langID = 1, $limit = 4, $offset = 0)
    {
        $this->newsSelect($langID);
        $this->db->where('n.Status', 'Active');
        $this->db->order_by('n.Date', 'DESC');
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get()->result();
    }
    
    public function getByID($id, $langID = 1)
    {
        $this->newsSelect($langID);
        $this->db->where('n.ID', $id);
        $this->db->where('n.Status', 'Active');
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function getImagesByNewsID($id)
    {
        $this->db->where('NewsID', $id);
        return $this->db->get('NewsImage')->result();
    }
    
    public function getFilesByNewsID($id)
    {
        $this->db->where('EntityID', $id);
        $this->db->where('EntityType', 'News');
        return $this->db->get('File')->result();
    }
    
}