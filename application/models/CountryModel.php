<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CountryModel extends CI_Model
{
    
    private function countrySelect($langID)
    {
        $this->db->select('*');
        $this->db->from('Country as c');
        $this->db->join('CountryLang as cl', "cl.CountryID = c.ID and cl.LangID = $langID");
    }
    
    public function getByID($id, $langID = 1)
    {
        $this->countrySelect($langID);
        $this->db->where('c.ID', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function searchByName($name, $langID = 1)
    {
        $this->db->select('*');
        $this->db->from('Country as c');
        $this->db->join('CountryLang as cl', "cl.CountryID = c.ID");
        $this->db->like('cl.Name', $name);
        $this->db->limit(150);
        $res = $this->db->get()->result();
        
        $return = [];
        foreach ($res as $row)
        {
            if ($row->LangID == $langID)
            {
                $return[$row->ID] = $row;
            }
        }
        
        return $return;
    }
    
    public function getList($langID = 1)
    {
        $this->countrySelect($langID);
        $this->db->order_by('cl.Name');
        return $this->db->get()->result();
    }
    
    public function getForEdit($id)
    {
        $this->db->select('*');
        $this->db->from('Country as c');
        $this->db->join('CountryLang as cl', "cl.CountryID = c.ID");
        $this->db->where('c.ID', $id);
        $res = $this->db->get()->result();
        
        $return = new stdClass();
        foreach ($res as $row)
        {
            $return->country = $row;
            $return->langs[$row->LangID] = $row;
        }
        
        return $return;
    }
    
    public static function dropdown($langID, $selected = '', $extra = '', $postName = 'CountryID')
    {
        $_this = new CountryModel();
        $res = $_this->getList($langID);
        
        $options = [];
        $options[''] = ' - ';
        foreach ($res as $row)
        {
            $options[$row->ID] = $row->Name;
        }
        
        return form_dropdown($postName, $options, $selected, $extra);
    }
    
}