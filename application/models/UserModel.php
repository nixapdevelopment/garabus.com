<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
    
    public function getForLogin($username, $password)
    {
        return $this->db->get_where('User', ['UserName' => $username, 'Password' => $password], 1)->row();
    }
    
    public function getByUsername($username)
    {
        return $this->db->get_where('User', ['UserName' => $username], 1)->row();
    }
    
    public function getByID($user_id)
    {
        return $this->db->get_where('User', ['ID' => $user_id], 1)->row();
    }
    
    public static function dropdown($langID, $selected = '', $extra = '', $userType = false)
    {
        $_this = new UserModel();
        
        $_this->db->select('*');
        $_this->db->from('User');
        
        if ($userType)
        {
            $_this->db->where('Type', $userType);
        }
        
        $_this->db->order_by('Name');
        $res = $_this->db->get()->result();
        
        $options = [];
        $options[''] = ' - ';
        foreach ($res as $row)
        {
            $options[$row->ID] = $row->Name;
        }
        
        return form_dropdown('UserID', $options, $selected, $extra);
    }
    
}