<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RouteModel extends CI_Model
{
    
    private function routeSelect($langID)
    {
        $this->db->select('*, r.ID, cfl.Name as CityFromName, ctl.Name as CityToName');
        $this->db->from('Route as r');
        $this->db->join('City as cf', 'cf.ID = r.CityFrom');
        $this->db->join('CityLang as cfl', "cfl.CityID = cf.ID AND cfl.LangID = $langID");
        $this->db->join('City as ct', 'ct.ID = r.CityTo');
        $this->db->join('CityLang as ctl', "ctl.CityID = ct.ID AND ctl.LangID = $langID");
    }
    
    public function getByID($id, $langID = 1)
    {
        $this->routeSelect($langID);
        $this->db->where('r.ID', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function getForEdit($id)
    {
        $this->db->select('*');
        $this->db->from('City as c');
        $this->db->join('CityLang as cl', "cl.CityID = c.ID");
        $this->db->where('c.ID', $id);
        $res = $this->db->get()->result();
        
        $return = new stdClass();
        foreach ($res as $row)
        {
            $return->city = $row;
            $return->langs[$row->LangID] = $row;
        }
        
        return $return;
    }
    
    public function getByUserID($userID, $langID = 1)
    {
        $this->routeSelect($langID);
        $this->db->where('r.UserID', $userID);
        return $this->db->get()->result();
    }
    
    public static function daySelect($langID = 1, $postName = 'Day', $selected = '', $extra = '')
    {
        $options = [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5
        ];
        return form_dropdown($postName, $options, $selected, $extra);
    }
    
    public function search($from, $to, $date, $persons = 1, $langID = 1)
    {
        $query = "
            SELECT 
                *,
                r.ID, 
                COUNT(r.ID) AS `count`, 
                (SELECT Name FROM CityLang WHERE CityID = r.CityFrom AND LangID = $langID LIMIT 1) AS FromName, 
                (SELECT Name FROM CityLang WHERE CityID = r.CityTo  AND LangID = $langID LIMIT 1) AS ToName, 
                (SELECT Time FROM RouteStation WHERE RouteID = r.ID AND Type = 'Start') as FromTime, 
                (SELECT Time FROM RouteStation WHERE RouteID = r.ID AND Type = 'End') as ToTime, 
                (41 - (SELECT COUNT(Seat) FROM ReservedSeats WHERE RouteID = r.ID AND Date = '$date')) as AvalableSeats 
            FROM RouteStation AS rs 
            LEFT JOIN RouteTrip AS rt ON rt.RouteID = rs.RouteID 
            LEFT JOIN Route AS r ON r.ID = rt.RouteID 
            LEFT JOIN User AS u ON u.ID = r.UserID 
            WHERE ((rs.CityID = $from AND rs.`Type` <> 'End') OR (rs.CityID = $to AND rs.`Type` <> 'Start')) AND rt.Date = '$date' 
            GROUP BY r.ID 
            HAVING `count` = 2 
            ORDER BY FromTime
        ";
        return $this->db->query($query)->result();
    }
    
    public function getTopRoutes($langID = 1)
    {
        $this->db->select('
            r.CityFrom as CityFromID, r.CityTo as CityToID, cfl.Name as CityFrom, ctl.Name as CityTo, 
            COALESCE((SELECT MIN(Price) FROM SeatPrice WHERE RouteID = r.ID), MIN(r.Price)) as MinPrice
        ');
        $this->db->from('Route as r');
        $this->db->join('TopRoutes as trf', 'trf.CityFromID=r.CityFrom');
        $this->db->join('TopRoutes as trt', 'trt.CityToID=r.CityTo');
        $this->db->join('City as cf', 'cf.ID = r.CityFrom');
        $this->db->join('CityLang as cfl', "cfl.CityID = cf.ID AND cfl.LangID = $langID");
        $this->db->join('City as ct', 'ct.ID = r.CityTo');
        $this->db->join('CityLang as ctl', "ctl.CityID = ct.ID AND ctl.LangID = $langID");
        $this->db->where("r.Status", "Active");
        $this->db->group_by("r.CityFrom, r.CityTo");
        $this->db->order_by("trf.Order");
        return $this->db->get()->result();
    }
    
    
    
}