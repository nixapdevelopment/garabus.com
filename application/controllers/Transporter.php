<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transporter extends TransporterController
{
    
    public $menuType = '';
    public $backend = true;
    
    public function __construct()
    {
        parent::__construct();
        
        if ($this->_user->Type != 'Transporter')
        {
            if (!in_array($this->uri->segment(2), ['', 'index', 'orders', 'order', 'users', 'user']))
            redirect(site_url('login'));
        }
    }
    
    public function index()
    {
        // last orders
        $this->db->select('SQL_CALC_FOUND_ROWS *, o.ID', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.UserID', $this->_user->ID);
        $this->db->order_by('o.Date', 'DESC');        
        $this->db->limit(10);
        $data['orders'] = $this->db->get()->result();
        
        $this->render('transporter/index', $data);
    }
    
    public function orders()
    {
        $this->addBreadscrumb('', lang('Orders'));
        
        $this->load->helper('form');
        
        $ClientID = $this->input->get('ClientID');
        $Date = $this->input->get('Date');
        $PaymentType = $this->input->get('PaymentType');
        $Type = $this->input->get('Type');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, o.ID', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.UserID', $this->_user->ID);
        $this->db->order_by('o.Date', 'DESC');
        if (!empty($ClientID)) $this->db->where('o.UserID', $ClientID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($PaymentType))$this->db->where('o.PaymentType', $PaymentType);
        if (!empty($Type))$this->db->where('o.Type', $Type);
        if (!empty($Status))$this->db->where('o.Status', $Status);
        
        if ($this->_user->Type == 'Manager')
        {
            $this->db->where('od.Region', $this->_user->Region);
        }
        
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['orders'] = $this->db->get()->result();
        
        $total_orders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('transporter/orders', [], true);
        $config['total_rows'] = $total_orders;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $clients = $this->db->where_in('Type', ['Angro', 'Retail'])->get('User')->result();
        $data['clients'][''] = 'All';
        foreach ($clients as $client)
        {
            $data['clients'][$client->ID] = $client->Name;
        }
        
        $this->render('transporter/orders', $data);
    }

    public function order($order_id = false)
    {
        if (!$order_id) redirect('transporter/orders');
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where(['o.ID' => $order_id, 'o.UserID' => $this->_user->ID]);
        $data['order'] = $this->db->get()->row();
        
        if (empty($data['order'])) redirect('transporter/orders');
        
        $this->db->update('Order', ['IsNew' => 0], ['ID' => $data['order']->OrderID], 1);
        
        $this->config->load('regions');
        
        $this->addBreadscrumb(site_url('transporter/orders'), lang('Orders'));
        $this->addBreadscrumb('', '# ' . $data['order']->OrderID);
        
        $data['client'] = $this->db->get_where('User', ['ID' => $data['order']->UserID], 1)->row();
        
        $this->db->select('*, op.Price');
        $this->db->from('OrderProduct as op');
        $this->db->where('op.OrderID', $data['order']->OrderID);
        $data['order_products'] = $this->db->get()->result();
        
        $this->render('transporter/order', $data);
    }
    
    public function change_order_status()
    {
        $orderID = $this->input->get('orderID');
        $status = $this->input->get('status');
        
        $this->db->update('Order', ['Status' => $status], ['ID' => $orderID], 1);
    }
    
    public function edit_transporter()
    {
        $user_id = $this->_user->ID;
        $this->config->load('regions');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $data['user'] = $this->db->get_where('User', ['ID' => $user_id], 1)->row();
        
        if (empty($data['user']->UserName) || $data['user']->UserName != $this->input->post('UserName'))
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
            $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        }
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
        
        $password = $this->input->post('Password');
        if (!empty($password))
        {
            $this->form_validation->set_rules('Password', lang('Password'), 'matches[PasswordConfirmation]');
        }
        
        $this->form_validation->set_rules('ClientType', lang('ClientType'), 'required');
        $this->form_validation->set_rules('Region', lang('Region'), 'required|is_natural');
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('ClientType');
            $address = $this->input->post('Address');
            
            if (is_null($address))
            {
                $address = $this->config->item($this->input->post('Region'), 'regions');
            }
            
            $userData = [
                'UserName' => $this->input->post('UserName'),
                'Name' => $this->input->post('Name'),
                'Type' => $clientType,
                'Status' => $clientType == 'Angro' ? 'NotConfirmed' : 'Active',
                'Region' => $this->input->post('Region'),
                'Address' => $address,
                'CompanyName' => $this->input->post('CompanyName'),
                'Phone' => $this->input->post('Phone'),
                'Zip' => $this->input->post('Zip'),
                'Activitation' => $this->input->post('Activitation'),
                'RegDate' => date('c')
            ];
            
            if (!empty($password))
            {
                $userData['Password'] = $password;
            }
            
            if ($user_id > 0)
            {
                $this->db->update('User', $userData, ['ID' => $user_id], 1);
            }
            else
            {
                $this->db->insert('User', $userData);
                $user_id = $this->db->insert_id();
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect('admin/edit_transporter/' . $user_id);
        }
        
        
        
        
        $this->load->model('RouteModel');
        $data['routes'] = $this->RouteModel->getByUserID($user_id, $this->langID);
        
        $this->render('admin/edit_transporter', $data);
    }
    
    public function routes()
    {
        $this->load->model('RouteModel');
        $data['routes'] = $this->RouteModel->getByUserID($this->_user->ID, $this->langID);        
        
        $this->render('transporter/routes', $data);
    }

    public function route_edit($id = 0)
    {
        $this->load->model('RouteModel');
        $this->load->model('CountryModel');
        $this->load->model('CityModel');
        $this->load->model('UserModel');
        
        $userID = $this->_user->ID;
        $route = [];
        
        if($id){
            $route = $this->RouteModel->getByID($id);
            
            // prevent to edit by diferent user
            if($route->UserID != $userID){
                redirect(site_url('transporter/routes'));
            }
        }
        
        
        $buses = $this->db->get_where('TransporterBus', ['UserID' => $userID])->result();
        //$buses = $this->db->get('TransporterBus')->result();
        
        $bus_select_data = [];
        foreach ($buses as $bus)
        {
            $bus_select_data[$bus->ID] = $bus->Name;
        }
        
        $this->db->select('*, TIME_FORMAT(Time, "%H:%i") as Time');
        $this->db->from('RouteStation');
        $this->db->where('RouteID', $id);
        $this->db->order_by('Position');
        $stations = $this->db->get()->result();
        
        $this->load->helper('form');
        
        // get route trips
        $trips = $this->db->select('YEAR(Date) as y, (MONTH(Date) - 1) as m, DAY(Date) as d')->from('RouteTrip')->where('RouteID', $id)->get()->result();
        
        if ($this->input->post())
        {
            $countryFrom = $this->input->post('CountryFrom');
            $cityFrom = $this->input->post('CityFrom');
            $price = $this->input->post('Price');
            $dayFrom = $this->input->post('DayFrom');
            $timeFrom = $this->input->post('TimeFrom');
            $countryTo = $this->input->post('CountryTo');
            $cityTo = $this->input->post('CityTo');
            $dayTo = $this->input->post('DayTo');
            $timeTo = $this->input->post('TimeTo');
            $busID = $this->input->post('TransporterBusID');
            $price = $this->input->post('Price');
                        
            $data = [
                'UserID' => $this->_user->ID,
                'TransporterBusID' => $busID,
                'Price' => $price,
                'CountryFrom' => $countryFrom,
                'CountryTo' => $countryTo,
                'CityFrom' => $cityFrom,
                'CityTo' => $cityTo,               
                'Status' => 'Active'
            ];
            
            if ($id > 0)
            {
                $this->db->update('Route', $data, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Route', $data);
                $id = $this->db->insert_id();
            }
            
            $this->db->delete('RouteStation', ['RouteID' => $id]);
            
            $position = 0;
            $stations_insert = [];
            $stations_insert[] = [
                'RouteID' => $id,
                'CountryID' => $countryFrom,
                'CityID' => $cityFrom,
                'Position' => $position,
                'Day' => $dayFrom,
                'Time' => $timeFrom,
                'Type' => 'Start'
            ];
            $position++;
            
            $countries = (array)$this->input->post('CountryID');
            $cities = (array)$this->input->post('CityID');
            $days = (array)$this->input->post('Day');
            $time = (array)$this->input->post('Time');
            
            foreach ($cities as $key => $value)
            {
                $stations_insert[] = [
                    'RouteID' => $id,
                    'CountryID' => $countries[$key],
                    'CityID' => $value,
                    'Position' => $position,
                    'Day' => $days[$key],
                    'Time' => $time[$key],
                    'Type' => 'Middle'
                ];
                $position++;
            }
            
            $stations_insert[] = [
                'RouteID' => $id,
                'CountryID' => $countryTo,
                'CityID' => $cityTo,
                'Position' => $position,
                'Day' => $dayTo,
                'Time' => $timeTo,
                'Type' => 'End'
            ];
            
            $this->db->insert_batch('RouteStation', $stations_insert);
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . 'Данные сохранены' . '</div>');
            
            redirect(site_url('transporter/route_edit/' . $id));
        }
        
        
        $this->render('transporter/route_edit', [
            'route' => $route,
            'buses' => $bus_select_data,
            'stations' => $stations,
            'trips' => $trips
        ]);
    }
    
    public function save_shedule($routeID)
    {
        $data = $this->input->post('data');
                        
        $this->db->delete('RouteTrip', ['RouteID' => $routeID]);
        
        $trip_insert = [];
        foreach ($data as $row)
        {
            $trip_insert[] = [
                'RouteID' => $routeID,
                'TransporterBusID' => 1,
                'Date' => $row['date']
            ];
        }        
        
        if (count($trip_insert) > 0)
        {
            $this->db->insert_batch('RouteTrip', $trip_insert);
        }
    }
    
    public function buses()
    {
        $buses = $this->db->get_where('TransporterBus', ['UserID' => $this->_user->ID])->result();
        
        $this->render('transporter/buses', [
            'buses' => $buses
        ]);
    }
    
    public function get_custom_seat_price(){
         if (!$this->input->is_ajax_request()) exit;
         
         $routeID = (int)$this->input->post('id');
         
         if($routeID){
             $return = $this->db->select("sp.*")
                      ->from("SeatPrice sp")
                      ->join("Route r", "r.ID = sp.RouteID", "LEFT")
                      ->where(["sp.RouteID" => $routeID, "r.UserID" => $this->_user->ID ])
                      ->get()->result();
             
             echo json_encode($return);             
         }else{
             echo json_encode([]);
         }
    }
    
    
    public function update_seat_price(){
         if (!$this->input->is_ajax_request()) exit;
         
         $routeID = (int)$this->input->post('id');
         $seat = (int)$this->input->post('seat');
         $price = (float)$this->input->post('price');
         
         if($routeID){
            // check if route is transporter property
            $return = $this->db->select("count(ID) as countR")
                      ->from("Route r")
                      ->where(["r.ID" => $routeID, "r.UserID" => $this->_user->ID ])
                      ->limit(1)
                      ->get()->row();
             
            if($return->countR == 1){
                 
                $custom_seat = $this->db->select("count(*) as countS")
                                        ->from("SeatPrice sp")
                                        ->where(["sp.RouteID" => $routeID, "sp.Seat" => $seat])
                                        ->limit(1)
                                        ->get()->row();
                 
                if($custom_seat->countS == 1){// Seat special price exists
                    $this->db->update('SeatPrice', ['Price'=>$price], ['RouteID' => $routeID, 'Seat' => $seat]);
                    echo json_encode(1);
                    exit();
                }elseif($custom_seat->countS == 0){ // New seat special price
                    $this->db->insert('SeatPrice', ['RouteID'=>$routeID, 'Seat'=>$seat, 'Price'=>$price]);
                    echo json_encode(1);
                    exit();
                }
                 
                
            }           
        }
         
        echo json_encode(0);
    }
    
    public function delete_seat_price(){
         if (!$this->input->is_ajax_request()) exit;
         
         $routeID = (int)$this->input->post('id');
         $seat = (int)$this->input->post('seat');
         
         if($routeID){
            // check if route is transporter property
            $return = $this->db->select("count(ID) as countR")
                      ->from("Route r")
                      ->where(["r.ID" => $routeID, "r.UserID" => $this->_user->ID ])
                      ->limit(1)
                      ->get()->row();
             
            if($return->countR == 1){
                 
                $custom_seat = $this->db->select("count(*) as countS")
                                        ->from("SeatPrice sp")
                                        ->where(["sp.RouteID" => $routeID, "sp.Seat" => $seat])
                                        ->limit(1)
                                        ->get()->row();
                 
                if($custom_seat->countS == 1){// Seat special price exists
                    $this->db->delete('SeatPrice', ['RouteID' => $routeID, 'Seat' => $seat]);
                    echo json_encode(1);
                    exit();
                }
                 
                
            }
            
        }
        echo json_encode(0);
    }
    
    
    
    
    
    public function transporter_buses_list($userID)
    {
        $data['buses'] = $this->db->get_where('TransporterBus', ['UserID' => $this->_user->ID])->result();
        
        echo $this->load->view('transporter/buses_list', $data, true);
    }
    
    public function edit_bus_form()
    {
        $id = (int) $this->input->post('id');
        
        $data['bus'] = $this->db->get_where('TransporterBus', ['ID' => $id], 1)->row();
        $data['bus_images'] = $this->db->get_where('TransporterBusImage', ['TransporterBusID' => $id])->result();
        $this->config->load('bus_templates');
        $data['templates'] = $this->config->item('bus_templates');
        
        echo $this->load->view('transporter/edit_bus_form', $data, true);
    }
    
    public function bus_save()
    {
        $ID = (int)$this->input->post('ID');
        $UserID = $this->_user->ID;
        $Name = $this->input->post('Name');
        $Template = $this->input->post('Template');
        
        $data = [
            'UserID' => $UserID,
            'Name' => $Name,
            'Template' => $Template
        ];
        
        if ($ID > 0)
        {
            $this->db->update('TransporterBus', $data, ['ID' => $ID]);
        }
        else
        {
            $this->db->insert('TransporterBus', $data);
            $ID = $this->db->insert_id();
        }
        
        $config = array(
            'upload_path'   => 'public/uploads/buses',
            'allowed_types' => 'jpg|gif|png',
            'encrypt_name' => true
        );
        $this->load->library('upload', $config);

        $images = array();
        foreach ($_FILES['Images']['name'] as $key => $image)
        {
            $_FILES['image']['name']= $_FILES['Images']['name'][$key];
            $_FILES['image']['type']= $_FILES['Images']['type'][$key];
            $_FILES['image']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
            $_FILES['image']['error']= $_FILES['Images']['error'][$key];
            $_FILES['image']['size']= $_FILES['Images']['size'][$key];

            $this->upload->initialize($config);

            if ($this->upload->do_upload('image'))
            {
                $images[] = $this->upload->data();
            }
        }

        if (count($images) > 0)
        {
            $images_insert = [];
            foreach ($images as $img)
            {
                $images_insert[] = [
                    'TransporterBusID' => $ID,
                    'Image' => $img['file_name']
                ];
            }
            $this->db->insert_batch('TransporterBusImage', $images_insert);
        }
    }
    
    public function delete_bus_image()
    {
        $ID = $this->input->post('id');
        
        $this->db->delete('TransporterBusImage', ['ID' => $ID], 1);
    }
    
}