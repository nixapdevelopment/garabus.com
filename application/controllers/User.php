<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends UserController
{
    
    public $backend = false;
    
    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('UserType') != "User") redirect ('/');
        
        $this->header = 'layouts/header';
        $this->footer = 'layouts/footer';
        
        $this->breadscrumbs = [];
        $this->addBreadscrumb('user', lang('Dashboard'));
        
        $this->menuType = 'User';
    }

    public function index()
    {
        $this->orders();
    }
    
    public function orders()
    {
        $this->addBreadscrumb('', lang('Orders'));
        
        $this->load->helper('form');
        
        $Date = $this->input->get('Date');
        $PaymentType = $this->input->get('PaymentType');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, cfl.Name as CityFrom, ctl.Name as CityTo, o.Status as OrderStatus, o.Date as Date', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->join('OrderProduct as op', 'op.OrderID = o.ID', 'LEFT');
        $this->db->join('Route as r', 'r.ID = op.RouteID', 'LEFT');
        $this->db->join('CityLang as cfl', 'cfl.CityID = r.CityFrom AND cfl.LangID='.$this->langID, 'LEFT');
        $this->db->join('CityLang as ctl', 'ctl.CityID = r.CityTo AND ctl.LangID='.$this->langID, 'LEFT');
        $this->db->order_by('o.Date', 'DESC');
        $this->db->where('o.UserID', $this->_user->ID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($PaymentType))$this->db->where('o.PaymentType', $PaymentType);
        if (!empty($Status))$this->db->where('o.Status', $Status);
        $this->db->group_by('o.ID');
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['orders'] = $this->db->get()->result();
        
        $total_orders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('user/orders', [], true);
        $config['total_rows'] = $total_orders;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('user/orders', $data);
    }
    
    public function order($order_id = false)
    {
        if (!$order_id) redirect('user/orders');
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.ID', $order_id);
        $this->db->where('o.UserID', $this->session->userdata('UserID'));
        $data['order'] = $this->db->get()->row();
                        
        if (empty($data['order'])) redirect('user/orders');
        // prevent to edit by diferent user
        if($data['order']->UserID != $this->_user->ID){
            redirect(site_url('user/orders'));
        }
        
        $this->config->load('regions');
        
        $this->addBreadscrumb(site_url('user/orders'), lang('Orders'));
        $this->addBreadscrumb('', '# ' . $data['order']->OrderID);
        
        $data['client'] = $this->db->get_where('User', ['ID' => $data['order']->UserID], 1)->row();
                
        $this->db->select('*, op.Price, cfl.Name as CityFrom, ctl.Name as CityTo');
        $this->db->from('OrderProduct as op');
        $this->db->join('Route as r', 'r.ID = op.RouteID', 'LEFT');
        $this->db->join('CityLang as cfl', 'cfl.CityID = r.CityFrom AND cfl.LangID='.$this->langID, 'LEFT');
        $this->db->join('CityLang as ctl', 'ctl.CityID = r.CityTo AND ctl.LangID='.$this->langID, 'LEFT');
        $this->db->where('op.OrderID', $data['order']->OrderID);
        $data['order_products'] = $this->db->get()->result();
        
//        print "<pre>".$this->db->last_query();
//        print_r($data['order_products']);
//        exit();
        
        $this->render('user/order', $data);
    }
    
    public function settings()
    {
        $this->load->model('CountryModel');
        $this->load->model('CityModel');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $data['user'] = $this->db->get_where('User', ['ID' => $this->_user->ID], 1)->row();
        
        // get countries
        $countries = [];        
        $countries_array = $this->CountryModel->getList($this->langID);        
        foreach ($countries_array as $country){
            $countries[$country->ID] = $country->Name;
        }
        $data['countries'] = $countries;
        
        // get cities
        $cities = [];
        $cities_array = $this->CityModel->getListByCountry($data['user']->CountryID, $this->langID);
        foreach ($cities_array as $city){
            $cities[$city->ID] = $city->Name;
        }
        $data['cities'] = $cities;
        
//        if (empty($data['user']->UserName) || $data['user']->UserName != $this->input->post('UserName'))
//        {
//            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
//            $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
//        }
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
   
        $password = $this->input->post('Password');
        if (!empty($password))
        {
            $this->form_validation->set_rules('Password', lang('Password'), 'trim|matches[PasswordConfirmation]');
            $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'trim');
        }
        
        $this->form_validation->set_rules('Country', lang('Country'), 'required|is_natural');
        $this->form_validation->set_rules('City', lang('City'), 'required|is_natural');
        $this->form_validation->set_rules('Phone', lang('Phone'), 'trim|required');
        
        
        if ($this->form_validation->run())
        {              
            $userData = [
                //'UserName' => $this->input->post('UserName'),
                'Name' => $this->input->post('Name'),
                'CountryID' => $this->input->post('Country'),
                'CityID' => $this->input->post('City'),                
                'Phone' => $this->input->post('Phone')
            ];
            
            if (!empty($password))
            {
                $userData['Password'] = $password;
            }
            
            $this->db->update('User', $userData, ['ID' => $this->_user->ID], 1);
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect('user/settings');
        }
        
        $this->render('user/user', $data);
    }
    
}