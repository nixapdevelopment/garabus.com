<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends FrontController
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('CategoryModel');
        $this->data['categories'] = $this->CategoryModel->getTree($this->langID, true);
        
        if (!empty($_SESSION['cart']) > 0)
        {
            foreach ($_SESSION['cart'] as $pID => $data)
            {
                $this->cartAmount += $_SESSION['cart'][$pID]['price'] * $_SESSION['cart'][$pID]['quantity'];
            }
        }
    }

    public function index()
    {
        $this->load->model('ProductModel');
        
        //$data['promo_products'] = $this->ProductModel->getPromoProducts($this->langID);
        //$data['last_products'] = $this->ProductModel->getLastProducts($this->langID);
        
        $this->load->model('NewsModel');
        
        $data['last_news'] = $this->NewsModel->getLast($this->langID);
        
        $data['slides'] = $this->db->select('*')->from('Slider')->order_by('Position')->get()->result();
        
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'");
        $this->db->where('p.ID', 1);
        $this->db->limit(1);
        $data['main_page'] = $this->db->get()->row();
        
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'");
        $this->db->where('p.ID', 3);
        $this->db->limit(1);
        $data['news_page'] = $this->db->get()->row();
        
        $this->load->model('RouteModel');        
        $data['top_routes'] = $this->RouteModel->getTopRoutes($this->langID);
               
        $this->render('home', $data);
    }
    
    public function login()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'required|valid_email');
        $this->form_validation->set_rules('Password', lang('Password'), 'required');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('Password', '', 'callback_do_user_login');
            
            $this->form_validation->run();
        }
        
        $this->addBreadscrumb('', lang('Login'));
        
        $this->render('login');
    }
    
    public function do_user_login()
    {
        $this->load->model('UserModel');
        
        $username = $this->input->post('UserName');
        $password = $this->input->post('Password');
        
        $user = $this->UserModel->getForLogin($username, $password);
        
        if (isset($user->ID))
        {
            $this->session->set_userdata('UserID', $user->ID);
            $this->session->set_userdata('UserType', $user->Type);
            
            switch ($user->Type)
            {
                case 'User':
                    redirect(site_url('user'));
                    break;
                case 'Transporter':
                    redirect(site_url('transporter'));
                    break;
                case 'Admin':
                    redirect(base_url('ru/admin'));
                    break;
                case 'Manager':
                    redirect(site_url('admin/index'));
                    break;
                case 'Seo':
                    redirect(site_url('admin'));
                    break;
            }
        }
        
        $this->form_validation->set_message('do_user_login', lang('InvalidLogin'));
        
        return false;
    }

    public function password_recovery()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'callback_do_password_recovery');
            
            $this->form_validation->run();
        }
        
        $this->addBreadscrumb('', lang('PasswordRecovery'));
        
        $this->render('password_recovery');
    }
    
    public function do_password_recovery()
    {
        $this->load->model('UserModel');
        
        $username = $this->input->post('UserName');
        
        $user = $this->UserModel->getByUsername($username);
        
        if (isset($user->ID))
        {
            $token = md5(uniqid());
            $this->db->update('User', ['Token' => $token], ['ID' => $user->ID]);
            
            $this->load->library('email');
            $this->email->from('system@agm.md', 'AGM');
            $this->email->to($user->UserName);
            $this->email->subject(lang('RecoveryMessageSubject'));
            $this->email->message('<a href="' . site_url('reset-password', ['token' => $token]) . '">Link</a> Token: ' . $token);
            $this->email->send();
            
            redirect('reset-password');
        }
        
        $this->form_validation->set_message('do_password_recovery', lang('EmailNotRegistered'));
        
        return false;
    }
    
    public function registration()
    {               
        
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        
        // get countries
        $this->load->model('CountryModel');
        $countries = [];
        $countries_array = $this->CountryModel->getList($this->langID);
        
        foreach ($countries_array as $country){
            $countries[$country->ID] = $country->Name;
        }
        
        $data['countries'] = $countries;
                
        if($this->input->post('UserType') == 'Transporter' )
        {
            $this->form_validation->set_rules('CompanyName', lang('CompanyName'), 'trim|required');
            $this->form_validation->set_rules('IDNO', lang('IDNO'), 'trim|required');
            $this->form_validation->set_rules('BankName', lang('BankName'), 'trim|required');
            $this->form_validation->set_rules('IBAN', lang('IBAN'), 'trim|required');
            $this->form_validation->set_rules('Address', lang('Address'), 'trim|required');
        }
        
        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
        $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        $this->form_validation->set_rules('Country', lang('Country'), 'required|is_natural');
        $this->form_validation->set_rules('City', lang('City'), 'required|is_natural');
        $this->form_validation->set_rules('Phone', lang('Phone'), 'trim|required');
        $this->form_validation->set_rules('Terms', lang('Terms'), 'required');       

        
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('UserType') == 'Transporter' ? 'Transporter' : 'User';
            
            $sql_insert = [];
            $insert = [
                'UserName' => $this->input->post('UserName'),
                'Password' => $this->input->post('Password'),
                'Name' => $this->input->post('Name'),
                'CountryID' => $this->input->post('Country'),
                'CityID' => $this->input->post('City'),
                'Phone' => $this->input->post('Phone'),
                'Type' => $clientType,
                'RegDate' => date('c')
            ];
            
            if($clientType == 'User'){
                $insert_user = ['Status' => 'Active'];
                $sql_insert = array_merge($insert, $insert_user);
                
            }elseif($clientType == 'Transporter'){
                $insert_trans = [                    
                    'CompanyName' => $this->input->post('CompanyName'),
                    'IDNO' => $this->input->post('IDNO'),
                    'BankName' => $this->input->post('BankName'),
                    'IBAN' => $this->input->post('IBAN'),
                    'Status' => 'NotConfirmed',                    
                    'Address' => $this->input->post('Address')                    
                ];
                $sql_insert = array_merge($insert, $insert_trans);
            }
            
            $this->db->insert('User', $sql_insert);
            $user_id = $this->db->insert_id();
            
            $this->session->set_userdata('UserID', $user_id);
            $this->session->set_userdata('UserType', $clientType);
            
            if ($clientType == 'Transporter')
            {
                redirect('wait-confirmation');
            }
            
            redirect('user');
        }
        
        $this->addBreadscrumb('', lang('Registration'));
        
        $this->render('registration', $data);
    }
    
    public function wait_confirmation()
    {
        $this->addBreadscrumb('', lang('WaitConfirmation'));
        
        $this->render('wait_confirmation');
    }
    
    public function logout()
    {
        $this->session->unset_userdata('UserID');
        redirect();
    }
    
    public function reset_password()
    {
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('Token', lang('RecoveryToken'), 'trim|required|callback_check_recovery_token');
        $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        
        if ($this->form_validation->run())
        {
            $token = $this->input->post('Token');
            
            $update = [];
            $update['Token'] = '';
            $update['Password'] = $this->input->post('Password');
            
            $this->db->update('User', $update, ['Token' => $token]);
            
            $this->session->set_flashdata('password_recovered', true);
            
            redirect('login');
        }
        
        $this->addBreadscrumb('', lang('ResetPassword'));
        
        $this->render('reset_password');
    }
    
    public function check_recovery_token()
    {
        $token = $this->input->post('Token');
        
        $user = $this->db->get_where('User', ['Token' => $token])->row();
        
        if (isset($user->ID))
        {
            return true;
        }
        
        $this->form_validation->set_message('check_recovery_token', lang('InvalidRecoveryToken'));
        
        return false;
    }
    
    public function subscribe()
    {
        $email = $this->input->post('Email');
        
        $subscribe = $this->db->get_where('Subscribe', ['Email' => $email], 1)->row();
        
        if (isset($subscribe->Email))
        {
            exit('<div class="text-danger">' . lang('AllreadySubscribed') . '</div>');
        }
        
        $this->db->insert('Subscribe', ['Email' => $email]);
        
        exit('<div class="text-success">' . lang('SubscribeSuccess') . '</div>');
    }
    
    public function router($url = '/')
    {
        $url = $this->db->get_where('Url', ['Link' => $url], 1)->row();
        
        if (empty($url->ID))
        {
            show_404();
        }
        
        switch ($url->Type) 
        {
            case 'Page':
                $this->page($url->ObjectID);
                break;
            case 'News':
                $this->article($url->ObjectID);
                break;
            case 'Category':
                $this->category($url->ObjectID);
                break;
            case 'Product':
                $this->product($url->ObjectID);
                break;
            default:
                show_404();
                break;
        }
    }
    
    private function page($id)
    {
        // get page
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'", 'LEFT');
        $this->db->where('p.ID', $id);
        $this->db->limit(1);
        $page = $this->db->get()->row();
        
        $this->seo = $page;
        
        // if page not found or not active
        if (empty($page->ID) || $page->Status != 'Active')
        {
            show_404();
        }
        
        $data['page'] = $page;
        
        // get images
        $data['images'] = $this->db->get_where('PageImage', ['PageID' => $page->ID])->result();
        
        // get files
        $data['files'] = $this->db->get_where('File', ['EntityID' => $page->ID, 'EntityType' => 'Page'])->result();
        
        $this->addBreadscrumb('', $page->Title);
        
        switch ($page->Template)
        {
            case 'home';
                $this->index($data);
                break;
            case 'catalog';
                $this->catalog($data);
                break;
            case 'news';                
                $this->news($data);
                break;
            case 'contacts';
                $this->contacts($data);
                break;
            case 'chain';
                $this->chain($data);
                break;
            case 'new_products';
                $this->new_products($data);
                break;
            default:
                $this->render('page', $data);
                break;
        }
    }    
    
    private function news($data)
    {
        $limit = 8;
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = current_url();
        $config['total_rows'] = $this->db->count_all_results('News');
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->model('NewsModel');
        $data['last_news'] = $this->NewsModel->getLast($this->langID, $limit, $offset);
        
        $this->render('news', $data);
    }
   
        
    private function article($id)
    {
        $this->load->model('NewsModel');
        $data['NewsArticle'] = $this->NewsModel->getByID($id, $this->langID);
        $data['NewsImages'] = $this->NewsModel->getImagesByNewsID($id);
        $data['NewsFiles'] = $this->NewsModel->getFilesByNewsID($id);
        
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID);
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'");
        $this->db->where('p.ID', 3);
        $news_page = $this->db->get()->row();
        
        $this->addBreadscrumb($news_page->Link, $news_page->Title);
        $this->addBreadscrumb('', $data['NewsArticle']->Title);
        
        $this->render('news_item', $data);
    }
    
    private function category($id)
    {
        $this->load->model('CategoryModel');
        
        // get category
        $data['category'] = $this->CategoryModel->getByID($id, $this->langID);
        
        // get subcategories
        $data['subcategories'] = $this->CategoryModel->getByParent($id, $this->langID);
        
        // menu
        $data['categories_menu'] = $this->CategoryModel->getTree($this->langID, true, $data['category']->ID);
        
        // get category products
        $this->db->select('*');
        $this->db->from('Product');
        $this->db->where('CategoryID', $id);
        $this->db->where('Status', 'Active');
        $this->db->where_in('Type', ['All', $this->shopType]);
        $res = $this->db->get()->result();
        
        $productIDs = [0];
        $data['min_price'] = 0;
        $data['max_price'] = 0;
        $data['filters'] = [];
        foreach ($res as $row)
        {
            $productIDs[$row->ID] = $row->ID;
            
            $price = $row->IsPromo == 1 && strtotime($row->DiscountPriceStart) < time() && strtotime($row->DiscountPriceEnd) > time() ? $row->DiscountPrice : $row->Price;

            if ($data['min_price'] == 0 || $price < $data['min_price'])
            {
                $data['min_price'] = $price;
            }
            
            if ($data['max_price'] == 0 || $price > $data['max_price'])
            {
                $data['max_price'] = $price;
            }
        }
        
        // get filters
        $this->db->select('f.ID, f.Type, f.Unit, fl.Name, pfl.Value');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID', 'LEFT');
        $this->db->join('Filter as f', 'f.ID = pf.FilterID', 'LEFT');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID and fl.LangID = ' . $this->langID, 'LEFT');
        $this->db->where_in('pf.ProductID', $productIDs);
        $this->db->order_by('f.System, fl.Name');
        $res = $this->db->get()->result();
        
        foreach ($res as $row)
        {
            if (!isset($data['filters'][$row->ID]['min']))
            {
                $data['filters'][$row->ID]['min'] = 0;
            }
            if (!isset($data['filters'][$row->ID]['max']))
            {
                $data['filters'][$row->ID]['max'] = 0;
            }
            
            $data['filters'][$row->ID]['data'] = $row;
            $data['filters'][$row->ID]['results'][$row->Value] = $row->Value;
            $data['filters'][$row->ID]['min'] = $data['filters'][$row->ID]['min'] == 0 || $row->Value < $data['filters'][$row->ID]['min'] ? $row->Value : $data['filters'][$row->ID]['min'];
            $data['filters'][$row->ID]['max'] = $data['filters'][$row->ID]['max'] == 0 || $row->Value > $data['filters'][$row->ID]['max'] ? $row->Value : $data['filters'][$row->ID]['max'];
        }
        
        $this->render('category', $data);
    }
    
    public function ajaxProducts()
    {
        if (!$this->input->is_ajax_request()) exit;
        
        $categoryID = $this->input->post('categoryID');
        
        parse_str($this->input->post('query'), $where_arr);
        
        $query = $this->db->get('Filter');
        $filters = [];
        foreach ($query->result() as $filter)
        {
            $filters[$filter->ID] = $filter->Type;
        }
        
        $this->db->select("SQL_CALC_FOUND_ROWS count(p.ID) as `count`, pf.ProductID, pl.Name, pi.Thumb, 0 as IsPromo, u.Link, d.PromoType, p.CategoryID, d.Value, p.Status as ProductStatus, p.Type as ProductType, 
            CASE d.PromoType 
                WHEN 'Percent' THEN ROUND(p.Price - (p.Price * d.Value / 100), 2) 
                WHEN 'Amount' THEN ROUND(p.Price - d.Value, 2) 
                WHEN 'Price' THEN ROUND(d.Value, 2)
                ELSE ROUND(p.Price, 2)
            END AS `ProductPrice`
        ", false);
        $this->db->from('Product as p');
        $this->db->join('ProductFilter as pf', 'pf.ProductID = p.ID', 'LEFT');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID and pfl.LangID = ' . $this->langID, 'LEFT');
        //$this->db->join('Product as p', "p.ID = pf.ProductID and p.`Status` = 'Active' and p.Type IN ()", 'RIGHT');
        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'RIGHT');
        $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'RIGHT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'RIGHT');
        $this->db->join("Discount as d", "d.EntityID = p.ID and d.EntityType = 'Product' and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1", 'LEFT');
        
        // filters query
        $i = 0;
        if (!empty($where_arr['f']))
        {
            foreach ($where_arr['f'] as $key => $val)
            {
                if ($filters[$key] == 'Number')
                {
                    $i++;
                    $val = explode(',', $val);
                    $minvalue = isset($val[0]) ? (int)$val[0] : 0;
                    $maxvalue = isset($val[1]) ? (int)$val[1] : 99999999999;
                    $this->db->or_where("(`pf`.`FilterID` = $key AND (`pfl`.`Value` >= $minvalue AND `pfl`.`Value` <=  $maxvalue))");
                }
                elseif ($filters[$key] == 'String')
                {
                    $i++;
                    $in = implode("','", $val);
                    if (!empty($in))
                    {
                        $this->db->or_where("(`pf`.`FilterID` = $key AND `pfl`.`Value` IN ('$in'))");
                    }
                }
                elseif ($filters[$key] == 'Price')
                {
                    $val = explode(',', $val);
                    $minvalue = isset($val[0]) ? (int)$val[0] : 0;
                    $maxvalue = isset($val[1]) ? (int)$val[1] : 99999999999;
                    $this->db->having("(`ProductPrice` >= $minvalue AND `ProductPrice` <= $maxvalue)");
                }
            }
        }
        $this->db->having('count', $i);
        $this->db->having('CategoryID', $categoryID);
        $this->db->having('ProductStatus', 'Active');
        $this->db->having("ProductType IN('All', '" . $this->shopType . "')", null, false);
        
        $this->db->group_by('p.ID');
        
        // order query
        switch ($where_arr['sort'])
        {
            case 'name':
                $this->db->order_by('pl.Name', 'asc');
                break;
            case 'date':
                $this->db->order_by('p.ID', 'desc');
                break;
            default:
                $this->db->order_by('ProductPrice', 'asc');
                break;
        }
        
        // page and per_page query
        $where_arr['page'] = $where_arr['page'] > 0 ? $where_arr['page'] : 1;
        $this->db->limit($where_arr['per_page']);
        $this->db->offset(($where_arr['page'] - 1) * $where_arr['per_page']);
        
        $products = $this->db->get()->result();
        $query = $this->db->simple_query('select FOUND_ROWS()')->fetch_row();
        $count_all = reset($query);
        
        echo $this->load->view('ajax_products', [
            'products' => $products,
            'count' => $count_all,
            'page' => $where_arr['page'],
            'per_page' => $where_arr['per_page']
        ], true);
    }
    
    public function ajaxCities()
    {
        if (!$this->input->is_ajax_request()) exit;
        
        $countryID = (int)$this->input->post('country');
        
        $this->load->model('CityModel');
        $cities = $this->CityModel->getListByCountry($countryID, $this->langID);           
                           
        echo $this->load->view('ajax_cities', [
            'cities' => $cities
        ], true);
    }

    private function product($id)
    {
        $this->load->model('ProductModel');
        $product = $this->ProductModel->getByID($id);
        
        if (empty($product->ID)) redirect();
        
        // images
        $this->db->order_by('IsMain', 'DESC');
        $images = $this->db->get_where('ProductImage', ['ProductID' => $product->ProductID])->result();
        
        // filters
        $this->db->select('*');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID AND pfl.LangID = ' . $this->langID);
        $this->db->join('FilterLang as fl', 'fl.FilterID = pf.FilterID AND fl.LangID = ' . $this->langID);
        $this->db->where('pf.ProductID', $product->ProductID);
        $this->db->group_by('pf.FilterID');
        $filters = $this->db->get()->result();
        
        $this->render('product', [
            'product' => $product,
            'images' => $images,
            'filters' => $filters
        ]);
    }
    
    public function cart_add($update = 0)
    {
        $productID = $this->input->post('productID');
        $quantity = $this->input->post('quantity') ? $this->input->post('quantity') : 1;
        
        $this->db->select("*, p.ID as ProductID, IF(d.ID IS NOT NULL, 1, 0) as `IsPromo`, 
            CASE d.PromoType 
                WHEN 'Percent' THEN ROUND(p.Price - (p.Price * d.Value / 100), 2) 
                WHEN 'Amount' THEN ROUND(p.Price - d.Value, 2) 
                WHEN 'Price' THEN ROUND(d.Value, 2)
                ELSE ROUND(p.Price, 2)
            END AS `Price`");
        $this->db->from('Product as p');
        $this->db->join("Discount as d", "d.EntityID = p.ID and d.EntityType = 'Product' and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1", 'LEFT');
        $this->db->where('p.ID', $productID);
        $this->db->group_by('p.ID');
        $this->db->limit(1);
        $product = $this->db->get()->row();
        
        $amount = 0;
        $errors = [];
        if (($product->Stock - $quantity) < 0)
        {
            $errors[] = lang('OutOfStock');
        }
        
        if (count($errors) == 0)
        {
            if (isset($_SESSION['cart'][$product->ProductID]['quantity']))
            {
                $_SESSION['cart'][$product->ProductID]['quantity'] = $update ? $quantity : $_SESSION['cart'][$product->ProductID]['quantity'] + $quantity;
                $_SESSION['cart'][$product->ProductID]['price'] = $product->Price;
                
            }
            else
            {
                $_SESSION['cart'][$product->ProductID] = [
                    'quantity' => $quantity,
                    'price' => $product->Price
                ];
            }
        }
        
        if (count($_SESSION['cart']) > 0)
        {
            foreach ($_SESSION['cart'] as $pID => $data)
            {
                $amount += $_SESSION['cart'][$pID]['price'] * $_SESSION['cart'][$pID]['quantity'];
            }
        }
        
        exit(json_encode([
            'result' => count($errors) > 0 ? 'error' : 'success',
            'messages' => count($errors) > 0 ? implode('<br>', $errors) : ($update ? lang('CartChanged') : lang('AddedToCart')),
            'amount' => number_format($amount, 2)
        ]));
    }
    
    public function cart()
    {
        if (count($_POST) > 0)
        {
            redirect('checkout');
        }
        
        $cart = $this->session->userdata('cart');
        
        $contents = [];
        $total = 0;
        if (!empty($cart))
        {
            $cartProdIDs = array_keys($cart);
            
            $this->db->select("*, pl.Name as Name, cl.Name as CategoryName, IF(d.ID IS NOT NULL, 1, 0) as `IsPromo`, 
            CASE d.PromoType 
                WHEN 'Percent' THEN ROUND(p.Price - (p.Price * d.Value / 100), 2) 
                WHEN 'Amount' THEN ROUND(p.Price - d.Value, 2) 
                WHEN 'Price' THEN ROUND(d.Value, 2)
                ELSE ROUND(p.Price, 2)
            END AS `Price`");
            $this->db->from('Product as p');
            $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'LEFT');
            $this->db->join('CategoryLang as cl', 'p.CategoryID = cl.CategoryID and cl.LangID = ' . $this->langID, 'LEFT');
            $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'LEFT');
            $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
            $this->db->join("Discount as d", "d.EntityID = p.ID and d.EntityType = 'Product' and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1", 'LEFT');
            $this->db->where_in('p.ID', $cartProdIDs);
            $products = $this->db->get()->result();
            
            foreach ($products as $item)
            {
                $total += $cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'];
                $contents[$item->ProductID] = [
                    'ID' => $item->ProductID,
                    'Name' => $item->Name,
                    'CategoryName' => $item->CategoryName,
                    'Thumb' => $item->Thumb,
                    'Image' => $item->Image,
                    'Link' => $item->Link,
                    'Quantity' => $cart[$item->ProductID]['quantity'],
                    'Price' => $cart[$item->ProductID]['price'],
                    'Subtotal' => number_format($cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'], 2),
                    'Stock' => $item->Stock
                ];
            }
        }
        
        $this->render('cart', [
            'contents' => $contents,
            'total' => $total
        ]);
    }
    
    public function cart_delete()
    {
        $productID = $this->input->post('productID');
        
        $amount = 0;
        
        unset($_SESSION['cart'][$productID]);
        
        if (count($_SESSION['cart']) > 0)
        {
            foreach ($_SESSION['cart'] as $pID => $data)
            {
                $amount += $_SESSION['cart'][$pID]['price'] * $_SESSION['cart'][$pID]['quantity'];
            }
        }
        
        exit(json_encode([
            'result' => 'success',
            'messages' => lang('CartProductDeleted'),
            'amount' => number_format($amount, 2)
        ]));
    }
    
    public function checkout()
    {
        if (!isset($_SESSION['cart']) || count($_SESSION['cart']) == 0)
        {
            redirect('cart');
        }
     
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->config->load('regions');
        
        $user = $this->db->get_where('User', ['ID' => $this->session->userdata('UserID')], 1)->row();
        
        $cart = $this->session->userdata('cart');
        
        $contents = [];
        $total = 0;
        
        $cartProdIDs = array_keys($cart);

        $this->db->select('*, pl.Name as Name, cl.Name as CategoryName');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('CategoryLang as cl', 'p.CategoryID = cl.CategoryID and cl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'LEFT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
        $this->db->where_in('p.ID', $cartProdIDs);
        $products = $this->db->get()->result();

        foreach ($products as $item)
        {
            $total += $cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'];
            $contents[$item->ProductID] = [
                'ID' => $item->ProductID,
                'Name' => $item->Name,
                'CategoryName' => $item->CategoryName,
                'Thumb' => $item->Thumb,
                'Image' => $item->Image,
                'Link' => $item->Link,
                'Quantity' => $cart[$item->ProductID]['quantity'],
                'Price' => $cart[$item->ProductID]['price'],
                'Subtotal' => number_format($cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'], 2),
                'Stock' => $item->Stock
            ];
        }
        
        $this->form_validation->set_rules('name', lang('Name'), 'required');
        $this->form_validation->set_rules('region', lang('Region'), 'required|integer');
        $this->form_validation->set_rules('address', lang('Address'), 'required');
        $this->form_validation->set_rules('zip', lang('Zip'), 'required');
        $this->form_validation->set_rules('phone', lang('Phone'), 'required');
        $this->form_validation->set_rules('email', lang('Email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('payment', lang('PaymentType'), 'required');
        
        if ($this->form_validation->run())
        {
            $paymentType = $this->input->post('payment') == 'card' ? 'Card' : 'Cash';
            $order = [
                'UserID' => $this->session->userdata('UserID'),
                'Amount' => $total,
                'Date' => date('c'),
                'Type' => $this->shopType,
                'PaymentType' => $paymentType,
                'Status' => 'Pending'
            ];
            $this->db->insert('Order', $order);
            $order_id = $this->db->insert_id();
            
            $orderDetails = [
                'OrderID' => $order_id,
                'Name' => $this->input->post('name'),
                'Region' => $this->input->post('region'),
                'Address' => $this->input->post('address'),
                'Zip' => $this->input->post('zip'),
                'Phone' => $this->input->post('phone'),
                'Email' => $this->input->post('email'),
            ];
            $this->db->insert('OrderDetail', $orderDetails);
            
            $order_products = [];
            foreach ($cart as $productID => $cData)
            {
                $order_products[] = [
                    'OrderID' => $order_id,
                    'ProductID' => $productID,
                    'Price' => $cData['price'],
                    'Quantity' => $cData['quantity']
                ];
            }
            $this->db->insert_batch('OrderProduct', $order_products);
            
            $this->session->unset_userdata('cart');
            
            if ($paymentType == 'Cash')
            {
                redirect('thankyou/' . $order_id);
            }
            else
            {
                // redirect to cart processing
            }
        }
        
        $this->render('checkout', [
            'user' => $user,
            'contents' => $contents,
            'total' => $total
        ]);
    }
    
    public function thankyou($order_id = false)
    {
        if (!$order_id)
        {
            redirect('cart');
        }
        
        $order = $this->db->get_where('Order', ['ID' => $order_id], 1)->row();
        
        if (empty($order->ID) || ($order->Status == 'Paid' && $order->PaymentType == 'Cash'))
        {
            redirect('cart');
        }
        
        $this->render('thankyou', [
            'order' => $order
        ]);
    }
    
    public function catalog()
    {
        $this->db->select('*');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID =' . $this->langID);
        $this->db->join('Url as u', "u.ObjectID = c.ID and u.`Type` = 'Category'");
        $this->db->where('c.ParentID', 0);
        $this->db->order_by('cl.Name', 'DESC');
        $categories = $this->db->get()->result();
        
        $this->render('catalog', [
            'categories' => $categories
        ]);
    }
    
    public function promo()
    {
        $limit = 10;
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        // get products
        $this->load->model('ProductModel');
        $data['products'] = $this->ProductModel->getPromoProducts($this->langID, $limit, $offset);
        $all_promo_products = $this->db->select('FOUND_ROWS() as `all`', false)->get()->row()->all;
        
        if (count($data['products']) == 0 && $offset > 0)
        {
            redirect(site_url('promo'));
        }
        
        // pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url('promo');
        $config['total_rows'] = $all_promo_products;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('promo', $data);
    }
    
    public function contacts($data)
    {
        $this->render('contacts', $data);
    }
    
    public function chain($data)
    {
        $this->render('chain', $data);
    }
    
    public function new_products($data)
    {
        $this->load->model('ProductModel');
        
        $limit = 1;
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        $data['products'] = $this->ProductModel->getLastProducts($this->langID, $limit, $offset);
        
        $total_products = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url($data['page']->Link, [], true);
        $config['total_rows'] = $total_products;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('new_products', $data);
    }
    
    public function feedback()
    {
        $Name = $this->input->post('Name');
        $Email = $this->input->post('Email');
        $Phone = $this->input->post('Phone');
        $Message = $this->input->post('Message');
        
        $this->db->insert('Feedback', [
            'Name' => $Name,
            'Email' => $Email,
            'Phone' => $Phone,
            'Message' => strip_tags($Message)
        ]);
    }
    
    public function search()
    {        
        $this->load->model('RouteModel');
        $this->load->model('RouteSeoModel');
        
        $from = (int)$this->input->get('from');
        $to = (int)$this->input->get('to');
        $date = $this->input->get('date') ? date('Y-m-d', strtotime($this->input->get('date'))) : date('Y-m-d');
        $persons = (int)$this->input->get('persons');
        
        $routes = $this->RouteModel->search($from, $to, $date, $persons, $this->langID);
        
        $this->seo = $this->RouteSeoModel->getByCity($from, $to, $this->langID);
        
        $this->render('search', [
            'routes' => $routes
        ]);
    }
    
    public function search_redirect()
    {
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $date = $this->input->get('date');
        $persons = $this->input->get('persons');
        
        $this->load->model('CityModel');
        
        $city_from = $this->CityModel->getByID($from, $this->langID);
        $city_to = $this->CityModel->getByID($to, $this->langID);
        
        redirect(site_url('search/' . str_to_url($city_from->Name, '_') . '-' . str_to_url($city_to->Name, '_'), [
            'from' => $city_from->ID,
            'to' => $city_to->ID,
            'date' => $date,
            'persons' => $persons,
        ]));
    }
    
    public function ajax_cities()
    {
        $data = $this->input->get('term');
        
        $this->db->select('CityID');
        $this->db->from('CityLang');
        $this->db->like('Name', $data['term']);
        $res = $this->db->get()->result();
        
        $ids = [0];
        foreach ($res as $row)
        {
            $ids[$row->CityID] = $row->CityID;
        }
        
        $this->db->select('CityID, Name');
        $this->db->from('CityLang');
        $this->db->where('LangID', $this->langID);
        $this->db->where_in('CityID', $ids);
        $this->db->order_by('Name');
        $res = $this->db->get()->result();
        
        $return = [];
        foreach ($res as $row)
        {
            $return['items'][] = [
                'id' => $row->CityID,
                'text' => $row->Name,
            ];
        }
        
        echo json_encode($return);
    }
    
    public function route_info()
    {
        $user = [];
        if($this->session->userdata('UserID'))
        {
            $this->load->model('UserModel');
            $user = $this->UserModel->getByID($this->session->userdata('UserID'));
        }
        $id = $this->input->post('id');
        
        $date = $this->input->post('date');
        
        if (empty($date))
        {
            $date = date('Y-m-d');
        }
        
        $this->load->model('RouteModel');        
        $route = $this->RouteModel->getByID($id);
        
        $transporter = $this->db->get_where('User', ['ID' => $route->UserID], 1)->row();
        
        $this->db->select('*, TIME_FORMAT(Time, "%H:%i") as Time');
        $this->db->from('RouteStation as rs');
        $this->db->join('CityLang as cl', 'cl.CityID = rs.CityID and cl.LangID = ' . $this->langID);
        $this->db->where('RouteID', $id);
        $this->db->order_by('Position');
        $stations = $this->db->get()->result();
        
        $bus = $this->db->get_where('TransporterBus', ['ID' => $route->TransporterBusID])->row();
        
        $bus_images = $this->db->get_where('TransporterBusImage', ['TransporterBusID' => $route->TransporterBusID])->result();
        
        // route info
        $data['info'] = $this->load->view('route_info', [
            'route' => $route,
            'stations' => $stations,
            'transporter' => $transporter,
            'user' => $user,
            'date' => $date,
            'bus' => $bus,
            'bus_images' => $bus_images
        ], true);
        
        // bus template and seats
        $this->db->select();
        $this->db->from('ReservedSeats');
        $this->db->where('RouteID', $route->ID);
        $this->db->where('Date', date('Y-m-d', strtotime($date)));
        $this->db->where('Expiration >', date('c'));
        $reserved_seats = $this->db->get()->result();
        
        $data['template'] = $this->load->view('buses/delfin', [
            'reserved_seats' => $reserved_seats
        ], true);
        
        echo json_encode($data);
    }
    
    public function do_order()
    {
        $data = [];
        parse_str($this->input->post('data'), $data);
        $payment_type = $this->input->post('payment_type');
        $date = $this->input->post('date');
        $date = $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        
        $fnames = $data['fname'];
        $lnames = $data['lname'];
        $prices = $data['price'];
        $seats = $data['seat'];
        
        $this->load->model('RouteModel');
        
        $route = $this->RouteModel->getByID($data['route']);
        
        switch ($payment_type)
        {
            case 'card':
                $payment_type = 'Card';
                break;
            case 'paypal':
                $payment_type = 'Paypal';
                break;
            default:
                $payment_type = 'Cash';
                break;
        }
        
        // reserve ticket time
        if ($payment_type == 'Card' || $payment_type == 'Paypal')
        {
            $reserved_time = '+30 minutes';
        }
        else
        {
            $reserved_time = '+20 years';
        }
        
        // calc price
        $total_price = 0;
        $order_product_insert = [];
        foreach ($prices as $pr)
        {
            $total_price += $route->Price;
        }
        
        // save order
        $this->db->insert('Order', [
            'UserID' => $this->session->userdata('UserID') ? $this->session->userdata('UserID') : null,
            'Amount' => $total_price,
            'Date' => date('c'),
            'PaymentType' => $payment_type,
            'Type' => 'All',
            'IsNew' => 1,
            'Status' => 'Pending'
        ]);
        $orderID = $this->db->insert_id();
        
        // save order detail
        $this->db->insert('OrderDetail', [
            'OrderID' => $orderID,
            'Name' => $data['cfname'] . ' ' . $data['clname'],
            'Region' => 1,
            'Address' => '-',
            'Phone' => $data['cphone'],
            'Email' => $data['cemail']
        ]);
        
        // save order products
        $op_insert = [];
        $rs_insert = [];
        foreach ($prices as $key => $pr)
        {
            $op_insert[] = [
                'OrderID' => $orderID,
                'ProductID' => null,
                'FirstName' => $fnames[$key],
                'LastName' => $lnames[$key],
                'Price' => $route->Price,
                'Quantity' => 1,
                'RouteID' => $route->ID,
                'Date' => $date,
                'Seat' => $seats[$key],
                'SeatType' => $pr == 'child' ? 'Child' : 'Adult'
            ];
            $rs_insert[] = [
                'RouteID' => $route->ID,
                'Date' => $date,
                'Seat' => $seats[$key],
                'Expiration' => date('c', strtotime($reserved_time))
            ];
        }
        $this->db->insert_batch('OrderProduct', $op_insert);
        $this->db->insert_batch('ReservedSeats', $rs_insert);
        
        // send email with tickets
        $pdf = file_get_contents(site_url('main/tickets/' . $orderID));
        $pdf_file = FCPATH . 'public/tmp/' . $orderID . '.pdf';
        file_put_contents($pdf_file, $pdf);
        
        $this->load->library('email');
        $this->email->from('oreder@garabus.com', 'GaraBus');
        $this->email->to($data['cemail']);
        $this->email->subject('Ваши билеты с garabus.com');
        $this->email->message('Текст сообщения');	
        $this->email->attach($pdf_file);
        $this->email->send();
        
        echo json_encode([
            'id' => $orderID,
            'type' => $payment_type
        ]);
    }
    
    public function template()
    {
        $template = $this->input->post('template');
        
        echo $this->load->view('buses/' . $template, [], true);
    }
    
    public function tickets($orderID = false)
    {
        $order = $this->db->get_where('Order', ['ID' => $orderID], 1)->row();
        
        $tickets = $this->db->get_where('OrderProduct', ['OrderID' => $order->ID])->result();
        
        $this->load->model('RouteModel');
        
        require_once APPPATH.'libraries/tcpdf/tcpdf.php';
        $this->load->library('tcpdf/TCPDF', null, 'pdf');
        $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->pdf->SetSubject('Tickets');
        $this->pdf->SetKeywords('Tickets');
        
        // set font
        $this->pdf->SetFont('dejavusans', '', 12);

        // disable header and footer
        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetPrintFooter(false);
        
        // add a page
        $this->pdf->AddPage();
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
        // draw
        foreach ($tickets as $ticket)
        {
            $route = $this->RouteModel->getByID($ticket->RouteID);
            
            $from_station = $this->db->get_where('RouteStation', ['RouteID' => $route->ID, 'Type' => 'Start'], 1)->row();
            $to_station = $this->db->get_where('RouteStation', ['RouteID' => $route->ID, 'Type' => 'End'], 1)->row();
            
            $html = $this->load->view('tickets/ticket', [
                'ticket' => $ticket,
                'route' => $route,
                'from_station' => $from_station,
                'to_station' => $to_station
            ], true);
            $this->pdf->writeHTML($html, true, false, true, false, '');
        }

        //Close and output PDF document
        $this->pdf->Output('tickets-' . $order->Date, 'I');
    }
    
    public function card($orderID)
    {
        $order = $this->db->get_where('Order', ['ID' => $orderID], 1)->row();
        
        if (empty($order->ID) || $order->PaymentType != 'Card' || $order->Status != 'Pending')
        {
            redirect(site_url());
        }
        
        $active_langs = $this->config->item('languages');
        
        $this->load->config('bpay');
        $protocolType = $this->config->item('protocol_type', 'bpay');
        $merchantId = $this->config->item('merchant_id', 'bpay');
        $signature = $this->config->item('signature', 'bpay');
        $orderID = $order->ID;
        $amount = number_format($order->Amount, 2, '.', '');
        
        $xmldata = "
            <payment>
                <type>$protocolType</type>
                <merchantid>$merchantId</merchantid>
                <amount>$amount</amount>
                <description>Покупка билетов на GaraBus</description>
                <method>card</method>
                <order_id>$orderID</order_id>
                <success_url>" . htmlspecialchars(site_url('thankyou/' . $orderID)) . "</success_url>
                <fail_url>" . htmlspecialchars(site_url('cancel/' . $orderID)) . "</fail_url>
                <callback_url>" . htmlspecialchars(site_url('main/card_callback/' . $orderID)) . "</callback_url>
                <lang>" . strtolower($active_langs[$this->langID]['Slug']) . "</lang>
                <advanced1></advanced1>
                <advanced2></advanced2>
                <istest>0</istest>
                <getUrl>0</getUrl>
            </payment>
        ";
        
        $data = base64_encode($xmldata);-
        $sign = md5(md5($xmldata) . md5($signature));
        
        $this->render('card_confirm', [
            'data' => $data,
            'sign' => $sign
        ]);
    }
    
    public function card_callback($orderID)
    {
        $this->load->config('bpay');
        $signature = $this->config->item('signature', 'bpay');
        
        $order = $this->db->get_where('Order', ['ID' => $orderID], 1)->row();
        
        $data = $this->input->post('data'); 
        $key = $this->input->post('key'); 
        $xmldata = base64_decode($data); 
        $vrfsign = md5(md5($xmldata) . md5($signature)); 
        
        if ($key == $vrfsign)
        {
            $xml = simplexml_load_string($xmldata);
            if ((string)$xml->comand == "check")
            {
                if (empty($order->ID))
                {
                    $code = 50;
                    $message = 'Order #' . $order->ID . ' not exist';
                }
                else
                {
                    $code = 100;
                    $message = 'Order #' . $order->ID . ' exist';
                }
                
                // проверка существования указанного order_id
                // 100 - номер существует, 50 - номер не существует
                echo "<?xml version='1.0' encoding=\"utf8\"?>";
                echo "<result>";
                echo "<code>$code</code>";
                echo "<text>$message</text>";
                echo "</result>";
            }
            elseif ((string)$xml->comand == "pay")
            {
                // set order as paid
                $this->db->update('Order', ['Status' => 'Paid'], ['ID' => $order->ID], 1);
                
                // здесь осуществляем обработку данного платежа
                echo "<?xml version='1.0' encoding=\"utf8\"?>";
                echo "<result>";
                echo "<code>100</code>";
                echo "<text>Success</text>";
                echo "</result>";
            }
            else
            {
                echo "<?xml version='1.0' encoding=\"utf8\"?>";
                echo "<result>";
                echo "<code>30</code>";
                echo "<text>Unknown method</text>";
                echo "</result>";
            }
        }
        else
        {
              echo "<?xml version='1.0' encoding=\"utf8\"?>";
              echo "<result>";
              echo "<code>30</code>";
              echo "<text>Incorrect signature</text>";
              echo "</result>";
        }
    }
    
    public function cancel($orderID)
    {
        $this->db->update('Order', ['Status' => 'Canceled'], ['ID' => $orderID, 'Status' => 'Pending'], 1);
        redirect(site_url());
    }
    
}