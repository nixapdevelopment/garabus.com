<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController
{
    
    public $menuType = '';
    
    public $backend = true;
    
    
    
    public function __construct()
    {
        parent::__construct();
        
        $this->menuType = $this->_user->Type;
        
        if ($this->_user->Type == 'Manager')
        {
            if (!in_array($this->uri->segment(2), ['', 'index', 'orders', 'order', 'users', 'user']))
            {
                redirect(site_url('admin/index'));
            }
        }
        
        if ($this->_user->Type == 'Seo')
        {
            if (!in_array($this->uri->segment(2), ['pages', 'news', 'seo', 'edit_news', 'edit_page', 'edit_seo', 'delete_history', 'delete_news_image', 'delete_file', 'delete_page_image', 'delete_page', 'delete_image', 'menus', 'cities', 'city_get', 'city_edit']))
            {
                redirect(site_url('admin/pages'));
            }
        }
    }

    public function index()
    {
        // counters
        $data['product_count'] = $this->db->where('Status <>', 'Deleted')->get('Product')->num_rows();
        $data['category_count'] = $this->db->where('Status <>', 'Deleted')->get('Category')->num_rows();
        $data['client_count'] = $this->db->where('Status <>', 'Deleted')->where_in('Type', ['Angro', 'Retail'])->get('User')->num_rows();
        
        // last orders
        $this->db->select('SQL_CALC_FOUND_ROWS *, o.ID', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->order_by('o.Date', 'DESC');
        $this->db->limit(10);
        $data['orders'] = $this->db->get()->result();
        
        // last transporters
        $this->db->select('*');
        $this->db->from('User');
        $this->db->where('Type', 'Transporter');
        $this->db->order_by('ID', 'DESC');
        $this->db->limit(10);
        $data['transporters'] = $this->db->get()->result();
        
        $this->render('admin/index', $data);
    }
    
    public function categories()
    {
        $nodeList = array();
        $tree     = array();

        $categories = $this->db->simple_query("select c.ID, c.ParentID, cl.Name from `Category` as c left join `CategoryLang` as cl on cl.LangID = " . $this->langID . " and c.ID = cl.CategoryID");
        
        foreach ($categories as $row)
        {
            $nodeList[$row['ID']] = array_merge($row, array('children' => array()));
        }

        foreach ($nodeList as $nodeId => &$node)
        {
            if (!$node['ParentID'] || !array_key_exists($node['ParentID'], $nodeList))
            {
                $tree[] = &$node;
            }
            else
            {
                $nodeList[$node['ParentID']]['children'][] = &$node;
            }
        }
        unset($node);
        unset($nodeList);
        
        $tree = $this->draw_tree($tree);
        
        if ($this->input->is_ajax_request())
        {
            exit($tree);
        }
        
        $this->render('admin/categories', [
            'tree' => $tree
        ]);
    }

    private function draw_tree($tree)
    {
        $return = '<ul>';
        foreach ($tree as $row)
        {
            $return .= '<li cat-id="' . $row['ID'] . '">';
            $return .= $row['Name'];
            if (!empty($row['children']))
            {
                $return .= $this->draw_tree($row['children']);
            }
            $return .= '</li>';
        }
        $return .= '</ul>';
        return $return;
    }

    public function get_category_form()
    {
        $this->load->helper('form');
        
        $categoryID = (int)$this->input->post('categoryID');
        $parentID = (int)$this->input->post('parentID');
        
        $data = [];
        $data['parentID'] = $parentID;
        $data['categoryID'] = $categoryID;
        $data['category_link'] = '';
        
        if ($categoryID > 0)
        {
            // get category
            $this->db->select('*');
            $this->db->from('Category');
            $this->db->where('ID', $categoryID);
            $this->db->where('Status !=', 'Deleted');
            $data['category'] = $this->db->get()->row();
            
            // get category langs
            $this->db->select('*');
            $this->db->from('CategoryLang');
            $this->db->where('CategoryID', $categoryID);
            $res = $this->db->get()->result();
            
            $data['category_langs'] = [];
            foreach ($res as $row)
            {
                $data['category_langs'][$row->LangID] = $row;
            }
            
            // get category url
            $url = $this->db->get_where('Url', ['Type' => 'Category', 'ObjectID' => $categoryID], 1)->row();
            $data['category_link'] = isset($url->Link) ? $url->Link : '';
        }
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][0] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }

        if ($categoryID > 0)
        {
            unset($data['all_categories'][$categoryID]);
        }
        
        echo $this->load->view('admin/category_form', $data, true);
    }
    
    public function save_category()
    {
        // get data
        $categoryID = $this->input->post('CategoryID');
        $parentID = $this->input->post('ParentID');
        $names = $this->input->post('Name');
        $titles = $this->input->post('Title');
        $texts = $this->input->post('Text');
        $keywords = $this->input->post('Keywords');
        $descriptions = $this->input->post('Description');
        $link = $this->input->post('Link');
        $status = $this->input->post('Status') == 'Disabled' ? 'Disabled' : 'Active';
        $type = $this->input->post('Type');
                print_r($texts); exit;
        // save category
        $category = [
            'ParentID' => $parentID,
            'Status' => $status,
            'Type' => $type
        ];
        
        // upload category image
        $config['upload_path'] = 'public/uploads/categories';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']	= '10000';
        $config['max_width']  = '4000';
        $config['max_height']  = '4000';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload('Image'))
        {
            $file = $this->upload->data();
            $category['Image'] = $file['file_name'];
        }
        
        if ($categoryID > 0)
        {
            $this->db->update('Category', $category, ['ID' => $categoryID]);
        }
        else
        {
            $this->db->insert('Category', $category);
            $categoryID = $this->db->insert_id();
        }
        
        // save url
        if (empty($link))
        {
            $link = str_to_url($names[1]);
        }
        
        $this->db->select('*');
        $this->db->from('Url');
        $res = $this->db->get()->result_array();
        
        $all_urls = [];
        $all_urls_ot = [];
        foreach ($res as $row)
        {
            $all_urls[$row['Link']] = $row;
            $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
        }
        
        if (isset($all_urls[$link]))
        {
            if ($all_urls[$link]['ObjectID'] != $categoryID || $all_urls[$link]['Type'] != 'Category')
            {
                $i = 1;
                $str = $link;
                $links = array_keys($all_urls);
                while (in_array($str, $links))
                {
                    $str .= '-' . $i;
                    $i++;
                }
                $link .= '-' . $i;
            }
        }
        
        if (isset($all_urls_ot['Category'][$categoryID]))
        {
            $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Category'][$categoryID]['ID']]);
        }
        else
        {
            $this->db->insert('Url', [
                'Link' => $link,
                'ObjectID' => $categoryID,
                'Type' => 'Category'
            ]);
        }
        
        // save category langs
        $this->db->delete('CategoryLang', ['CategoryID' => $categoryID]);
        
        $insert = [];
        foreach ($names as $langID => $name)
        {
            $insert[] = [
                'CategoryID' => $categoryID,
                'LangID' => $langID,
                'Name' => $names[$langID],
                'Title' => $titles[$langID],
                'Text' => $texts[$langID],
                'Keywords' => $keywords[$langID],
                'Description' => $descriptions[$langID]
            ];
        }
        $this->db->insert_batch('CategoryLang', $insert);
        
        exit(json_encode([
            'categoryId' => $categoryID,
            'ParentID' => $parentID
        ]));
    }

    public function delete_category()
    {
        $categoryID = (int)$this->input->post('categoryID');
        
        $this->db->delete('Category', [
            'ID' => $categoryID
        ]);
        
        $this->db->delete('CategoryLang', [
            'CategoryID' => $categoryID
        ]);
        
        $this->db->delete('Url', [
            'ObjectID' => $categoryID,
            'Type' => 'Category'
        ], 1);
        
        $this->delete_subcategories($categoryID);
    }
    
    private function delete_subcategories($parentID)
    {
        $subcategories = $this->db->get_where('Category', ['ParentID' => $parentID])->result();
        
        $this->db->delete('Category', [
            'ParentID' => $parentID
        ]);
        
        if (count($subcategories) > 0)
        {
            foreach ($subcategories as $subcat)
            {
                $this->delete_subcategories($subcat->ID);
                $this->db->delete('CategoryLang', [
                    'CategoryID' => $subcat->ID
                ]);
                $this->db->delete('Url', [
                    'ObjectID' => $subcat->ID,
                    'Type' => 'Category'
                ], 1);
            }
        }
    }
    
    public function filters()
    {
        $this->load->helper('form');
        
        if (isset($_GET['delId']))
        {
            $this->db->delete('Filter', ['ID' => (int)$_GET['delId']]);
            $this->db->delete('FilterLang', ['FilterID' => (int)$_GET['delId']]);
            
            $this->session->set_flashdata('success', lang('FilterDeleted'));
            
            redirect('admin/filters');
        }
        
        if (count($_POST) > 0)
        {
            $names = $this->input->post('Name');
            $type = $this->input->post('Type');
            $filterID = (int)$this->input->post('FilterID');
            
            if ($filterID > 0)
            {
                $this->db->update('Filter', ['Type' => $type], ['ID' => $filterID]);
                $this->db->delete('FilterLang', ['FilterID' => $filterID]);
            }
            else
            {
                $this->db->insert('Filter', [
                    'Type' => $type
                ]);
                $filterID = $this->db->insert_id();
            }
            
            $insert = [];
            foreach ($names as $langID => $name)
            {
                $insert[] = [
                    'FilterID' => $filterID,
                    'LangID' => $langID,
                    'Name' => $name
                ];
            }
            $this->db->insert_batch('FilterLang', $insert);
            
            $this->session->set_flashdata('success', lang('FilterSaved'));
            
            redirect('admin/filters');
        }
        
        $this->db->select('*');
        $this->db->from('Filter as f');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID');
        $this->db->order_by('f.ID');
        $res = $this->db->get()->result();
        
        $data['filters'] = [];
        foreach ($res as $row)
        {
            $data['filters'][$row->ID]['Name'][$row->LangID] = $row->Name;
            $data['filters'][$row->ID]['Type'] = $row->Type;
            $data['filters'][$row->ID]['System'] = $row->System;
        }
        
        if (!empty($_GET['id']))
        {
            $id = (int)$_GET['id'];
            $data['filter'] = $data['filters'][$id];
        }
        
        $this->render('admin/filters', $data);
    }
    
    public function products()
    {
        $this->load->helper('form');
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][0] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }
        
        $limit = 10;
        $page = $this->input->get('page');
        $categories = $this->input->get('Category[]');
        $name = $this->input->get('Name');
        $sort = $this->input->get('Sort');
        $is_promo = $this->input->get('IsPromo');
        $sku = $this->input->get('Sku');
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, p.ID as ID, d.ID as DiscountID', false);
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
        $this->db->join('Discount as d', 'd.EntityID = p.ID and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1', 'LEFT');
        
        if (!empty($categories))
        {
            $this->db->where_in('p.CategoryID', $categories);
        }
        
        if (!empty($name))
        {
            $this->db->like('pl.Name', $name);
        }
        
        if (!empty($sort))
        {
            switch ($sort)
            {
                case 'az':
                    $this->db->order_by('pl.Name asc');
                    break;
                case 'za':
                    $this->db->order_by('pl.Name desc');
                    break;
                case 'price_d':
                    $this->db->order_by('ProductPrice desc');
                    break;
                case 'price_a':
                    $this->db->order_by('ProductPrice asc');
                    break;
                default:
                    $this->db->order_by('ID asc');
                    break;
            }
        }
        
        if (!empty($is_promo))
        {
            $this->db->where('d.ID IS NOT NULL', null, false);
        }
        
        if (!empty($sku))
        {
            $this->db->like('Sku', $sku, 'after');
        }
        
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result();
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/products');
        $config['total_rows'] = $this->db->select('FOUND_ROWS() AS `all`', false)->get()->row()->all;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $data['products'] = [];
        foreach ($products as $product)
        {
            $data['products'][$product->ID] = $product;
        }
        
        $this->render('admin/products', $data);
    }
    
    public function update_product_price()
    {
        $pID = $this->input->post('pid');
        $price = $this->input->post('price');
        $price = $price > 0 ? $price : 0;
        
        $this->db->update('Product', ['Price' => $price], ['ID' => $pID], 1);
    }
    
    public function update_product_stock()
    {
        $pID = $this->input->post('pid');
        $stock = (int)$this->input->post('stock');
        $stock = $stock > 0 ? $stock : 0;
        
        $this->db->update('Product', ['Stock' => $stock], ['ID' => $pID], 1);
    }

    public function edit_product()
    {
        $id = (int)$this->input->get('id');
        
        $this->load->helper('form');
        $this->config->load('promotion_types');
        
        if (count($_POST) > 0)
        {
            $category_id = $this->input->post('CategoryID');
            $link = $this->input->post('Link');
            $sku = $this->input->post('Sku');
            $status = $this->input->post('Status');
            $type = $this->input->post('Type');
            $names = $this->input->post('Name');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $price = $this->input->post('Price');
            $stock = $this->input->post('Stock');
            $is_promo = $this->input->post('IsPromo') == 1 ? 1 : 0;
            $discount_price = $this->input->post('DiscountPrice');
            $discount_price_start = $this->input->post('DiscountPriceStart');
            $discount_price_end = $this->input->post('DiscountPriceEnd');
            $primary_image = $this->input->post('PrimaryImage');
            $filter_values = $this->input->post('filter_value');
            
            if (empty($link))
            {
                $link = str_to_url($names[1]);
            }
            
            // save product
            $product = [
                'CategoryID' => $category_id,
                'Sku' => $sku,
                'Stock' => $stock,
                'Price' => $price,
                'DiscountPrice' => $discount_price,
                'DiscountPriceStart' => date('c', strtotime($discount_price_start)),
                'DiscountPriceEnd' => date('c', strtotime($discount_price_end)),
                'IsPromo' => $is_promo,
                'Type' => $type,
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('Product', $product, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Product', $product);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('ProductLang', ['ProductID' => $id]);
            
            $insert = [];
            foreach ($names as $langID => $name)
            {
                $insert[] = [
                    'ProductID' => $id,
                    'LangID' => $langID,
                    'Name' => $names[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('ProductLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Product')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $id;
                }
            }

            if (isset($all_urls_ot['Product'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Product'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'Product'
                ]);
            }
            
            // save filters
            $this->db->delete('ProductFilter', ['ProductID' => $id]);
            
            if (!empty($filter_values))
            {
                $insert = [];
                foreach ($filter_values as $filterID => $filterData)
                {
                    $this->db->insert('ProductFilter', [
                        'ProductID' => $id,
                        'FilterID' => $filterID
                    ]);
                    $productFilterID = $this->db->insert_id();
                    foreach ($this->config->item('languages') as $langID => $lang)
                    {
                        $insert[] = [
                            'ProductFilterID' => $productFilterID,
                            'LangID' => $langID,
                            'Value' => !empty($filter_values[$filterID][$langID]) ? $filter_values[$filterID][$langID] : $filter_values[$filterID][1]
                        ];
                    }
                }
                $this->db->insert_batch('ProductFilterLang', $insert);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('ProductImage', ['IsMain' => 0], ['ProductID' => $id]);
                $this->db->update('ProductImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/products',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/products/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'ProductID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('ProductImage', $insert);
                }
            }
            
            // save discounts
            $this->db->delete('Discount', ['EntityID' => $id, 'EntityType' => 'Product']);
            
            if (!empty($_POST['Discount']))
            {
                $discount_insert = [];
                foreach ($_POST['Discount']['StartDate'] as $key => $val)
                {
                    $discount_insert[] = [
                        'EntityID' => $id,
                        'EntityType' => 'Product',
                        'PromoType' => $_POST['Discount']['PromoType'][$key],
                        'Value' => $_POST['Discount']['Value'][$key],
                        'StartDate' => date('c', strtotime($_POST['Discount']['StartDate'][$key])),
                        'EndDate' => date('c', strtotime($_POST['Discount']['EndDate'][$key])),
                        'PromoStatus' => $_POST['Discount']['PromoStatus'][$key]
                    ];
                }
                $this->db->insert_batch('Discount', $discount_insert);
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('ProductSaved') . '</div>');
                
            $red = site_url('admin/edit_product', ['id' => $id], true);
            redirect($red);
        }
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][''] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }
        
        // get all filters
        $this->db->select('f.ID, fl.Name');
        $this->db->from('Filter as f');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID and fl.LangID = ' . $this->langID);
        $this->db->where_in('f.Type', ['Number', 'String']);
        $this->db->order_by('fl.Name');
        $res = $this->db->get();
        
        $data['all_filters'] = [];
        foreach ($res->result() as $row)
        {
            $data['all_filters'][$row->ID] = $row->Name;
        }
        
        $data['product'] = [];
        $data['images'] = [];
        $data['filters'] = [];
        $data['product_filters'] = [];
        $data['product_filter_values'] = [];
        $data['product_link'] = '';
        
        if ($id > 0)
        {
            // get product
            $this->db->select('*');
            $this->db->from('Product');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $data['product'] = $this->db->get()->row();
            
            // get product langs
            $this->db->select('*');
            $this->db->from('ProductLang');
            $this->db->where('ProductID', $id);
            $res = $this->db->get()->result();
            
            $data['product_langs'] = [];
            foreach ($res as $row)
            {
                $data['product_langs'][$row->LangID] = $row;
            }
            
            // get product url
            $url = $this->db->get_where('Url', ['Type' => 'Product', 'ObjectID' => $id], 1)->row();
            $data['product_link'] = isset($url->Link) ? $url->Link : '';
            
            // get images
            $this->db->select('*');
            $this->db->from('ProductImage');
            $this->db->where('ProductID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get product filters
            $this->db->select('f.ID, pfl.LangID, pfl.Value');
            $this->db->from('ProductFilter as pf');
            $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
            $this->db->join('Filter as f', 'f.ID = pf.FilterID');
            $this->db->join('FilterLang as fl', 'f.ID = fl.FilterID and fl.LangID = ' . $this->langID);
            $this->db->where('pf.ProductID', $id);
            $this->db->order_by('fl.Name');
            $res = $this->db->get();

            foreach ($res->result() as $row)
            {
                $data['product_filters'][$row->ID][$row->LangID] = $row->Value;
            }
            
            // get filter values
            if (count($data['product_filters']) > 0)
            {
                $this->db->select('pfl.Value, pfl.LangID, pf.FilterID');
                $this->db->from('ProductFilter as pf');
                $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
                $res = $this->db->get()->result();
                
                foreach ($res as $row)
                {
                    $data['product_filter_values'][$row->FilterID][$row->LangID][$row->Value] = $row->Value;
                }
            }
            
            // get product discounts
            $data['discounts'] = $this->db->get_where('Discount', ['EntityID' => $id, 'EntityType' => 'Product'])->result();
        }
        
        $this->render('admin/edit_product', $data);
    }
    
    public function product_filter_select()
    {
        $filter_id = $this->input->post('id');
        
        $filter = $this->db->get_where('Filter', ['ID' => $filter_id], 1)->row();
        
        $this->db->select('pfl.LangID, pfl.Value');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
        $this->db->where('pf.FilterID', $filter_id);
        $res = $this->db->get()->result();
        
        $values = [];
        foreach ($res as $row)
        {
            $values[$row->LangID][] = $row->Value;
        }
        
        $str = "";
        foreach ($this->config->item('languages') as $langID => $lang)
        {
            $str .= " <div class=\"col-md-2 filter-value-wrap\">"
                     . "<div class=\"input-group\">
                            <span class=\"input-group-addon\">" . $lang['Slug'] . "</span>
                            <select lang-id=\"$langID\" " . ($filter->Type == 'Number' && $langID != 1 ? 'disabled' : '') . " id=\"filter-select-$filter_id-$langID\" type=\"text\" style=\"width: 100%;\" class=\"form-control filter-value-select\" multiple name=\"filter_value[$filter_id][$langID]\" placeholder=\"Value\">
                    ";
            
            if (isset($values[$langID]))
            {
                foreach ($values[$langID] as $val)
                {
                    $str .= "<option value=\"$val\">$val</option>";
                }
            }
            
            $str .= "
                            </select>
                        </div>"
                    . "</div>";
        }
        
        echo $str;
    }

    public function delete_product_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('ProductImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_news_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('NewsImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_page_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('PageImage', ['ID' => $imageID]);
        }
    }
    
    public function filter_search()
    {
        $name = $this->input->get('q');
        
        exit(json_encode([
            [
                'id' => 20,
                'text' => 20
            ]
        ]));
    }
    
    public function news()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/news');
        $config['total_rows'] = $this->db->count_all_results('News');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('n.ID, nl.Title, nl.Text, n.Date, n.Status, u.Link');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', 'nl.NewsID = n.ID and nl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = n.ID and u.Type = 'News'", 'LEFT');
        $this->db->order_by('n.Date', 'DESC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['news'] = $this->db->get()->result();
        
        $this->render('admin/news', $data);
    }
    
    public function edit_news()
    {
        $id = (int)$this->input->get('id');
        $this->load->model('ContentHistoryModel');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status');
            $titles = $this->input->post('Title');
            $texts = $this->input->post('Text');
            $titles_seo = $this->input->post('TitleSeo');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            $date = $this->input->post('Date');
            $comment = $this->input->post('Comment');
            
            if (empty($link))
            {
                $link = str_to_url($titles[1]);
            }
            
            $this->ContentHistoryModel->saveHistory($id, 'News', $titles, $texts, $titles_seo, $keywords, $descriptions, $link, $status, $comment);
            
            if ($this->input->post('publish') !== null)
            {
                // save product
                $article = [
                    'Date' => date('c', strtotime($date)),
                    'Status' => $status
                ];

                if ($id > 0)
                {
                    $this->db->update('News', $article, ['ID' => $id]);
                }
                else
                {
                    $this->db->insert('News', $article);
                    $id = $this->db->insert_id();
                }

                // save product langs
                $this->db->delete('NewsLang', ['NewsID' => $id]);

                $insert = [];
                foreach ($titles as $langID => $name)
                {
                    $insert[] = [
                        'NewsID' => $id,
                        'LangID' => $langID,
                        'Title' => $titles[$langID],
                        'Text' => $texts[$langID],
                        'TitleSeo' => $titles_seo[$langID],
                        'Keywords' => $keywords[$langID],
                        'Description' => $descriptions[$langID]
                    ];
                }
                $this->db->insert_batch('NewsLang', $insert);

                // save link
                $this->db->select('*');
                $this->db->from('Url');
                $res = $this->db->get()->result_array();

                $all_urls = [];
                $all_urls_ot = [];
                foreach ($res as $row)
                {
                    $all_urls[$row['Link']] = $row;
                    $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
                }

                if (isset($all_urls[$link]))
                {
                    if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'News')
                    {
                        $i = 1;
                        $str = $link;
                        $links = array_keys($all_urls);
                        while (in_array($str, $links))
                        {
                            $str .= '-' . $i;
                            $i++;
                        }
                        $link .= '-' . $i;
                    }
                }

                if (isset($all_urls_ot['News'][$id]))
                {
                    $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['News'][$id]['ID']]);
                }
                else
                {
                    $this->db->insert('Url', [
                        'Link' => $link,
                        'ObjectID' => $id,
                        'Type' => 'News'
                    ]);
                }
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('NewsImage', ['IsMain' => 0], ['NewsID' => $id]);
                $this->db->update('NewsImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/news',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/news/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'NewsID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('NewsImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/news',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'News',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('NewsSaved') . '</div>');
                
            $red = site_url('admin/edit_news', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            if ($this->input->get('history') > 0)
            {
                // get article
                $this->db->select('*');
                $this->db->from('ContentHistory');
                $this->db->where('ID', $this->input->get('history'));
                $this->db->limit(1);
                $data['article'] = $this->db->get()->row();

                // get article langs
                $this->db->select('*');
                $this->db->from('ContentHistoryLang');
                $this->db->where('ContentHistoryID', $this->input->get('history'));
                $res = $this->db->get()->result();

                $data['article_langs'] = [];
                foreach ($res as $row)
                {
                    $data['article_langs'][$row->LangID] = $row;
                }

                // get article url
                $url = $data['article'];
                $data['article_link'] = isset($url->Link) ? $url->Link : '';
            }
            else
            {
                // get article
                $this->db->select('*');
                $this->db->from('News');
                $this->db->where('ID', $id);
                $this->db->where('Status !=', 'Deleted');
                $this->db->limit(1);
                $data['article'] = $this->db->get()->row();

                // get article langs
                $this->db->select('*');
                $this->db->from('NewsLang');
                $this->db->where('NewsID', $id);
                $res = $this->db->get()->result();

                $data['article_langs'] = [];
                foreach ($res as $row)
                {
                    $data['article_langs'][$row->LangID] = $row;
                }

                // get article url
                $url = $this->db->get_where('Url', ['Type' => 'News', 'ObjectID' => $id], 1)->row();
                $data['article_link'] = isset($url->Link) ? $url->Link : '';
            }
            
            // get article images
            $this->db->select('*');
            $this->db->from('NewsImage');
            $this->db->where('NewsID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'News');
            $data['files'] = $this->db->get()->result();
            
            $data['history'] = $this->ContentHistoryModel->getHistoryList($id, 'News', 'admin/edit_news');
        }
        
        $this->render('admin/edit_news', $data);
    }
    
    public function delete_news()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('News', ['ID' => $id]);
            $this->db->delete('NewsLang', ['NewsID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'News']);
            $this->db->delete('NewsImage', ['NewsID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'News'], 1);
        }
        
        redirect(site_url('admin/news'), $page > 0 ? ['page' => $page] : []);
    }
    
    public function pages()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/pages');
        $config['total_rows'] = $this->db->count_all_results('Page');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('p.ID, pl.Title, u.Link, pl.Text, p.Status, p.IsSystem');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'", 'LEFT');
        $this->db->order_by('ID', 'ASC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['pages'] = $this->db->get()->result();
        
        $this->render('admin/pages', $data);
    }
    
    public function edit_page()
    {
        $id = (int)$this->input->get('id');
        $this->load->model('ContentHistoryModel');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status') == 'Disabled' ? 'Disabled' : 'Active';
            $titles = $this->input->post('Title');
            $titles_seo = $this->input->post('TitleSeo');
            $texts = $this->input->post('Text');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            $ACTitle = (array)$this->input->post('ACTitle');
            $ACText = (array)$this->input->post('ACText');
            $comment = $this->input->post('Comment');
            
            if (empty($link))
            {
                $link = $id == 1 ? '/' : str_to_url($titles[1]);
            }
            
            $this->ContentHistoryModel->saveHistory($id, 'Page', $titles, $texts, $titles_seo, $keywords, $descriptions, $link, $status, $comment);
            
            if ($this->input->post('publish') !== null)
            {
                // save page
                $article = [
                    'Status' => $status
                ];

                if ($id > 0)
                {
                    $this->db->update('Page', $article, ['ID' => $id]);
                }
                else
                {
                    $this->db->insert('Page', $article);
                    $id = $this->db->insert_id();
                }

                // save product langs
                $this->db->delete('PageLang', ['PageID' => $id]);

                $insert = [];
                foreach ($titles as $langID => $name)
                {
                    $insert[] = [
                        'PageID' => $id,
                        'LangID' => $langID,
                        'Title' => $titles[$langID],
                        'Text' => $texts[$langID],
                        'TitleSeo' => $titles_seo[$langID],
                        'Keywords' => $keywords[$langID],
                        'Description' => $descriptions[$langID]
                    ];
                }
                $this->db->insert_batch('PageLang', $insert);

                // save link
                $this->db->select('*');
                $this->db->from('Url');
                $res = $this->db->get()->result_array();

                $all_urls = [];
                $all_urls_ot = [];
                foreach ($res as $row)
                {
                    $all_urls[$row['Link']] = $row;
                    $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
                }

                if (isset($all_urls[$link]))
                {
                    if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Page')
                    {
                        $i = 1;
                        $str = $link;
                        $links = array_keys($all_urls);
                        while (in_array($str, $links))
                        {
                            $str .= '-' . $i;
                            $i++;
                        }
                        $link .= '-' . $i;
                    }
                }

                if (isset($all_urls_ot['Page'][$id]))
                {
                    $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Page'][$id]['ID']]);
                }
                else
                {
                    $this->db->insert('Url', [
                        'Link' => $link,
                        'ObjectID' => $id,
                        'Type' => 'Page'
                    ]);
                }
           }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('PageImage', ['IsMain' => 0], ['PageID' => $id]);
                $this->db->update('PageImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/pages',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/pages/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'PageID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('PageImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/pages',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'Page',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            // save additional content
            $this->db->delete('AdditionalContent', ['EntityID' => $id, 'EntityType' => 'Page']);
            
            $acl_insert = [];
            foreach ($ACTitle as $key => $ACTtls)
            {
                $this->db->insert('AdditionalContent', ['EntityID' => $id, 'EntityType' => 'Page']);
                $acID = $this->db->insert_id();
                foreach ($ACTtls as $langID => $ACTtl)
                {
                    $acl_insert[] = [
                        'AdditionalContentID' => $acID,
                        'LangID' => $langID,
                        'Title' => $ACTtl,
                        'Text' => $ACText[$key][$langID]
                    ];
                }
            }
            
            if (count($acl_insert) > 0)
            {
                $this->db->insert_batch('AdditionalContentLang', $acl_insert);
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('PageSaved') . '</div>');
                
            $red = site_url('admin/edit_page', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            if ($this->input->get('history') > 0)
            {
                // get article
                $this->db->select('*');
                $this->db->from('ContentHistory');
                $this->db->where('ID', $this->input->get('history'));
                $this->db->limit(1);
                $data['article'] = $this->db->get()->row();

                // get article langs
                $this->db->select('*');
                $this->db->from('ContentHistoryLang');
                $this->db->where('ContentHistoryID', $this->input->get('history'));
                $res = $this->db->get()->result();

                $data['article_langs'] = [];
                foreach ($res as $row)
                {
                    $data['article_langs'][$row->LangID] = $row;
                }

                // get article url
                $url = $data['article'];
                $data['article_link'] = isset($url->Link) ? $url->Link : '';
            }
            else
            {
                // get article
                $this->db->select('*');
                $this->db->from('Page');
                $this->db->where('ID', $id);
                $this->db->where('Status !=', 'Deleted');
                $this->db->limit(1);
                $data['article'] = $this->db->get()->row();

                // get article langs
                $this->db->select('*');
                $this->db->from('PageLang');
                $this->db->where('PageID', $id);
                $res = $this->db->get()->result();

                $data['article_langs'] = [];
                foreach ($res as $row)
                {
                    $data['article_langs'][$row->LangID] = $row;
                }

                // get article url
                $url = $this->db->get_where('Url', ['Type' => 'Page', 'ObjectID' => $id], 1)->row();
                $data['article_link'] = isset($url->Link) ? $url->Link : '';
            }
            
            // get article images
            $this->db->select('*');
            $this->db->from('PageImage');
            $this->db->where('PageID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'Page');
            $data['files'] = $this->db->get()->result();
            
            // get additional content
            $res = $this->db->select('*')
                ->from('AdditionalContent as ac')
                ->join('AdditionalContentLang as acl', 'acl.AdditionalContentID = ac.ID', 'LEFT')
                ->where("ac.EntityID", $id)
                ->where('ac.EntityType', 'Page')
                ->get()->result();
            
            $data['additional_content'] = [];
            foreach ($res as $row)
            {
                $data['additional_content'][$row->ID][$row->LangID] = $row;
            }
            
            $data['history'] = $this->ContentHistoryModel->getHistoryList($id, 'Page', 'admin/edit_page');
        }
        
        $this->render('admin/edit_page', $data);
    }
    
    public function delete_page()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('Page', ['ID' => $id]);
            $this->db->delete('PageLang', ['PageID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'Page']);
            $this->db->delete('PageImage', ['PageID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'Page'], 1);
        }
        
        redirect(site_url('admin/pages'), $page > 0 ? ['page' => $page] : []);
    }
    
    public function delete_file()
    {
        $fileID = (int)$this->input->post('fileID');
        
        if ($fileID > 0)
        {
            $this->db->delete('File', ['ID' => $fileID]);
        }
    }
    
    public function orders()
    {
        $this->addBreadscrumb('', lang('Orders'));
        
        $this->load->helper('form');
        
        $ClientID = $this->input->get('ClientID');
        $Date = $this->input->get('Date');
        $PaymentType = $this->input->get('PaymentType');
        $Type = $this->input->get('Type');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, o.ID', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->order_by('o.Date', 'DESC');
        if (!empty($ClientID)) $this->db->where('o.UserID', $ClientID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($PaymentType))$this->db->where('o.PaymentType', $PaymentType);
        if (!empty($Type))$this->db->where('o.Type', $Type);
        if (!empty($Status))$this->db->where('o.Status', $Status);
        
        if ($this->_user->Type == 'Manager')
        {
            $this->db->where('od.Region', $this->_user->Region);
        }
        
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['orders'] = $this->db->get()->result();
        
        $total_orders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/orders', [], true);
        $config['total_rows'] = $total_orders;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $clients = $this->db->where_in('Type', ['Angro', 'Retail'])->get('User')->result();
        $data['clients'][''] = 'All';
        foreach ($clients as $client)
        {
            $data['clients'][$client->ID] = $client->Name;
        }
        
        $this->render('admin/orders', $data);
    }

    public function order($order_id = false)
    {
        if (!$order_id) redirect('admin/orders');
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.ID', $order_id);
        $data['order'] = $this->db->get()->row();
        
        if (empty($data['order'])) redirect('admin/orders');
        
        $this->db->update('Order', ['IsNew' => 0], ['ID' => $data['order']->OrderID], 1);
        
        $this->config->load('regions');
        
        $this->addBreadscrumb(site_url('admin/orders'), lang('Orders'));
        $this->addBreadscrumb('', '# ' . $data['order']->OrderID);
        
        $data['client'] = $this->db->get_where('User', ['ID' => $data['order']->UserID], 1)->row();
        
        $this->db->select('*, op.Price');
        $this->db->from('OrderProduct as op');
        $this->db->where('op.OrderID', $data['order']->OrderID);
        $data['order_products'] = $this->db->get()->result();
        
        $this->render('admin/order', $data);
    }
    
    public function users()
    {
        $this->addBreadscrumb('', lang('Users'));
        
        $UserName = $this->input->get('Email');
        $Name = $this->input->get('Name');
        $Country = $this->input->get('Country');
        $City = $this->input->get('City');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        $per_page = 10;
        
        $this->load->helper('form');
        $this->load->model('CountryModel');
        $this->load->model('CityModel');
                
        // get countries
        $countries = [];
        $countries[0] = ""; 
        $countries_array = $this->CountryModel->getList($this->langID);        
        foreach ($countries_array as $country){
            $countries[$country->ID] = $country->Name;
        }
        $data['countries'] = $countries;
        $data['cities'] = [];
            
        // get cities
        $cities = [];
        $cities[0] = "";
        if (!empty($Country)){
            
            $cities_array = $this->CityModel->getListByCountry($Country, $this->langID);
            foreach ($cities_array as $city){
                $cities[$city->ID] = $city->Name;
            }
            $data['cities'] = $cities;
            
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        if (!empty($UserName)) $this->db->like('UserName', $UserName);
        if (!empty($Name)) $this->db->like('Name', $Name);
        if (!empty($Country)) $this->db->where('CountryID', $Country);
        if (!empty($City)) $this->db->where('CityID', $City);
        if (!empty($Status)) $this->db->where('Status', $Status);
                
        if($this->input->get('Type')) $this->db->where('Type', 'Manager');
        else $this->db->where('Type', 'User');
        
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['users'] = $this->db->get('User')->result();
        
        $total_users = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/users', [], true);
        $config['total_rows'] = $total_users;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('admin/users', $data);
    }
    
    public function user($user_id = false)
    {        
        $this->load->model('CountryModel');
        $this->load->model('CityModel');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $data['user'] = $this->db->get_where('User', ['ID' => $user_id], 1)->row();
        
        // get countries
        $countries = [];        
        $countries_array = $this->CountryModel->getList($this->langID);        
        foreach ($countries_array as $country){
            $countries[$country->ID] = $country->Name;
        }
        $data['countries'] = $countries;
        
        // get cities
        $cities = [];
        $cities_array = $this->CityModel->getListByCountry($data['user']->CountryID, $this->langID);
        foreach ($cities_array as $city){
            $cities[$city->ID] = $city->Name;
        }
        $data['cities'] = $cities;        
        
        
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
   
        $password = $this->input->post('Password');
        if (!empty($password))
        {
            $this->form_validation->set_rules('Password', lang('Password'), 'trim|matches[PasswordConfirmation]');
            $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'trim');
        }
        
        $this->form_validation->set_rules('Country', lang('Country'), 'required|is_natural');
        $this->form_validation->set_rules('City', lang('City'), 'required|is_natural');
        $this->form_validation->set_rules('Phone', lang('Phone'), 'trim|required');
        
        
        if ($this->form_validation->run())
        {              
            $userData = [
                //'UserName' => $this->input->post('UserName'),
                'Name' => $this->input->post('Name'),
                'CountryID' => $this->input->post('Country'),
                'CityID' => $this->input->post('City'),                
                'Phone' => $this->input->post('Phone')
            ];
            
            if (!empty($password))
            {
                $userData['Password'] = $password;
            }
            
            $this->db->update('User', $userData, ['ID' => $this->_user->ID], 1);
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect('admin/user/' . $user_id);
        }
        
        $this->render('admin/user', $data);
    }
    
    public function menus()
    {
        if (!($id = $this->input->get('id')))
        {
            $id = 1;
        }
        
        $this->load->helper('form');
        
        if (count($_POST) > 0)
        {
            $this->db->delete('MenuItem', ['MenuID' => $id]);
            
            $mi_insert = array();
            $mil_insert = array();
            $pos = 1;
            foreach($_POST['Entity'] as $Entity => $EntityData)
            {
                $this->db->insert('MenuItem', [
                    'MenuID' => $id,
                    'EntityID' => $EntityData['ID'],
                    'EntityType' => $EntityData['Type'],
                    'Position' => $pos
                ]);
                $miID = $this->db->insert_id();

                foreach ($this->config->item('languages') as $langID => $lang)
                {
                    $name = isset($_POST['Name'][$EntityData['Type']][$EntityData['ID']][$langID]) ? $_POST['Name'][$EntityData['Type']][$EntityData['ID']][$langID] : '';
                    $mil_insert[] = [
                       'MenuItemID' => $miID, 
                       'LangID' => $langID,
                       'Name' => $name,
                    ];
                }
                $pos++;
            }
            
            if (count($mil_insert) > 0)
            {
                $this->db->insert_batch('MenuItemLang', $mil_insert);
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Menu a fost salvata</div>');
            redirect(site_url('admin/menus', ['id' => $id]));
        }
        
        // menu list
        $this->db->select('*');
        $this->db->from('Menu as m');
        $this->db->join('MenuLang as ml', 'ml.MenuID = m.ID AND ml.LangID = ' . $this->langID);
        $this->db->order_by('ID');
        $res = $this->db->get()->result();
        
        $data['all_menus'] = [];
        foreach ($res as $row)
        {
            $data['all_menus'][$row->MenuID] = $row->Name;
        }
        
        // current menu
        $this->db->select('*');
        $this->db->from('Menu as m');
        $this->db->join('MenuLang as ml', 'ml.MenuID = m.ID');
        $this->db->where('m.ID', $id);
        $menu = $this->db->get()->result();
        
        $data['menu_lang'] = [];
        foreach ($menu as $row)
        {
            $data['menu_lang'][$row->LangID] = $row;
        }
        
        // current menu items
        $this->db->select('*');
        $this->db->from('MenuItem as mi');
        $this->db->join('MenuItemLang as mil', 'mil.MenuItemID = mi.ID');
        $this->db->where('mi.MenuID', $id);
        $this->db->order_by('mi.Position');
        $res = $this->db->get()->result();
        
        $data['menu_items'] = [];
        foreach ($res as $row)
        {
            $data['menu_items'][$row->ID]['data'] = $row;
            $data['menu_items'][$row->ID]['langs'][$row->LangID] = $row;
        }
        
        // page list
        $this->db->select('p.ID, pl.Title as Name, pl.LangID');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID');
        $this->db->where('p.Status', 'Active');
        $this->db->order_by('ID', 'ASC');
        $res = $this->db->get()->result();
        
        $data['pages'] = [];
        foreach ($res as $row)
        {
            $row->EntityType = 'Page';
            $data['pages'][$row->ID]['data'] = $row;
            $data['pages'][$row->ID]['langs'][$row->LangID] = $row;
        }
        
        // category list
        $this->db->select('c.ID, cl.Name, cl.LangID');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID');
        $this->db->where('c.Status', 'Active');
        $res = $this->db->get()->result();
        
        $data['categories'] = [];
        foreach ($res as $row)
        {
            $row->EntityType = 'Category';
            $data['categories'][$row->ID]['data'] = $row;
            $data['categories'][$row->ID]['langs'][$row->LangID] = $row;
        }
        
        $this->render('admin/menus', $data);
    }
    
    public function change_order_status()
    {
        $orderID = $this->input->post('orderID');
        $status = $this->input->post('status');
        
        $this->db->update('Order', ['Status' => $status], ['ID' => $orderID], 1);
    }
    
    public function slider()
    {
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('Slider', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Slide a fost eliminat</div>');
            redirect(site_url('admin/slider'));
        }
        
        $data['sliders'] = $this->db->select('*')->from('Slider')->order_by('Position')->get()->result();
        
        if (count($_POST) > 0)
        {
            $config['upload_path'] = 'public/uploads/sliders';
            $config['allowed_types'] = 'jpg|png';
            $config['max_size']	= '10000';
            $config['max_width'] = '4500';
            $config['max_height'] = '2000';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image'))
            {
                $image = $this->upload->data();
                $this->db->insert('Slider', ['Image' => $image['file_name'], 'Position' => 999999]);

                $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
                redirect(site_url('admin/slider'));
            }
            else
            {
                $data['errors'] = $this->upload->display_errors();
            }
        }
        
        $this->render('admin/slider', $data);
    }
    
    public function slider_sort()
    {
        $sids = $this->input->post('sid');
        
        $pos = 1;
        foreach ($sids as $sid)
        {
            $this->db->update('Slider', ['Position' => $pos], ['ID' => $sid], 1);
            $pos++;
        }
    }
    
    public function feedback()
    {
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('Feedback', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Feedback a fost eliminat</div>');
            redirect(site_url('admin/feedback'));
        }
        
        $data['feedbacks'] = $this->db->select('*')->order_by('Date', 'DESC')->get('Feedback')->result();
        
        $this->render('admin/feedback', $data);
    }
    
    public function transporters()
    {
        $transporters = $this->db->get_where('User', ['Type' => 'Transporter'])->result();
        
        $this->render('admin/transporters', [
            'transporters' => $transporters
        ]);
    }
    
    public function edit_transporter($user_id = 0)
    {
        $this->config->load('regions');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        // get countries
        $this->load->model('CountryModel');
        $countries = [];
        $countries_array = $this->CountryModel->getList($this->langID);
        
        foreach ($countries_array as $country){
            $countries[$country->ID] = $country->Name;
        }        
        $data['countries'] = $countries;
        
        $data['user'] = $this->db->get_where('User', ['ID' => $user_id], 1)->row();
        
        $data['cities'] = [];
        if($user_id){
            // get cities
            $this->load->model('CityModel');
            $cities = [];
            $cities_array = $this->CityModel->getListByCountry($data['user']->CountryID, $this->langID);
            foreach ($cities_array as $city){
                $cities[$city->ID] = $city->Name;
            } 
            $data['cities'] = $cities;
        }
        
        if (empty($data['user']->UserName) || $data['user']->UserName != $this->input->post('UserName'))
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
            $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        }
                
        $password = $this->input->post('Password');
        if (!empty($password))
        {
            $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
            $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        }
                
        $this->form_validation->set_rules('CompanyName', lang('CompanyName'), 'trim|required');
        $this->form_validation->set_rules('IDNO', lang('IDNO'), 'trim|required');
        $this->form_validation->set_rules('BankName', lang('BankName'), 'trim|required');
        $this->form_validation->set_rules('IBAN', lang('IBAN'), 'trim|required');
        $this->form_validation->set_rules('Address', lang('Address'), 'trim|required');         
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');        
        $this->form_validation->set_rules('Country', lang('Country'), 'required|is_natural');
        $this->form_validation->set_rules('City', lang('City'), 'required|is_natural');
        $this->form_validation->set_rules('Phone', lang('Phone'), 'trim|required');
        $this->form_validation->set_rules('Status', lang('Status'), 'required'); 
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('ClientType');
            $address = $this->input->post('Address');
            
            if (is_null($address))
            {
                $address = $this->config->item($this->input->post('Region'), 'regions');
            }
            
            $userData = [   
                'CompanyName' => $this->input->post('CompanyName'),
                'IDNO' => $this->input->post('IDNO'),
                'BankName' => $this->input->post('BankName'),
                'IBAN' => $this->input->post('IBAN'),
                'Status' => $this->input->post('Status'),                    
                'Address' => $this->input->post('Address'),
                'UserName' => $this->input->post('UserName'),
                'Name' => $this->input->post('Name'),
                'CountryID' => $this->input->post('Country'),
                'CityID' => $this->input->post('City'),
                'Phone' => $this->input->post('Phone'),
                'Type' => 'Transporter',
                'RegDate' => date('c')
            ];
            
            if (!empty($password))
            {
                $userData['Password'] = $password;
            }
            
            if ($user_id > 0)
            {
                $this->db->update('User', $userData, ['ID' => $user_id], 1);
            }
            else
            {
                $this->db->insert('User', $userData);
                $user_id = $this->db->insert_id();
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect('admin/edit_transporter/' . $user_id);
        }
        
        $this->load->model('RouteModel');
        $data['routes'] = $this->RouteModel->getByUserID($user_id, $this->langID);
        
        $this->render('admin/edit_transporter', $data);
    }
    
    public function seo()
    {
        $this->load->model('RouteSeoModel');
        
        $route_seos = $this->RouteSeoModel->getAll($this->langID);
        
        $this->render('admin/seo', [
            'route_seos' => $route_seos
        ]);
    }
    
    public function edit_seo()
    {
        $id = (int)$this->input->get('id');
        $this->load->model('ContentHistoryModel');
        
        if ($this->input->post())
        {
            $data = [
                'CountryFrom' => (int)$this->input->post('CountryFrom'),
                'CityFrom' => (int)$this->input->post('CityFrom'),
                'CountryTo' => (int)$this->input->post('CountryTo'),
                'CityTo' => (int)$this->input->post('CityTo'),
                'CountryID' => (int)$this->input->post('CountryID'),
            ];
            
            $Title = (array)$this->input->post('Title');
            $TitleSeo = (array)$this->input->post('TitleSeo');
            $Text = (array)$this->input->post('Text');
            $Keywords = (array)$this->input->post('Keywords');
            $Description = (array)$this->input->post('Description');
            $comment = $this->input->post('Comment');
            
            $this->ContentHistoryModel->saveHistory($id, 'Seo', $Title, $Text, $TitleSeo, $Keywords, $Description, '', 'Active', $comment);

            if ($this->input->post('publish') !== null)
            {
                if ($id > 0)
                {
                    $this->db->update('RouteSeo', $data, ['ID' => $id]);
                }
                else
                {
                    $this->db->insert('RouteSeo', $data);
                    $id = $this->db->insert_id();
                }

                $this->db->delete('RouteSeoLang', ['RouteSeoID' => $id]);

                $lang_insert = [];
                foreach ($Title as $langID => $ttl)
                {
                    $lang_insert[] = [
                        'RouteSeoID' => $id,
                        'LangID' => $langID,
                        'Title' => $Title[$langID],
                        'TitleSeo' => $TitleSeo[$langID],
                        'Text' => $Text[$langID],
                        'Keywords' => $Keywords[$langID],
                        'Description' => $Description[$langID]
                    ];
                }

                if (count($lang_insert) > 0)
                {
                    $this->db->insert_batch('RouteSeoLang', $lang_insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . 'Данные сохранены' . '</div>');
            
            redirect(site_url('admin/edit_seo', ['id' => $id]));
        }
        
        $this->load->helper('form');
        $this->load->model('RouteSeoModel');
        $this->load->model('CountryModel');
        $this->load->model('CityModel');
        
        $route_seo = $this->RouteSeoModel->getForEdit($id);
        
        if ($this->input->get('history') > 0)
        {
            // get article
            $this->db->select('*');
            $this->db->from('ContentHistory');
            $this->db->where('ID', $this->input->get('history'));
            $this->db->limit(1);
            $data['article'] = $this->db->get()->row();
            
            $route_seo->data = (object)array_merge((array)$route_seo->data, (array)$data['article']);
            
            // get article langs
            $this->db->select('*');
            $this->db->from('ContentHistoryLang');
            $this->db->where('ContentHistoryID', $this->input->get('history'));
            $res = $this->db->get()->result();

            $data['article_langs'] = [];
            foreach ($res as $row)
            {
                $route_seo->langs[$row->LangID] = $row;
            }
        }
        
        $history = $this->ContentHistoryModel->getHistoryList($id, 'Seo', 'admin/edit_seo');
        
        $this->render('admin/edit_seo', [
            'route_seo' => $route_seo,
            'history' => $history
        ]);
    }
    
    public function delete_seo($id)
    {
        $this->db->delete('RouteSeo', ['ID' => $id], 1);
        $this->db->delete('RouteSeoLang', ['RouteSeoID' => $id]);
        
        redirect(site_url('admin/seo'));
    }

    public function import()
    {
//        $json_en = '{"AF":"Afghanistan","AX":"\u00c5land Islands","AL":"Albania","DZ":"Algeria","AS":"American Samoa","AD":"Andorra","AO":"Angola","AI":"Anguilla","AQ":"Antarctica","AG":"Antigua & Barbuda","AR":"Argentina","AM":"Armenia","AW":"Aruba","AC":"Ascension Island","AU":"Australia","AT":"Austria","AZ":"Azerbaijan","BS":"Bahamas","BH":"Bahrain","BD":"Bangladesh","BB":"Barbados","BY":"Belarus","BE":"Belgium","BZ":"Belize","BJ":"Benin","BM":"Bermuda","BT":"Bhutan","BO":"Bolivia","BA":"Bosnia & Herzegovina","BW":"Botswana","BR":"Brazil","IO":"British Indian Ocean Territory","VG":"British Virgin Islands","BN":"Brunei","BG":"Bulgaria","BF":"Burkina Faso","BI":"Burundi","KH":"Cambodia","CM":"Cameroon","CA":"Canada","IC":"Canary Islands","CV":"Cape Verde","BQ":"Caribbean Netherlands","KY":"Cayman Islands","CF":"Central African Republic","EA":"Ceuta & Melilla","TD":"Chad","CL":"Chile","CN":"China","CX":"Christmas Island","CC":"Cocos (Keeling) Islands","CO":"Colombia","KM":"Comoros","CG":"Congo - Brazzaville","CD":"Congo - Kinshasa","CK":"Cook Islands","CR":"Costa Rica","CI":"C\u00f4te d\u2019Ivoire","HR":"Croatia","CU":"Cuba","CW":"Cura\u00e7ao","CY":"Cyprus","CZ":"Czech Republic","DK":"Denmark","DG":"Diego Garcia","DJ":"Djibouti","DM":"Dominica","DO":"Dominican Republic","EC":"Ecuador","EG":"Egypt","SV":"El Salvador","GQ":"Equatorial Guinea","ER":"Eritrea","EE":"Estonia","ET":"Ethiopia","FK":"Falkland Islands","FO":"Faroe Islands","FJ":"Fiji","FI":"Finland","FR":"France","GF":"French Guiana","PF":"French Polynesia","TF":"French Southern Territories","GA":"Gabon","GM":"Gambia","GE":"Georgia","DE":"Germany","GH":"Ghana","GI":"Gibraltar","GR":"Greece","GL":"Greenland","GD":"Grenada","GP":"Guadeloupe","GU":"Guam","GT":"Guatemala","GG":"Guernsey","GN":"Guinea","GW":"Guinea-Bissau","GY":"Guyana","HT":"Haiti","HN":"Honduras","HK":"Hong Kong SAR China","HU":"Hungary","IS":"Iceland","IN":"India","ID":"Indonesia","IR":"Iran","IQ":"Iraq","IE":"Ireland","IM":"Isle of Man","IL":"Israel","IT":"Italy","JM":"Jamaica","JP":"Japan","JE":"Jersey","JO":"Jordan","KZ":"Kazakhstan","KE":"Kenya","KI":"Kiribati","XK":"Kosovo","KW":"Kuwait","KG":"Kyrgyzstan","LA":"Laos","LV":"Latvia","LB":"Lebanon","LS":"Lesotho","LR":"Liberia","LY":"Libya","LI":"Liechtenstein","LT":"Lithuania","LU":"Luxembourg","MO":"Macau SAR China","MK":"Macedonia","MG":"Madagascar","MW":"Malawi","MY":"Malaysia","MV":"Maldives","ML":"Mali","MT":"Malta","MH":"Marshall Islands","MQ":"Martinique","MR":"Mauritania","MU":"Mauritius","YT":"Mayotte","MX":"Mexico","FM":"Micronesia","MD":"Moldova","MC":"Monaco","MN":"Mongolia","ME":"Montenegro","MS":"Montserrat","MA":"Morocco","MZ":"Mozambique","MM":"Myanmar (Burma)","NA":"Namibia","NR":"Nauru","NP":"Nepal","NL":"Netherlands","NC":"New Caledonia","NZ":"New Zealand","NI":"Nicaragua","NE":"Niger","NG":"Nigeria","NU":"Niue","NF":"Norfolk Island","KP":"North Korea","MP":"Northern Mariana Islands","NO":"Norway","OM":"Oman","PK":"Pakistan","PW":"Palau","PS":"Palestinian Territories","PA":"Panama","PG":"Papua New Guinea","PY":"Paraguay","PE":"Peru","PH":"Philippines","PN":"Pitcairn Islands","PL":"Poland","PT":"Portugal","PR":"Puerto Rico","QA":"Qatar","RE":"R\u00e9union","RO":"Romania","RU":"Russia","RW":"Rwanda","WS":"Samoa","SM":"San Marino","ST":"S\u00e3o Tom\u00e9 & Pr\u00edncipe","SA":"Saudi Arabia","SN":"Senegal","RS":"Serbia","SC":"Seychelles","SL":"Sierra Leone","SG":"Singapore","SX":"Sint Maarten","SK":"Slovakia","SI":"Slovenia","SB":"Solomon Islands","SO":"Somalia","ZA":"South Africa","GS":"South Georgia & South Sandwich Islands","KR":"South Korea","SS":"South Sudan","ES":"Spain","LK":"Sri Lanka","BL":"St. Barth\u00e9lemy","SH":"St. Helena","KN":"St. Kitts & Nevis","LC":"St. Lucia","MF":"St. Martin","PM":"St. Pierre & Miquelon","VC":"St. Vincent & Grenadines","SD":"Sudan","SR":"Suriname","SJ":"Svalbard & Jan Mayen","SZ":"Swaziland","SE":"Sweden","CH":"Switzerland","SY":"Syria","TW":"Taiwan","TJ":"Tajikistan","TZ":"Tanzania","TH":"Thailand","TL":"Timor-Leste","TG":"Togo","TK":"Tokelau","TO":"Tonga","TT":"Trinidad & Tobago","TA":"Tristan da Cunha","TN":"Tunisia","TR":"Turkey","TM":"Turkmenistan","TC":"Turks & Caicos Islands","TV":"Tuvalu","UM":"U.S. Outlying Islands","VI":"U.S. Virgin Islands","UG":"Uganda","UA":"Ukraine","AE":"United Arab Emirates","GB":"United Kingdom","US":"United States","UY":"Uruguay","UZ":"Uzbekistan","VU":"Vanuatu","VA":"Vatican City","VE":"Venezuela","VN":"Vietnam","WF":"Wallis & Futuna","EH":"Western Sahara","YE":"Yemen","ZM":"Zambia","ZW":"Zimbabwe"}';
//        $json_ro = '{"AF":"Afganistan","ZA":"Africa de Sud","AL":"Albania","DZ":"Algeria","AD":"Andorra","AO":"Angola","AI":"Anguilla","AQ":"Antarctica","AG":"Antigua \u0219i Barbuda","SA":"Arabia Saudit\u0103","AR":"Argentina","AM":"Armenia","AW":"Aruba","AU":"Australia","AT":"Austria","AZ":"Azerbaidjan","BS":"Bahamas","BH":"Bahrain","BD":"Bangladesh","BB":"Barbados","BY":"Belarus","BE":"Belgia","BZ":"Belize","BJ":"Benin","BM":"Bermuda","BT":"Bhutan","BO":"Bolivia","BA":"Bosnia \u0219i Her\u021begovina","BW":"Botswana","BR":"Brazilia","BN":"Brunei","BG":"Bulgaria","BF":"Burkina Faso","BI":"Burundi","KH":"Cambodgia","CM":"Camerun","CA":"Canada","CV":"Capul Verde","EA":"Ceuta \u0219i Melilla","CL":"Chile","CN":"China","TD":"Ciad","CY":"Cipru","CO":"Columbia","KM":"Comore","CG":"Congo - Brazzaville","CD":"Congo - Kinshasa","KP":"Coreea de Nord","KR":"Coreea de Sud","CR":"Costa Rica","CI":"C\u00f4te d\u2019Ivoire","HR":"Croa\u021bia","CU":"Cuba","CW":"Cura\u00e7ao","DK":"Danemarca","DG":"Diego Garcia","DJ":"Djibouti","DM":"Dominica","EC":"Ecuador","EG":"Egipt","SV":"El Salvador","CH":"Elve\u021bia","AE":"Emiratele Arabe Unite","ER":"Eritreea","EE":"Estonia","ET":"Etiopia","FJ":"Fiji","PH":"Filipine","FI":"Finlanda","FR":"Fran\u021ba","GA":"Gabon","GM":"Gambia","GE":"Georgia","GS":"Georgia de Sud \u0219i Insulele Sandwich de Sud","DE":"Germania","GH":"Ghana","GI":"Gibraltar","GR":"Grecia","GD":"Grenada","GL":"Groenlanda","GP":"Guadelupa","GU":"Guam","GT":"Guatemala","GG":"Guernsey","GN":"Guineea","GQ":"Guineea Ecuatorial\u0103","GW":"Guineea-Bissau","GY":"Guyana","GF":"Guyana Francez\u0103","HT":"Haiti","HN":"Honduras","IN":"India","ID":"Indonezia","AC":"Insula Ascension","CX":"Insula Christmas","IM":"Insula Man","AX":"Insulele \u00c5land","IC":"Insulele Canare","BQ":"Insulele Caraibe Olandeze","KY":"Insulele Cayman","CC":"Insulele Cocos (Keeling)","CK":"Insulele Cook","FK":"Insulele Falkland","FO":"Insulele Feroe","UM":"Insulele \u00cendep\u0103rtate ale S.U.A.","MP":"Insulele Mariane de Nord","MH":"Insulele Marshall","NF":"Insulele Norfolk","PN":"Insulele Pitcairn","SB":"Insulele Solomon","TC":"Insulele Turks \u0219i Caicos","VG":"Insulele Virgine Britanice","VI":"Insulele Virgine S.U.A.","JO":"Iordania","IQ":"Irak","IR":"Iran","IE":"Irlanda","IS":"Islanda","IL":"Israel","IT":"Italia","JM":"Jamaica","JP":"Japonia","JE":"Jersey","KZ":"Kazahstan","KG":"K\u00e2rg\u00e2zstan","KE":"Kenya","KI":"Kiribati","XK":"Kosovo","KW":"Kuweit","LA":"Laos","LS":"Lesotho","LV":"Letonia","LB":"Liban","LR":"Liberia","LY":"Libia","LI":"Liechtenstein","LT":"Lituania","LU":"Luxemburg","MK":"Macedonia","MG":"Madagascar","MY":"Malaezia","MW":"Malawi","MV":"Maldive","ML":"Mali","MT":"Malta","MA":"Maroc","MQ":"Martinica","MR":"Mauritania","MU":"Mauritius","YT":"Mayotte","MX":"Mexic","FM":"Micronezia","MC":"Monaco","MN":"Mongolia","MS":"Montserrat","MZ":"Mozambic","ME":"Muntenegru","MM":"Myanmar (Birmania)","NA":"Namibia","NR":"Nauru","NP":"Nepal","NI":"Nicaragua","NE":"Niger","NG":"Nigeria","NU":"Niue","NO":"Norvegia","NC":"Noua Caledonie","NZ":"Noua Zeeland\u0103","OM":"Oman","PK":"Pakistan","PW":"Palau","PA":"Panama","PG":"Papua-Noua Guinee","PY":"Paraguay","PE":"Peru","PF":"Polinezia Francez\u0103","PL":"Polonia","PT":"Portugalia","PR":"Puerto Rico","QA":"Qatar","HK":"R.A.S. Hong Kong a Chinei","MO":"R.A.S. Macao a Chinei","GB":"Regatul Unit","CZ":"Republica Ceh\u0103","CF":"Republica Centrafrican\u0103","DO":"Republica Dominican\u0103","MD":"Republica Moldova","RE":"R\u00e9union","RO":"Rom\u00e2nia","RU":"Rusia","RW":"Rwanda","EH":"Sahara Occidental\u0103","KN":"Saint Kitts \u0219i Nevis","VC":"Saint Vincent \u0219i Grenadine","PM":"Saint-Pierre \u0219i Miquelon","WS":"Samoa","AS":"Samoa American\u0103","SM":"San Marino","ST":"Sao Tome \u0219i Principe","SN":"Senegal","RS":"Serbia","SC":"Seychelles","SH":"Sf\u00e2nta Elena","LC":"Sf\u00e2nta Lucia","BL":"Sf\u00e2ntul Bartolomeu","MF":"Sf\u00e2ntul Martin","SL":"Sierra Leone","SG":"Singapore","SX":"Sint-Maarten","SY":"Siria","SK":"Slovacia","SI":"Slovenia","SO":"Somalia","ES":"Spania","LK":"Sri Lanka","US":"Statele Unite ale Americii","VA":"Statul Cet\u0103\u021bii Vaticanului","SD":"Sudan","SS":"Sudanul de Sud","SE":"Suedia","SR":"Surinam","SJ":"Svalbard \u0219i Jan Mayen","SZ":"Swaziland","TJ":"Tadjikistan","TW":"Taiwan","TZ":"Tanzania","TF":"Teritoriile Australe \u0219i Antarctice Franceze","PS":"Teritoriile Palestiniene","IO":"Teritoriul Britanic din Oceanul Indian","TH":"Thailanda","TL":"Timorul de Est","TG":"Togo","TK":"Tokelau","TO":"Tonga","TT":"Trinidad \u0219i Tobago","TA":"Tristan da Cunha","TN":"Tunisia","TR":"Turcia","TM":"Turkmenistan","TV":"Tuvalu","NL":"\u021a\u0103rile de Jos","UA":"Ucraina","UG":"Uganda","HU":"Ungaria","UY":"Uruguay","UZ":"Uzbekistan","VU":"Vanuatu","VE":"Venezuela","VN":"Vietnam","WF":"Wallis \u0219i Futuna","YE":"Yemen","ZM":"Zambia","ZW":"Zimbabwe"}';
//        $json_ru = '{"AU":"\u0410\u0432\u0441\u0442\u0440\u0430\u043b\u0438\u044f","AT":"\u0410\u0432\u0441\u0442\u0440\u0438\u044f","AZ":"\u0410\u0437\u0435\u0440\u0431\u0430\u0439\u0434\u0436\u0430\u043d","AX":"\u0410\u043b\u0430\u043d\u0434\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","AL":"\u0410\u043b\u0431\u0430\u043d\u0438\u044f","DZ":"\u0410\u043b\u0436\u0438\u0440","AS":"\u0410\u043c\u0435\u0440\u0438\u043a\u0430\u043d\u0441\u043a\u043e\u0435 \u0421\u0430\u043c\u043e\u0430","AI":"\u0410\u043d\u0433\u0438\u043b\u044c\u044f","AO":"\u0410\u043d\u0433\u043e\u043b\u0430","AD":"\u0410\u043d\u0434\u043e\u0440\u0440\u0430","AQ":"\u0410\u043d\u0442\u0430\u0440\u043a\u0442\u0438\u0434\u0430","AG":"\u0410\u043d\u0442\u0438\u0433\u0443\u0430 \u0438 \u0411\u0430\u0440\u0431\u0443\u0434\u0430","AR":"\u0410\u0440\u0433\u0435\u043d\u0442\u0438\u043d\u0430","AM":"\u0410\u0440\u043c\u0435\u043d\u0438\u044f","AW":"\u0410\u0440\u0443\u0431\u0430","AF":"\u0410\u0444\u0433\u0430\u043d\u0438\u0441\u0442\u0430\u043d","BS":"\u0411\u0430\u0433\u0430\u043c\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","BD":"\u0411\u0430\u043d\u0433\u043b\u0430\u0434\u0435\u0448","BB":"\u0411\u0430\u0440\u0431\u0430\u0434\u043e\u0441","BH":"\u0411\u0430\u0445\u0440\u0435\u0439\u043d","BY":"\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u044c","BZ":"\u0411\u0435\u043b\u0438\u0437","BE":"\u0411\u0435\u043b\u044c\u0433\u0438\u044f","BJ":"\u0411\u0435\u043d\u0438\u043d","BM":"\u0411\u0435\u0440\u043c\u0443\u0434\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","BG":"\u0411\u043e\u043b\u0433\u0430\u0440\u0438\u044f","BO":"\u0411\u043e\u043b\u0438\u0432\u0438\u044f","BQ":"\u0411\u043e\u043d\u044d\u0439\u0440, \u0421\u0438\u043d\u0442-\u042d\u0441\u0442\u0430\u0442\u0438\u0443\u0441 \u0438 \u0421\u0430\u0431\u0430","BA":"\u0411\u043e\u0441\u043d\u0438\u044f \u0438 \u0413\u0435\u0440\u0446\u0435\u0433\u043e\u0432\u0438\u043d\u0430","BW":"\u0411\u043e\u0442\u0441\u0432\u0430\u043d\u0430","BR":"\u0411\u0440\u0430\u0437\u0438\u043b\u0438\u044f","IO":"\u0411\u0440\u0438\u0442\u0430\u043d\u0441\u043a\u0430\u044f \u0442\u0435\u0440\u0440\u0438\u0442\u043e\u0440\u0438\u044f \u0432 \u0418\u043d\u0434\u0438\u0439\u0441\u043a\u043e\u043c \u043e\u043a\u0435\u0430\u043d\u0435","BN":"\u0411\u0440\u0443\u043d\u0435\u0439-\u0414\u0430\u0440\u0443\u0441\u0441\u0430\u043b\u0430\u043c","BF":"\u0411\u0443\u0440\u043a\u0438\u043d\u0430-\u0424\u0430\u0441\u043e","BI":"\u0411\u0443\u0440\u0443\u043d\u0434\u0438","BT":"\u0411\u0443\u0442\u0430\u043d","VU":"\u0412\u0430\u043d\u0443\u0430\u0442\u0443","VA":"\u0412\u0430\u0442\u0438\u043a\u0430\u043d","GB":"\u0412\u0435\u043b\u0438\u043a\u043e\u0431\u0440\u0438\u0442\u0430\u043d\u0438\u044f","HU":"\u0412\u0435\u043d\u0433\u0440\u0438\u044f","VE":"\u0412\u0435\u043d\u0435\u0441\u0443\u044d\u043b\u0430","VG":"\u0412\u0438\u0440\u0433\u0438\u043d\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430 (\u0411\u0440\u0438\u0442\u0430\u043d\u0441\u043a\u0438\u0435)","VI":"\u0412\u0438\u0440\u0433\u0438\u043d\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430 (\u0421\u0428\u0410)","UM":"\u0412\u043d\u0435\u0448\u043d\u0438\u0435 \u043c\u0430\u043b\u044b\u0435 \u043e-\u0432\u0430 (\u0421\u0428\u0410)","TL":"\u0412\u043e\u0441\u0442\u043e\u0447\u043d\u044b\u0439 \u0422\u0438\u043c\u043e\u0440","VN":"\u0412\u044c\u0435\u0442\u043d\u0430\u043c","GA":"\u0413\u0430\u0431\u043e\u043d","HT":"\u0413\u0430\u0438\u0442\u0438","GY":"\u0413\u0430\u0439\u0430\u043d\u0430","GM":"\u0413\u0430\u043c\u0431\u0438\u044f","GH":"\u0413\u0430\u043d\u0430","GP":"\u0413\u0432\u0430\u0434\u0435\u043b\u0443\u043f\u0430","GT":"\u0413\u0432\u0430\u0442\u0435\u043c\u0430\u043b\u0430","GN":"\u0413\u0432\u0438\u043d\u0435\u044f","GW":"\u0413\u0432\u0438\u043d\u0435\u044f-\u0411\u0438\u0441\u0430\u0443","DE":"\u0413\u0435\u0440\u043c\u0430\u043d\u0438\u044f","GG":"\u0413\u0435\u0440\u043d\u0441\u0438","GI":"\u0413\u0438\u0431\u0440\u0430\u043b\u0442\u0430\u0440","HN":"\u0413\u043e\u043d\u0434\u0443\u0440\u0430\u0441","HK":"\u0413\u043e\u043d\u043a\u043e\u043d\u0433 (\u043e\u0441\u043e\u0431\u044b\u0439 \u0440\u0430\u0439\u043e\u043d)","GD":"\u0413\u0440\u0435\u043d\u0430\u0434\u0430","GL":"\u0413\u0440\u0435\u043d\u043b\u0430\u043d\u0434\u0438\u044f","GR":"\u0413\u0440\u0435\u0446\u0438\u044f","GE":"\u0413\u0440\u0443\u0437\u0438\u044f","GU":"\u0413\u0443\u0430\u043c","DK":"\u0414\u0430\u043d\u0438\u044f","JE":"\u0414\u0436\u0435\u0440\u0441\u0438","DJ":"\u0414\u0436\u0438\u0431\u0443\u0442\u0438","DG":"\u0414\u0438\u0435\u0433\u043e-\u0413\u0430\u0440\u0441\u0438\u044f","DM":"\u0414\u043e\u043c\u0438\u043d\u0438\u043a\u0430","DO":"\u0414\u043e\u043c\u0438\u043d\u0438\u043a\u0430\u043d\u0441\u043a\u0430\u044f \u0420\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430","EG":"\u0415\u0433\u0438\u043f\u0435\u0442","ZM":"\u0417\u0430\u043c\u0431\u0438\u044f","EH":"\u0417\u0430\u043f\u0430\u0434\u043d\u0430\u044f \u0421\u0430\u0445\u0430\u0440\u0430","ZW":"\u0417\u0438\u043c\u0431\u0430\u0431\u0432\u0435","IL":"\u0418\u0437\u0440\u0430\u0438\u043b\u044c","IN":"\u0418\u043d\u0434\u0438\u044f","ID":"\u0418\u043d\u0434\u043e\u043d\u0435\u0437\u0438\u044f","JO":"\u0418\u043e\u0440\u0434\u0430\u043d\u0438\u044f","IQ":"\u0418\u0440\u0430\u043a","IR":"\u0418\u0440\u0430\u043d","IE":"\u0418\u0440\u043b\u0430\u043d\u0434\u0438\u044f","IS":"\u0418\u0441\u043b\u0430\u043d\u0434\u0438\u044f","ES":"\u0418\u0441\u043f\u0430\u043d\u0438\u044f","IT":"\u0418\u0442\u0430\u043b\u0438\u044f","YE":"\u0419\u0435\u043c\u0435\u043d","CV":"\u041a\u0430\u0431\u043e-\u0412\u0435\u0440\u0434\u0435","KZ":"\u041a\u0430\u0437\u0430\u0445\u0441\u0442\u0430\u043d","KY":"\u041a\u0430\u0439\u043c\u0430\u043d\u043e\u0432\u044b \u043e-\u0432\u0430","KH":"\u041a\u0430\u043c\u0431\u043e\u0434\u0436\u0430","CM":"\u041a\u0430\u043c\u0435\u0440\u0443\u043d","CA":"\u041a\u0430\u043d\u0430\u0434\u0430","IC":"\u041a\u0430\u043d\u0430\u0440\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","QA":"\u041a\u0430\u0442\u0430\u0440","KE":"\u041a\u0435\u043d\u0438\u044f","CY":"\u041a\u0438\u043f\u0440","KG":"\u041a\u0438\u0440\u0433\u0438\u0437\u0438\u044f","KI":"\u041a\u0438\u0440\u0438\u0431\u0430\u0442\u0438","CN":"\u041a\u0438\u0442\u0430\u0439","KP":"\u041a\u041d\u0414\u0420","CC":"\u041a\u043e\u043a\u043e\u0441\u043e\u0432\u044b\u0435 \u043e-\u0432\u0430","CO":"\u041a\u043e\u043b\u0443\u043c\u0431\u0438\u044f","KM":"\u041a\u043e\u043c\u043e\u0440\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","CG":"\u041a\u043e\u043d\u0433\u043e - \u0411\u0440\u0430\u0437\u0437\u0430\u0432\u0438\u043b\u044c","CD":"\u041a\u043e\u043d\u0433\u043e - \u041a\u0438\u043d\u0448\u0430\u0441\u0430","XK":"\u041a\u043e\u0441\u043e\u0432\u043e","CR":"\u041a\u043e\u0441\u0442\u0430-\u0420\u0438\u043a\u0430","CI":"\u041a\u043e\u0442-\u0434\u2019\u0418\u0432\u0443\u0430\u0440","CU":"\u041a\u0443\u0431\u0430","KW":"\u041a\u0443\u0432\u0435\u0439\u0442","CW":"\u041a\u044e\u0440\u0430\u0441\u0430\u043e","LA":"\u041b\u0430\u043e\u0441","LV":"\u041b\u0430\u0442\u0432\u0438\u044f","LS":"\u041b\u0435\u0441\u043e\u0442\u043e","LR":"\u041b\u0438\u0431\u0435\u0440\u0438\u044f","LB":"\u041b\u0438\u0432\u0430\u043d","LY":"\u041b\u0438\u0432\u0438\u044f","LT":"\u041b\u0438\u0442\u0432\u0430","LI":"\u041b\u0438\u0445\u0442\u0435\u043d\u0448\u0442\u0435\u0439\u043d","LU":"\u041b\u044e\u043a\u0441\u0435\u043c\u0431\u0443\u0440\u0433","MU":"\u041c\u0430\u0432\u0440\u0438\u043a\u0438\u0439","MR":"\u041c\u0430\u0432\u0440\u0438\u0442\u0430\u043d\u0438\u044f","MG":"\u041c\u0430\u0434\u0430\u0433\u0430\u0441\u043a\u0430\u0440","YT":"\u041c\u0430\u0439\u043e\u0442\u0442\u0430","MO":"\u041c\u0430\u043a\u0430\u043e (\u043e\u0441\u043e\u0431\u044b\u0439 \u0440\u0430\u0439\u043e\u043d)","MK":"\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0438\u044f","MW":"\u041c\u0430\u043b\u0430\u0432\u0438","MY":"\u041c\u0430\u043b\u0430\u0439\u0437\u0438\u044f","ML":"\u041c\u0430\u043b\u0438","MV":"\u041c\u0430\u043b\u044c\u0434\u0438\u0432\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","MT":"\u041c\u0430\u043b\u044c\u0442\u0430","MA":"\u041c\u0430\u0440\u043e\u043a\u043a\u043e","MQ":"\u041c\u0430\u0440\u0442\u0438\u043d\u0438\u043a\u0430","MH":"\u041c\u0430\u0440\u0448\u0430\u043b\u043b\u043e\u0432\u044b \u043e-\u0432\u0430","MX":"\u041c\u0435\u043a\u0441\u0438\u043a\u0430","MZ":"\u041c\u043e\u0437\u0430\u043c\u0431\u0438\u043a","MD":"\u041c\u043e\u043b\u0434\u043e\u0432\u0430","MC":"\u041c\u043e\u043d\u0430\u043a\u043e","MN":"\u041c\u043e\u043d\u0433\u043e\u043b\u0438\u044f","MS":"\u041c\u043e\u043d\u0442\u0441\u0435\u0440\u0440\u0430\u0442","MM":"\u041c\u044c\u044f\u043d\u043c\u0430 (\u0411\u0438\u0440\u043c\u0430)","NA":"\u041d\u0430\u043c\u0438\u0431\u0438\u044f","NR":"\u041d\u0430\u0443\u0440\u0443","NP":"\u041d\u0435\u043f\u0430\u043b","NE":"\u041d\u0438\u0433\u0435\u0440","NG":"\u041d\u0438\u0433\u0435\u0440\u0438\u044f","NL":"\u041d\u0438\u0434\u0435\u0440\u043b\u0430\u043d\u0434\u044b","NI":"\u041d\u0438\u043a\u0430\u0440\u0430\u0433\u0443\u0430","NU":"\u041d\u0438\u0443\u044d","NZ":"\u041d\u043e\u0432\u0430\u044f \u0417\u0435\u043b\u0430\u043d\u0434\u0438\u044f","NC":"\u041d\u043e\u0432\u0430\u044f \u041a\u0430\u043b\u0435\u0434\u043e\u043d\u0438\u044f","NO":"\u041d\u043e\u0440\u0432\u0435\u0433\u0438\u044f","AC":"\u043e-\u0432 \u0412\u043e\u0437\u043d\u0435\u0441\u0435\u043d\u0438\u044f","IM":"\u041e-\u0432 \u041c\u044d\u043d","NF":"\u043e-\u0432 \u041d\u043e\u0440\u0444\u043e\u043b\u043a","CX":"\u043e-\u0432 \u0420\u043e\u0436\u0434\u0435\u0441\u0442\u0432\u0430","SH":"\u041e-\u0432 \u0421\u0432. \u0415\u043b\u0435\u043d\u044b","CK":"\u043e-\u0432\u0430 \u041a\u0443\u043a\u0430","TC":"\u041e-\u0432\u0430 \u0422\u0451\u0440\u043a\u0441 \u0438 \u041a\u0430\u0439\u043a\u043e\u0441","AE":"\u041e\u0410\u042d","OM":"\u041e\u043c\u0430\u043d","PK":"\u041f\u0430\u043a\u0438\u0441\u0442\u0430\u043d","PW":"\u041f\u0430\u043b\u0430\u0443","PS":"\u041f\u0430\u043b\u0435\u0441\u0442\u0438\u043d\u0441\u043a\u0438\u0435 \u0442\u0435\u0440\u0440\u0438\u0442\u043e\u0440\u0438\u0438","PA":"\u041f\u0430\u043d\u0430\u043c\u0430","PG":"\u041f\u0430\u043f\u0443\u0430 \u2013 \u041d\u043e\u0432\u0430\u044f \u0413\u0432\u0438\u043d\u0435\u044f","PY":"\u041f\u0430\u0440\u0430\u0433\u0432\u0430\u0439","PE":"\u041f\u0435\u0440\u0443","PN":"\u041f\u0438\u0442\u043a\u044d\u0440\u043d","PL":"\u041f\u043e\u043b\u044c\u0448\u0430","PT":"\u041f\u043e\u0440\u0442\u0443\u0433\u0430\u043b\u0438\u044f","PR":"\u041f\u0443\u044d\u0440\u0442\u043e-\u0420\u0438\u043a\u043e","KR":"\u0420\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430 \u041a\u043e\u0440\u0435\u044f","RE":"\u0420\u0435\u044e\u043d\u044c\u043e\u043d","RU":"\u0420\u043e\u0441\u0441\u0438\u044f","RW":"\u0420\u0443\u0430\u043d\u0434\u0430","RO":"\u0420\u0443\u043c\u044b\u043d\u0438\u044f","SV":"\u0421\u0430\u043b\u044c\u0432\u0430\u0434\u043e\u0440","WS":"\u0421\u0430\u043c\u043e\u0430","SM":"\u0421\u0430\u043d-\u041c\u0430\u0440\u0438\u043d\u043e","ST":"\u0421\u0430\u043d-\u0422\u043e\u043c\u0435 \u0438 \u041f\u0440\u0438\u043d\u0441\u0438\u043f\u0438","SA":"\u0421\u0430\u0443\u0434\u043e\u0432\u0441\u043a\u0430\u044f \u0410\u0440\u0430\u0432\u0438\u044f","SZ":"\u0421\u0432\u0430\u0437\u0438\u043b\u0435\u043d\u0434","MP":"\u0421\u0435\u0432\u0435\u0440\u043d\u044b\u0435 \u041c\u0430\u0440\u0438\u0430\u043d\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","SC":"\u0421\u0435\u0439\u0448\u0435\u043b\u044c\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","BL":"\u0421\u0435\u043d-\u0411\u0430\u0440\u0442\u0435\u043b\u044c\u043c\u0438","MF":"\u0421\u0435\u043d-\u041c\u0430\u0440\u0442\u0435\u043d","PM":"\u0421\u0435\u043d-\u041f\u044c\u0435\u0440 \u0438 \u041c\u0438\u043a\u0435\u043b\u043e\u043d","SN":"\u0421\u0435\u043d\u0435\u0433\u0430\u043b","VC":"\u0421\u0435\u043d\u0442-\u0412\u0438\u043d\u0441\u0435\u043d\u0442 \u0438 \u0413\u0440\u0435\u043d\u0430\u0434\u0438\u043d\u044b","KN":"\u0421\u0435\u043d\u0442-\u041a\u0438\u0442\u0441 \u0438 \u041d\u0435\u0432\u0438\u0441","LC":"\u0421\u0435\u043d\u0442-\u041b\u044e\u0441\u0438\u044f","RS":"\u0421\u0435\u0440\u0431\u0438\u044f","EA":"\u0421\u0435\u0443\u0442\u0430 \u0438 \u041c\u0435\u043b\u0438\u043b\u044c\u044f","SG":"\u0421\u0438\u043d\u0433\u0430\u043f\u0443\u0440","SX":"\u0421\u0438\u043d\u0442-\u041c\u0430\u0440\u0442\u0435\u043d","SY":"\u0421\u0438\u0440\u0438\u044f","SK":"\u0421\u043b\u043e\u0432\u0430\u043a\u0438\u044f","SI":"\u0421\u043b\u043e\u0432\u0435\u043d\u0438\u044f","US":"\u0421\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u043d\u044b\u0435 \u0428\u0442\u0430\u0442\u044b","SB":"\u0421\u043e\u043b\u043e\u043c\u043e\u043d\u043e\u0432\u044b \u043e-\u0432\u0430","SO":"\u0421\u043e\u043c\u0430\u043b\u0438","SD":"\u0421\u0443\u0434\u0430\u043d","SR":"\u0421\u0443\u0440\u0438\u043d\u0430\u043c","SL":"\u0421\u044c\u0435\u0440\u0440\u0430-\u041b\u0435\u043e\u043d\u0435","TJ":"\u0422\u0430\u0434\u0436\u0438\u043a\u0438\u0441\u0442\u0430\u043d","TH":"\u0422\u0430\u0438\u043b\u0430\u043d\u0434","TW":"\u0422\u0430\u0439\u0432\u0430\u043d\u044c","TZ":"\u0422\u0430\u043d\u0437\u0430\u043d\u0438\u044f","TG":"\u0422\u043e\u0433\u043e","TK":"\u0422\u043e\u043a\u0435\u043b\u0430\u0443","TO":"\u0422\u043e\u043d\u0433\u0430","TT":"\u0422\u0440\u0438\u043d\u0438\u0434\u0430\u0434 \u0438 \u0422\u043e\u0431\u0430\u0433\u043e","TA":"\u0422\u0440\u0438\u0441\u0442\u0430\u043d-\u0434\u0430-\u041a\u0443\u043d\u044c\u044f","TV":"\u0422\u0443\u0432\u0430\u043b\u0443","TN":"\u0422\u0443\u043d\u0438\u0441","TM":"\u0422\u0443\u0440\u043a\u043c\u0435\u043d\u0438\u0441\u0442\u0430\u043d","TR":"\u0422\u0443\u0440\u0446\u0438\u044f","UG":"\u0423\u0433\u0430\u043d\u0434\u0430","UZ":"\u0423\u0437\u0431\u0435\u043a\u0438\u0441\u0442\u0430\u043d","UA":"\u0423\u043a\u0440\u0430\u0438\u043d\u0430","WF":"\u0423\u043e\u043b\u043b\u0438\u0441 \u0438 \u0424\u0443\u0442\u0443\u043d\u0430","UY":"\u0423\u0440\u0443\u0433\u0432\u0430\u0439","FO":"\u0424\u0430\u0440\u0435\u0440\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","FM":"\u0424\u0435\u0434\u0435\u0440\u0430\u0442\u0438\u0432\u043d\u044b\u0435 \u0428\u0442\u0430\u0442\u044b \u041c\u0438\u043a\u0440\u043e\u043d\u0435\u0437\u0438\u0438","FJ":"\u0424\u0438\u0434\u0436\u0438","PH":"\u0424\u0438\u043b\u0438\u043f\u043f\u0438\u043d\u044b","FI":"\u0424\u0438\u043d\u043b\u044f\u043d\u0434\u0438\u044f","FK":"\u0424\u043e\u043b\u043a\u043b\u0435\u043d\u0434\u0441\u043a\u0438\u0435 \u043e-\u0432\u0430","FR":"\u0424\u0440\u0430\u043d\u0446\u0438\u044f","GF":"\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0430\u044f \u0413\u0432\u0438\u0430\u043d\u0430","PF":"\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0430\u044f \u041f\u043e\u043b\u0438\u043d\u0435\u0437\u0438\u044f","TF":"\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0438\u0435 \u042e\u0436\u043d\u044b\u0435 \u0422\u0435\u0440\u0440\u0438\u0442\u043e\u0440\u0438\u0438","HR":"\u0425\u043e\u0440\u0432\u0430\u0442\u0438\u044f","CF":"\u0426\u0410\u0420","TD":"\u0427\u0430\u0434","ME":"\u0427\u0435\u0440\u043d\u043e\u0433\u043e\u0440\u0438\u044f","CZ":"\u0427\u0435\u0445\u0438\u044f","CL":"\u0427\u0438\u043b\u0438","CH":"\u0428\u0432\u0435\u0439\u0446\u0430\u0440\u0438\u044f","SE":"\u0428\u0432\u0435\u0446\u0438\u044f","SJ":"\u0428\u043f\u0438\u0446\u0431\u0435\u0440\u0433\u0435\u043d \u0438 \u042f\u043d-\u041c\u0430\u0439\u0435\u043d","LK":"\u0428\u0440\u0438-\u041b\u0430\u043d\u043a\u0430","EC":"\u042d\u043a\u0432\u0430\u0434\u043e\u0440","GQ":"\u042d\u043a\u0432\u0430\u0442\u043e\u0440\u0438\u0430\u043b\u044c\u043d\u0430\u044f \u0413\u0432\u0438\u043d\u0435\u044f","ER":"\u042d\u0440\u0438\u0442\u0440\u0435\u044f","EE":"\u042d\u0441\u0442\u043e\u043d\u0438\u044f","ET":"\u042d\u0444\u0438\u043e\u043f\u0438\u044f","ZA":"\u042e\u0410\u0420","GS":"\u042e\u0436\u043d\u0430\u044f \u0413\u0435\u043e\u0440\u0433\u0438\u044f \u0438 \u042e\u0436\u043d\u044b\u0435 \u0421\u0430\u043d\u0434\u0432\u0438\u0447\u0435\u0432\u044b \u043e-\u0432\u0430","SS":"\u042e\u0436\u043d\u044b\u0439 \u0421\u0443\u0434\u0430\u043d","JM":"\u042f\u043c\u0430\u0439\u043a\u0430","JP":"\u042f\u043f\u043e\u043d\u0438\u044f"}';
//        
//        $arr_en = (array)json_decode($json_en);
//        $arr_ro = (array)json_decode($json_ro);
//        $arr_ru = (array)json_decode($json_ru);
//        
//        foreach ($arr_en as $code => $name)
//        {
//            $this->db->insert('Country', [
//                'Code' => $code
//            ]);
//            $country_id = $this->db->insert_id();
//            
//            $this->db->insert_batch('CountryLang', [
//                [
//                    'CountryID' => $country_id,
//                    'LangID' => 1,
//                    'Name' => trim($arr_ro[$code])
//                ],
//                [
//                    'CountryID' => $country_id,
//                    'LangID' => 2,
//                    'Name' => trim($arr_ru[$code])
//                ],
//                [
//                    'CountryID' => $country_id,
//                    'LangID' => 3,
//                    'Name' => trim($arr_en[$code])
//                ],
//            ]);
//        }
    }
    
    // city
    public function cities()
    {
        $this->load->model('CityModel');
        $cities = $this->CityModel->getList($this->langID);
        
        $this->render('admin/cities', [
            'cities' => $cities
        ]);
    }
    
    public function city_get()
    {
        $id = $this->input->post('id');
        
        $this->load->model('CityModel');
        $city = $this->CityModel->getForEdit($id);
        
        $this->load->model('CountryModel');
        $res = $this->CountryModel->getList($this->langID);
        
        $countries = [];
        $countries[''] = ' - ';
        foreach ($res as $row)
        {
            $countries[$row->ID] = $row->Name;
        }
        
        $this->load->helper('form');
        
        $this->load->view('admin/city_get', [
            'city' => $city,
            'countries' => $countries
        ]);
    }
    
    public function city_edit()
    {
        $names = $this->input->post('Name');
        $long = $this->input->post('Long');
        $lat = $this->input->post('Lat');
        $countryID = $this->input->post('CountryID');
        $ID = $this->input->post('ID');
        
        $data = [
            'CountryID' => $countryID,
            'Long' => $long,
            'Lat' => $lat
        ];
        
        // save city
        if ($ID > 0)
        {
            $this->db->update('City', $data, ['ID' => $ID]);
        }
        else
        {
            $this->db->insert('City', $data);
            $ID = $this->db->insert_id();
        }
        
        // save city langs
        $this->db->delete('CityLang', ['CityID' => $ID]);
        $langs_insert = [];
        foreach ($names as $langID => $name)
        {
            $langs_insert[] = [
                'CityID' => $ID,
                'LangID' => $langID,
                'Name' => $name
            ];
        }
        $this->db->insert_batch('CityLang', $langs_insert);
    }
    
    public function route_edit($id = 0)
    {
        $this->load->model('RouteModel');
        $this->load->model('CountryModel');
        $this->load->model('CityModel');
        $this->load->model('UserModel');
        
        $route = $this->RouteModel->getByID($id);
        
        $userID = 2;
        $buses = $this->db->get('TransporterBus')->result();
        
        $bus_select_data = [];
        foreach ($buses as $bus)
        {
            $bus_select_data[$bus->ID] = $bus->Name;
        }
        
        $this->db->select('*, TIME_FORMAT(Time, "%H:%i") as Time');
        $this->db->from('RouteStation');
        $this->db->where('RouteID', $id);
        $this->db->order_by('Position');
        $stations = $this->db->get()->result();
        
        $this->load->helper('form');
        
        // get route trips
        $trips = $this->db->select('YEAR(Date) as y, (MONTH(Date) - 1) as m, DAY(Date) as d')->from('RouteTrip')->where('RouteID', $id)->get()->result();
        
        if ($this->input->post())
        {
            $countryFrom = $this->input->post('CountryFrom');
            $cityFrom = $this->input->post('CityFrom');
            $stationFrom = $this->input->post('StationFrom');
            $dayFrom = $this->input->post('DayFrom');
            $timeFrom = $this->input->post('TimeFrom');
            $countryTo = $this->input->post('CountryTo');
            $cityTo = $this->input->post('CityTo');
            $stationTo = $this->input->post('StationTo');
            $dayTo = $this->input->post('DayTo');
            $timeTo = $this->input->post('TimeTo');
            $busID = $this->input->post('TransporterBusID');
            $price = $this->input->post('Price');
            
            $data = [
                'UserID' => $this->input->post('UserID'),
                'TransporterBusID' => $busID,
                'Price' => $price,
                'CountryFrom' => $countryFrom,
                'CountryTo' => $countryTo,
                'CityFrom' => $cityFrom,
                'CityTo' => $cityTo,
                'StationFrom' => $stationFrom,
                'StationTo' => $stationTo,
                'Status' => 'Active'
            ];
            
            if ($id > 0)
            {
                $this->db->update('Route', $data, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Route', $data);
                $id = $this->db->insert_id();
            }
            
            $this->db->delete('RouteStation', ['RouteID' => $id]);
            
            $position = 0;
            $stations_insert = [];
            $stations_insert[] = [
                'RouteID' => $id,
                'CountryID' => $countryFrom,
                'CityID' => $cityFrom,
                'Station' => $stationFrom,
                'Position' => $position,
                'Day' => $dayFrom,
                'Time' => $timeFrom,
                'Type' => 'Start'
            ];
            $position++;
            
            $countries = (array)$this->input->post('CountryID');
            $cities = (array)$this->input->post('CityID');
            $stations = (array)$this->input->post('Station');
            $days = (array)$this->input->post('Day');
            $time = (array)$this->input->post('Time');
            
            foreach ($cities as $key => $value)
            {
                $stations_insert[] = [
                    'RouteID' => $id,
                    'CountryID' => $countries[$key],
                    'CityID' => $value,
                    'Station' => $stations[$key],
                    'Position' => $position,
                    'Day' => $days[$key],
                    'Time' => $time[$key],
                    'Type' => 'Middle'
                ];
                $position++;
            }
            
            $stations_insert[] = [
                'RouteID' => $id,
                'CountryID' => $countryTo,
                'CityID' => $cityTo,
                'Station' => $stationTo,
                'Position' => $position,
                'Day' => $dayTo,
                'Time' => $timeTo,
                'Type' => 'End'
            ];
            
            $this->db->insert_batch('RouteStation', $stations_insert);
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . 'Данные сохранены' . '</div>');
            
            redirect(site_url('admin/route_edit/' . $id));
        }
        
        
        $this->render('admin/route_edit', [
            'route' => $route,
            'buses' => $bus_select_data,
            'stations' => $stations,
            'trips' => $trips
        ]);
    }
    
    public function save_shedule($routeID)
    {
        $data = $this->input->post('data');
        
        $this->db->delete('RouteTrip', ['RouteID' => $routeID]);
        
        $trip_insert = [];
        foreach ($data as $row)
        {
            $trip_insert[] = [
                'RouteID' => $routeID,
                'TransporterBusID' => 1,
                'Date' => $row['date']
            ];
        }
        
        if (count($trip_insert) > 0)
        {
            $this->db->insert_batch('RouteTrip', $trip_insert);
        }
    }
    
    public function toproutes()
    {   
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('TopRoutes', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Ruta a fost eliminata!</div>');
            redirect(site_url('admin/toproutes'));
        }
        
        $from_city = $this->input->post('CityFromID');
        $to_city = $this->input->post('CityToID');
               
        if($from_city && $to_city){
            $data = [
                'CityFromID' => $from_city,
                'CityToID' => $to_city,
                'Order' => '9999'
            ];
            $this->db->insert('TopRoutes', $data);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Ruta a fost adaugata!</div>');
            redirect(site_url('admin/toproutes'));
        }
        
        $data['cities'] = [];
        $data['toproutes'] = $this->db->select("tr.ID, tr.Order, cfl.Name as CityFrom, ctl.Name as CityTo")
                                      ->from("TopRoutes tr")
                                      ->join('City as cf', 'cf.ID = tr.CityFromID')
                                      ->join('CityLang as cfl', "cfl.CityID = cf.ID AND cfl.LangID = ".$this->langID)
                                      ->join('City as ct', 'ct.ID = tr.CityToID')
                                      ->join('CityLang as ctl', "ctl.CityID = ct.ID AND ctl.LangID = ".$this->langID)                           
                                      ->order_by("tr.Order")
                                      ->get()->result();
        
         
        $this->load->model('CityModel');
        $cities = $this->CityModel->getList($this->langID);
        
        foreach($cities as $city){
            $data['cities'][$city->ID] = $city->Name;
        }
                
        $this->load->helper('form');

        $this->render('admin/top_routes', [
            'toproutes' => $data['toproutes'],
            'cities' => $data['cities']
        ]);
    }
    
    public function sort_toproutes()
    {
        if ($this->input->is_ajax_request())
        {
            if($this->input->post('sortedIDs')){
                $routes_sort = json_decode($_POST['sortedIDs'], true);

                foreach ($routes_sort as $sid => $sval){        
                        $sql[1] = ['Order' => $sid];
                        $sql[2] = ['ID' => $sval];
                        $this->db->update("TopRoutes", $sql[1], $sql[2]);
                }
            }
        }
       
    }
    
    public function routes()
    {
        $langID = $this->langID;
        $this->db->select('*, cfl.Name as CityFromName, ctl.Name as CityToName, COUNT(DISTINCT r.UserID) as Count');
        $this->db->from('Route as r');
        $this->db->join('City as cf', 'cf.ID = r.CityFrom');
        $this->db->join('CityLang as cfl', "cfl.CityID = cf.ID AND cfl.LangID = $langID");
        $this->db->join('City as ct', 'ct.ID = r.CityTo');
        $this->db->join('CityLang as ctl', "ctl.CityID = ct.ID AND ctl.LangID = $langID");
        $this->db->where("r.Status", "Active");
        $this->db->group_by("r.CityFrom, r.CityTo");
        $this->db->order_by("r.ID", "DESC");
        $routes = $this->db->get()->result();
        
        $this->render('admin/routes', [
            'routes' => $routes
        ]);
    }
    
    public function transporters_by_cities()
    {
        $from = (int)$this->input->get('from');
        $to = (int)$this->input->get('to');
        
        $this->db->select('*');
        $this->db->from('Route as r');
        $this->db->join('User as u', 'u.ID = r.UserID');
        $this->db->where('r.CityFrom', $from);
        $this->db->where('r.CityTo', $to);
        $this->db->where('r.Status', 'Active');
        $this->db->group_by('u.ID');
        $transporters = $this->db->get()->result();
        
        echo $this->load->view('admin/transporters_by_cities', ['transporters' => $transporters], true);
    }
    
    
    
    public function transporter_buses_list($userID)
    {
        $data['buses'] = $this->db->get_where('TransporterBus', ['UserID' => $userID])->result();
        
        echo $this->load->view('admin/buses_list', $data, true);
    }
    
    public function edit_bus_form()
    {
        $id = (int) $this->input->post('id');
        
        $data['bus'] = $this->db->get_where('TransporterBus', ['ID' => $id], 1)->row();
        $data['bus_images'] = $this->db->get_where('TransporterBusImage', ['TransporterBusID' => $id])->result();
        $this->config->load('bus_templates');
        $data['templates'] = $this->config->item('bus_templates');
        
        echo $this->load->view('admin/edit_bus_form', $data, true);
    }
    
    public function bus_save()
    {
        $ID = (int)$this->input->post('ID');
        $UserID = (int)$this->input->post('UserID');
        $Name = $this->input->post('Name');
        $Template = $this->input->post('Template');
        
        $data = [
            'UserID' => $UserID,
            'Name' => $Name,
            'Template' => $Template
        ];
        
        if ($ID > 0)
        {
            $this->db->update('TransporterBus', $data, ['ID' => $ID]);
        }
        else
        {
            $this->db->insert('TransporterBus', $data);
            $ID = $this->db->insert_id();
        }
        
        $config = array(
            'upload_path'   => 'public/uploads/buses',
            'allowed_types' => 'jpg|gif|png',
            'encrypt_name' => true
        );
        $this->load->library('upload', $config);

        $images = array();
        foreach ($_FILES['Images']['name'] as $key => $image)
        {
            $_FILES['image']['name']= $_FILES['Images']['name'][$key];
            $_FILES['image']['type']= $_FILES['Images']['type'][$key];
            $_FILES['image']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
            $_FILES['image']['error']= $_FILES['Images']['error'][$key];
            $_FILES['image']['size']= $_FILES['Images']['size'][$key];

            $this->upload->initialize($config);

            if ($this->upload->do_upload('image'))
            {
                $images[] = $this->upload->data();
            }
        }

        if (count($images) > 0)
        {
            $images_insert = [];
            foreach ($images as $img)
            {
                $images_insert[] = [
                    'TransporterBusID' => $ID,
                    'Image' => $img['file_name']
                ];
            }
            $this->db->insert_batch('TransporterBusImage', $images_insert);
        }
    }
    
    public function delete_bus_image()
    {
        $ID = $this->input->post('id');
        
        $this->db->delete('TransporterBusImage', ['ID' => $ID], 1);
    }
    
    public function delete_history()
    {
        $ID = $this->input->get('history');
        
        $contentHistory = $this->db->get_where('ContentHistory', ['ID' => $ID], 1)->row();
        
        $this->db->delete('ContentHistory', ['ID' => $ID], 1);
        $this->db->delete('ContentHistoryLang', ['ContentHistoryID' => $ID]);
        
        $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . 'Версия была успешно удалена из истории' . '</div>');
        
        redirect(site_url('admin/edit_' . strtolower($contentHistory->EntityType), ['id' => $contentHistory->EntityID]));
    }
    
}