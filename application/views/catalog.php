<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?=lang('Catalog')?></h1>
        <div class="row">
            <?php foreach ($categories as $cat) { ?>
            <a href="<?= site_url($cat->Link) ?>">
                <div class="col-md-3 text-center">
                    <div>
                        <img src="<?= base_url('public/uploads/categories/' . $cat->Image) ?>" />
                    </div>
                    <div>
                        <?= $cat->Name   ?>
                    </div>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>
</section>