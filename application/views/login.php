<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div  style="width:350px; margin: 0 auto;">
            <h2 class="title text-center"><?= lang('SignIn') ?></h2>
            <form id="login-form" method="post">
                <div class="text-danger">
                    <?= validation_errors() ?>
                </div>
                <div class="text-success">
                    <?= $this->session->flashdata('password_recovered') ? lang('LoginWithNewPassword') : '' ?>
                </div>
                <div class="form-group">
                    <label class="control-label"><?= lang('UserEmail') ?></label>
                    <input name="UserName" type="text" value="<?= set_value('UserName') ?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label class="control-label"><?= lang('Password') ?></label>
                    <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right"><?= lang('SignIn') ?></button>
                    <a href="<?= site_url('password-recovery') ?>" type="submit" class="btn btn-link pull-left"><?= lang('LostPassword') ?></a>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</section>