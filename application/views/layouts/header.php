<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-site-verification" content="-5k2JQH5TldklNOY_pyCE10sE5DeGKma3CaOHDtQN1A" />
        <meta name="description" content="<?= isset($this->seo->Description) && !empty($this->seo->Description) ? $this->seo->Description : (isset($this->seo->Title) ? $this->seo->Title : '') ?>">
        <meta name="keywords" content="<?= isset($this->seo->Keywords) && !empty($this->seo->Keywords) ? $this->seo->Keywords : (isset($this->seo->Title) ? $this->seo->Title : '') ?>">
        <title><?= isset($this->seo->TitleSeo) && !empty($this->seo->TitleSeo) ? $this->seo->TitleSeo : (isset($this->seo->Title) ? $this->seo->Title : 'TORPEDO') ?></title>
        <!--<link rel="stylesheet" href="<?= base_url('public/css/styles.css') ?>">-->
        <link rel="stylesheet" href="<?= base_url('public/css/admin_styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/fancybox/jquery.fancybox.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/easyTree.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/notifIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/select2.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-slider.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/jquery-ui/jquery-ui.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/jquery-ui/jquery-ui.theme.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/owl-carousel/owl.carousel.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/owl-carousel/owl.theme.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/datatables/datatables.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/calendar/bootstrap-year-calendar.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/timepicker/clockpicker.css') ?>">

        <link rel="stylesheet" href="<?= base_url('public/css/stylesheet.css') ?>" />	
        <link rel="stylesheet" href="<?= base_url('public/css/theme.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('public/css/responsive.css') ?>">
        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">

        <script>var LOADER = '<div class="text-center"><img src="<?= base_url('public/images/preloader.gif') ?>" /></div>';</script>
        <script>var SITE_LANG = '<?= $this->config->item('languages')[$this->langID]['Slug'] ?>';</script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="<?= base_url('public/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.pack.js') ?>"></script>
        <script src="<?= base_url('public/js/easyTree.min.js') ?>"></script>
        <script src="<?= base_url('public/js/validate.js') ?>"></script>
        <script src="<?= base_url('public/js/owl-carousel/owl.carousel.js') ?>"></script>
        <script>
            // error messages
            jQuery.extend(jQuery.validator.messages, {
                required: "Completați câmpul",
                remote: "Please fix this field.",
                email: "Vă rugăm să introduceți o adresă de e-mail validă",
                url: "Please enter a valid URL.",
                date: "Please enter a valid date.",
                dateISO: "Please enter a valid date (ISO).",
                number: "Please enter a valid number.",
                digits: "Please enter only digits.",
                creditcard: "Please enter a valid credit card number.",
                equalTo: "Please enter the same value again.",
                accept: "Please enter a value with a valid extension.",
                maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                minlength: jQuery.validator.format("Please enter at least {0} characters."),
                rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
                range: jQuery.validator.format("Please enter a value between {0} and {1}."),
                max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
                min: jQuery.validator.format("Vă rugăm să introduceți o valoare mai mare sau egală cu {0}")
            });
        </script>
        <script src="<?= base_url('public/js/notifIt.min.js') ?>"></script>
        <script src="<?= base_url('public/js/select2.min.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-datepicker.js') ?>"></script>
        <script src="<?= base_url('public/js/ckeditor/ckeditor.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery.tmpl.min.js') ?>"></script>
        <script src="<?= base_url('public/js/moment.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-slider.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery-ui/jquery-ui.min.js') ?>"></script>
        <script src="<?= base_url('public/js/datatables/datatables.min.js') ?>"></script>
        <script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyAzuMfxq6R44YFgDdykza0-spYWoYw-KGs'></script>
        <script src="<?= base_url('public/js/locationpicker.jquery.js') ?>"></script>
        <script src="<?= base_url('public/js/calendar/bootstrap-year-calendar.min.js') ?>"></script>
        <script src="<?= base_url('public/js/calendar/bootstrap-year-calendar.ru.js') ?>"></script>
        <script src="<?= base_url('public/js/timepicker/clockpicker.js') ?>"></script>
        <script src="<?= base_url('public/js/scripts.js') ?>"></script>
    </head>
    <body class="<?= $this->backend ? 'backend-page' : '' ?>">             
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <?php if (!empty($this->_user->Type) && in_array($this->_user->Type, ['Transporter']) && $this->backend) { ?>
                            <a href="<?= site_url('transporter'); ?>" class="header-logo"><img src="<?= base_url('public/images/logotype.png') ?>" alt=""></a>
                        <?php } elseif (!empty($this->_user->Type) && in_array($this->_user->Type, ['Admin']) && $this->backend) { ?>
                            <a href="<?= site_url('admin'); ?>" class="header-logo"><img src="<?= base_url('public/images/logotype.png') ?>" alt=""></a>
                        <?php } else { ?>
                            <a href="<?= site_url(); ?>" class="header-logo"><img src="<?= base_url('public/images/logotype.png') ?>" alt=""></a>
                        <?php } ?>
                        <a href="<?= base_url('ro/' . $this->uri->uri_string() . (empty($_SERVER['QUERY_STRING']) ? '' : "?" . $_SERVER['QUERY_STRING'])) ?>" class="lang-item">Rom</a>
                        <a href="<?= base_url('ru/' . $this->uri->uri_string() . (empty($_SERVER['QUERY_STRING']) ? '' : "?" . $_SERVER['QUERY_STRING'])) ?>" class="lang-item">Рус</a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">

                        <div class="user-bar dropdown">
                            <div id="user-bar" class="user-bar-btn"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-id-card-o"></i>
                                <span class="hidden-xs"><?= lang('MyCabinet') ?></span>
                            </div>
                            <ul class="dropdown-menu" aria-labelledby="user-bar">
                                <?php if (!empty($this->_user)) { ?>
                                    <li><a href="<?= site_url('user') ?>"><?= lang('Orders') ?></a></li>
                                    <li><a href="<?= site_url($this->accountLink) ?>"><?= lang('UserAccount') ?></a></li>
                                    <li><a href="<?= site_url('logout') ?>"><?= lang('Logout') ?></a></li>
                                <?php } else { ?>
                                    <li><a href="<?= site_url('login') ?>"><?= lang('SignIn') ?></a></li>
                                    <li><a href="<?= site_url('registration') ?>"><?= lang('Registration') ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <?php if (!empty($this->_user->Type) && in_array($this->_user->Type, ['Admin']) && $this->backend) { ?>

            <nav class="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<?= menu($this->menu[$this->menuType]) ?>-->
                            <ul>
                                <li><a href="<?= site_url('admin/index') ?>"><?= lang('Dashboard') ?></a></li>
                                <li><a href="#"><?= lang('Content') ?> <i class="pull-right fa fa-caret-down"></i></a>
                                    <ul>
                                        <li><a href="<?= site_url('admin/news') ?>"><?= lang('News') ?></a></li>
                                        <li><a href="<?= site_url('admin/toproutes') ?>"><?= lang('TopRoutes') ?></a></li>
                                        <li><a href="<?= site_url('admin/pages') ?>"><?= lang('Pages') ?></a></li>
                                        <!--<li><a href="<?= site_url('admin/slider') ?>"><?= lang('Slider') ?></a></li>-->
                                        <li><a href="<?= site_url('admin/menus') ?>"><?= lang('Menus') ?></a></li>
                                        <li><a href="<?= site_url('admin/seo') ?>"><?= lang('Seo') ?></a></li>
                                    </ul>
                                </li>
                                <li><a href="<?= site_url('admin/cities') ?>"><?= lang('Cities') ?></a></li>
                                <li><a href="<?= site_url('admin/transporters') ?>"><?= lang('Carriers') ?></a></li>
                                <li><a href="<?= site_url('admin/routes') ?>"><?= lang('Route') ?></a></li>
                                <li><a href="<?= site_url('admin/users/?Type=Manager') ?>"><?= lang('Managers') ?></a></li>
                                <li><a href="<?= site_url('admin/users') ?>"><?= lang('Users') ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        <?php } ?>

        <?php if (!empty($this->_user->Type) && in_array($this->_user->Type, ['Transporter']) && $this->backend) { ?>

            <nav class="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <ul>
                                <li><a href="<?= site_url('transporter/index') ?>"><?= lang('Dashboard') ?></a></li>
                                <li><a href="<?= site_url('transporter/orders') ?>"><?= lang('Orders') ?></a></li>
                                <li><a href="<?= site_url('transporter/routes') ?>"><?= lang('Route') ?></a></li>
                                <li><a href="<?= site_url('transporter/buses') ?>">Автобусы</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        <?php } ?>
        

        <?php if (!empty($this->_user->Type) && in_array($this->_user->Type, ['Seo']) && $this->backend) { ?>

            <nav class="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <ul>
                                <li><a href="<?= site_url('admin/menus') ?>"><?= lang('Menus') ?></a></li>
                                <li><a href="<?= site_url('admin/pages') ?>"><?= lang('Pages') ?></a></li>
                                <li><a href="<?= site_url('admin/news') ?>"><?= lang('News') ?></a></li>
                                <li><a href="<?= site_url('admin/seo') ?>"><?= lang('Seo') ?></a></li>
                                <li><a href="<?= site_url('admin/cities') ?>"><?= lang('Cities') ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        <?php } ?>
        