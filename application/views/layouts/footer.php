<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h3><?= lang('Informations') ?></h3>
                        <ul class="footer-nav">
                            <?= menu(isset($this->menu['Footer1']) ? $this->menu['Footer1'] : [], false) ?>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <h3><?= lang('Services') ?></h3>
                        <ul class="footer-nav">
                            <?= menu(isset($this->menu['Footer2']) ? $this->menu['Footer2'] : [], false) ?>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <h3><?= lang('Partners') ?></h3>
                        <ul class="footer-nav">
                            <?= menu(isset($this->menu['Footer3']) ? $this->menu['Footer3'] : [], false) ?>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <h3><?= lang('Support') ?></h3>
                        <ul class="footer-nav">
                            <?= menu(isset($this->menu['Footer4']) ? $this->menu['Footer4'] : [], false) ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">                    
                <div class="sic-socials">
                    <strong><?= lang('Socials') ?>:</strong>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-vk"></i></a>
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="subfooter hidden-sm hidden-xs">
    <div class="container">
        <strong><?= lang('WeAccept') ?>: </strong>
        <img src="<?= base_url('public/images/pay.png'); ?>" alt="">
    </div>
</div>
<div class="subsub">
    <div class="container">
        <div class="text-center">
            Există o mulţime de variaţii disponibile ale pasajelor Lorem Ipsum, dar majoritatea lor au suferit alterări într-o oarecare măsură prin infiltrare de elemente de umor, sau de cuvinte luate aleator, care nu sunt câtuşi de puţin credibile. Daca vreţi să folosiţi un pasaj de Lorem Ipsum, trebuie să vă asiguraţi că nu conţine nimic stânjenitor ascuns printre randuri. 
        </div>
    </div>
</div>
</body>

<script>
    $(document).on('click', '*[add-to-cart]', function () {
        $.post('/main/cart_add', {productID: $(this).attr('add-to-cart')}, function (json) {
            notif({
                msg: json.messages,
                type: json.result,
                position: "right"
            });
            $('#cart-amount').text(json.amount);
        }, 'json');
    });

    $(document).on('click', function (e) {
        $('[data-toggle="popover"],[data-original-title]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false  // fix for BS 3.3.6
            }

        });
    });

    $(function () {
        // Enables popover
        $("[data-toggle=popover]").popover();
    });

    setTimeout(function () {
        $('.alert.alert-success').slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

    $('.select-2').select2({
        'theme': 'bootstrap'
    });

    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy'
    });

    $('a.fancybox').fancybox();


    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });


    $(document).ready(function () {
        $('.datatable').DataTable();
    });

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-92286830-1', 'auto');
  ga('send', 'pageview');
 
</script>
</html>