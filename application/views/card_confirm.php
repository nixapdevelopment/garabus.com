<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="row">
            <br />
            <br />
            <div class="alert alert-info" role="alert">
                Перенаправление на сайт платёжной системы ...
            </div>
            <div class="col-md-6 col-md-offset-3 text-center">
                <form id="card-form" method="POST" action="https://www.bpay.md/user-api/payment1">
                    <input type="hidden" name="data" value="<?= $data ?>" />
                    <input type="hidden" name="key" value="<?= $sign ?>" />
                    <input type="submit" class="hidden" value="GO" />
                </form>
            </div>
        </div>
    </div>
</section>
<script>
$('#card-form').submit();
</script>