<div class="row">
    <div class="col-md-3 col-sm-6">
        <div class="si-title hidden-sm hidden-xs"><?= lang('Seats')?></div>
        <!--div class="si-form">
            <input type="text" name="" placeholder="<?= lang('FirstNLastName') ?>" />
            <input type="text" name="" placeholder="<?= lang('Phone') ?>" />
            <input type="text" name="" placeholder="<?= lang('Email') ?>" />

            <p>&nbsp;</p>
            <p><?= lang('WishRegEnterPassword') ?>:</p>  

            <input type="text" name="" placeholder="<?= lang('Password') ?>" />
            <input type="text" name="" placeholder="<?= lang('ConfirmationPassword') ?>" />
        </div-->
        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#select-seat"><?= lang('ChooseYourSeat') ?></button>
        <hr />
        <p style="font-size: 18px;"><?=lang('ChosenSeats')?>: <span style="display: inline; font-weight: 600; font-size: 18px;" id="seats-labels">-</span></p>
        <hr />
        <p style="font-size: 18px; font-weight: bold;"><?=lang('Cost')?>: <span style="display: inline-block; font-size: 18px;" class="price-seats">0</span> MDL</p>
        <hr />
        <!--button onclick="doOrder(this)" class="btn btn-danger btn-lg">Cumpără</button-->
    </div>
    <div class="col-md-5 col-sm-6">
        <div class="si-title"><?= lang('Transporter') ?></div>
        <table class="table table-bordered">
            <tr>
                <td><?= lang('Company') ?>:</td>
                <td><a href="#"><?= $transporter->CompanyName ?></a></td>
            </tr>
            <tr>
                <td><?= lang('IDNO') ?>:</td>
                <td><a href="#"><?= $transporter->IDNO ?></a></td>
            </tr>
            <tr>
                <td><?= lang('Bus') ?>:</td>
                <td><a href="#"><?= $bus->Name ?></a></td>
            </tr>
            <tr>
                <td><?= lang('Phone') ?>:</td>
                <td><a href="#"><?= $transporter->Phone ?></a></td>
            </tr>
        </table>
        <div class="si-gallery hidden-sm hidden-xs">
            <div class="row">
                <?php foreach ($bus_images as $bi) { ?>
                <div style="margin-bottom: 15px;" class="col-md-3 col-sm-6">
                    <a rel="images-bus-<?= $bi->TransporterBusID ?>" class="si-gallery-item fancybox" href="<?= base_url('public/uploads/buses/' . $bi->Image) ?>"><img src="<?= base_url('public/uploads/buses/' . $bi->Image) ?>" alt=""></a>
                </div>
                <?php } ?>
            </div>
        </div>
        <!--a href="#"><?= lang('ViewTransporterPage') ?></a-->
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="si-title" id="si-traseu"><?= lang('Route') ?></div>
        <ul class="si-traseu">
            <?php foreach ($stations as $station) { ?>
            <li><?= $station->Name ?> <?= empty($station->Station) ? '' : ('(' . $station->Station . ')') ?> <div class="pull-right"><?= date('d.m.Y', strtotime($date) + (($station->Day - 1) * 86400)) ?> - <?= $station->Time ?></div></li>
            <?php } ?>
        </ul>                        
    </div>
</div>
<hr />
<div style="display: none;" class="order-info">
    <form class="tickets-info">
        <strong><?=lang('ContactData')?></strong>
        <?php
        $readonly = "";
        if(isset($user->Name)){
            $fullname = explode(" ", $user->Name);
            $name = $fullname[0];
            $surname = isset($fullname[1]) ? $fullname[1] : '';
            $readonly = "";
        }
        ?>
        <div class="row">
            <div class="col-md-3">
                <input type="text" name="cfname" class="form-control" placeholder="Имя" value="<?=isset($name) ? $name : '' ?>" <?=$readonly?> />
            </div>
            <div class="col-md-3">
                <input type="text" name="clname" class="form-control" placeholder="Фамилия" value="<?=isset($surname) ? $surname : '' ?>" <?=$readonly?> />
            </div>
            <div class="col-md-3">
                <input type="text" name="cphone" class="form-control" placeholder="Телефон" value="<?=isset($user->Phone) ? $user->Phone : '' ?>" <?=$readonly?> />
            </div>
            <div class="col-md-3">
                <input type="text" name="cemail" class="form-control" placeholder="Эл. почта" value="<?=isset($user->UserName) ? $user->UserName : '' ?>" <?=$readonly?> />
            </div>
        </div>
        <hr />
        <strong><?=lang('TicketsData')?></strong>
        <div class="tickets-data">
            
        </div>
    </form>
    <div class="tickets-info-btns">
        <div class="row">
            <div class="text-center">
                <div class="pay-buttons">
                    <button data-payment-type="card" style="display: inline-block; width: auto;" type="button" class="btn btn-default"><i class="fa fa-credit-card"></i> <?=lang('Card')?></button>&nbsp;
                    <button data-payment-type="paypal" style="display: inline-block; width: auto;" type="button" class="btn btn-default"><i class="fa fa-paypal" aria-hidden="true"></i> PayPal</button>&nbsp;
                    <button  data-payment-type="cash" style="display: inline-block; width: auto;" type="button" class="btn btn-default"><i class="fa fa-money" aria-hidden="true"></i> <?=lang('Cash')?></button>
                </div>
            </div>
            <div style="margin-top: 10px;" class="text-center">
                <button onclick="doOrder(this)" style="display: inline-block; width: auto;" class="btn btn-danger btn-lg"><?=lang('ToPayment')?></button>
            </div>
        </div>
    </div>
</div>

<script id="ticket-row-tmpl" type="text/x-jquery-tmpl">
<div style="margin: 15px -15px;" class="row">
    <div class="col-md-3">
        <input type="text" class="form-control" name="fname[]" placeholder="<?=lang('Name')?>" />
    </div>
    <div class="col-md-3">
        <input type="text" class="form-control" name="lname[]" placeholder="<?= lang('FamilyName')?>" />
    </div>
    <div class="col-md-2">
        <select onchange="$(this).closest('.row').find('.ticket-row-price').html($(this).find('option:selected').attr('data-price'))" name="price[]" class="form-control">
                <option data-price="<?= number_format($route->Price) ?>" value="adult"><?= lang('Adult')?></option>
                <option data-price="<?= number_format(round($route->Price, 2)) ?>" value="child"><?=lang('Child')?></option>
        </select>
    </div>
    <div class="col-md-2">
        <input readonly type="text" class="form-control" placeholder="<?=lang('Seat')?>" value="${value} <?=lang('SeatLower')?>" />
        <input type="hidden" name="seat[]" value="${value}" />
    </div>
    <div class="col-md-2">
        <span style="font-size: 25px; line-height: 1.8; display: inline;" class="ticket-row-price"><?= number_format($route->Price) ?></span> <span style="font-size: 25px; line-height: 1.8; display: inline;">MDL</span>
    </div>
    <input type="hidden" name="route" value="<?= $route->ID ?>" />
</div>
</script>

<script>window.tprice = <?= $route->Price ?>;</script>