<div class="container">
    <h1><?= lang('Order') ?> # <?= $order->OrderID ?></h1>
    <div>
        <a class="btn btn-info" href="<?= site_url('user/orders', $_GET) ?>"><i class="glyphicon glyphicon-chevron-left"></i> <?= lang('BackToList') ?></a>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= lang('OrderDelails') ?></h3>
                </div>
                <div class="panel-body">
                    <p>ID: # <?= $order->OrderID ?></p>
                    <p><?= lang('Date') ?>: <?= date('d.m.Y - H:i:s', strtotime($order->Date)) ?></p>
                    <p><?= lang('PaymentType') ?>: <?= $order->PaymentType ?></p>
                    <p><?= lang('Status') ?>: <?= $order->Status ?></p>
                    <p><strong><?= lang('Amount') ?>: <?= $order->Amount ?> MDL</strong></p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= lang('OrderProducts') ?></h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><?= lang('ProductName') ?></th>
                                <th><?= lang('Quantity') ?></th>
                                <th><?= lang('Price') ?></th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($order_products as $product) { ?>
                            <tr>
                                <td>(<strong><?= $product->CityFrom ?> -> <?= $product->CityTo ?></strong>) <?= lang('Seats') ?> № <?= $product->Seat ?> (<?= $product->SeatType == 'Child' ? 'Ребёнок' : 'Взрослый' ?>)</td>
                                <td><?= (int)$product->Quantity ?></td>
                                <td><?= $product->Price ?></td>
                                <td><?= number_format($product->Price * $product->Quantity, 2) ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-right">
                        <h3><?=lang('CartTotal')?>: <?= $order->Amount ?> MDL</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>