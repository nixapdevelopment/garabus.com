<div class="container">
    <div class="dash-block">
        <h3 class="text-center"><?= lang('Settings') ?></h3>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <form id="registration-form" method="post" class="col-md-offset-2 col-md-8" enctype="multipart/form-data">
            <div class="text-danger">
                <?= validation_errors() ?>
            </div>
            <div class="row">
                <div class="col-md-6">                    
                    <div class="form-group">
                        <label class="control-label"><?= lang('Name') ?> <span class="text-danger">*</span></label>
                        <input name="Name" type="text" value="<?= set_value('Name', isset($user->Name) ? $user->Name : '') ?>" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Phone') ?> <span class="text-danger">*</span></label>
                        <input name="Phone" type="text" value="<?= set_value('Phone', isset($user->Phone) ? $user->Phone : '') ?>" required class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Country') ?> <span class="text-danger">*</span></label>                                
                        <?= form_dropdown('Country', $countries, set_value('Country', isset($user->CountryID) ? $user->CountryID : ''), 'class="form-control select2" required') ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('City') ?> <span class="text-danger">*</span></label>
                        <?= form_dropdown('City', $cities, set_value('City', isset($user->CityID) ? $user->CityID : ''), 'class="form-control select2" required') ?>
                    </div>
                </div>
                <div class="col-md-6">                    
                    <div class="form-group">
                        <label class="control-label"><?= lang('UserEmail') ?></label>
                        <input name="UserName" type="email" value="<?= set_value('UserName', isset($user->UserName) ? $user->UserName : '') ?>" class="form-control" readonly=""/>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Password') ?></label>
                        <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('PasswordConfirmation') ?></label>
                        <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?= lang('Save') ?></button>
            </div>
        </form>
    </div>
</div>
<script>
$(function() {
    appplySelect2ToCC();// country and city selects
    applyLocationSelect('select[name=Country]', 'select[name=City]'); 
});

function appplySelect2ToCC(){
    applySelect2('select[name=Country]');
    applySelect2('select[name=City]');
}

</script>