<section class="content">
    <div class="container">
        <div class="dash-block">
            <h3><?= lang('Orders') ?></h3>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th><?= lang('Date') ?></th>
                                <th><?= lang('ProductName') ?></th>
                                <th><?= lang('Amount') ?></th>
                                <th><?= lang('PaymentType') ?></th>
                                <th><?= lang('Status') ?></th>
                                <th class="text-center"></th>                            
                            </tr>
                        </thead>
                        <tbody>
                        <form>
                            <tr>
                                <td></td>
                                <td>
                                    <div style="max-width: 180px;" class="input-group">
                                        <input value="<?= $this->input->get('Date') ?>" onchange="return false;" type="text" name="Date" class="form-control datepicker" />
                                        <span class="input-group-btn">
                                            <button onclick="$('input[name=Date]').val('')" style="margin-top: 1px;" class="btn btn-danger" type="button">Clear</button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?= form_dropdown('PaymentType', ['' => lang('All'), 'Cash' => lang('Cash'), 'Card' => lang('Card'), 'Paypal' => 'Paypal'], $this->input->get('PaymentType'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <?= form_dropdown('Status', ['' => lang('All'), 'Pending' => lang('Pending'), 'Paid' => lang('Paid')], $this->input->get('Status'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> <?= lang('Filter') ?></button>
                                    <a href="<?= site_url('user/orders') ?>" type="submit" class="btn btn-danger"><?= lang('ClearFilters') ?></a>
                                </td>
                            </tr>
                        </form>
                        <?php foreach ($orders as $order) { ?>
                            <tr>
                                <td class="text-center"><a href="<?= site_url('user/order/' . $order->OrderID) ?>"><?= $order->OrderID ?></a></td>
                                <td><?= date('d.m.Y H:m', strtotime($order->Date)) ?></td>
                                <td><?= $order->CityFrom ?> -> <?= $order->CityTo ?></td>
                                <td><?= $order->Amount ?></td>
                                <td><?= $order->PaymentType ?></td>
                                <td><?= $order->OrderStatus ?></td>
                                <td class="text-center">
                                    <a href="<?= site_url('user/order/' . $order->OrderID, $_GET) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-center">
                        <?= $pagination ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $('.select-2').select2({
            theme: 'bootstrap'
        });

        $('.datepicker').datepicker({
            format: 'dd.mm.yyyy',
            maxDate: moment(),
            clearBtn: true
        });

    </script>
</section>