<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <nav class="categories">
                    <?= $this->data['categories'] ?>
                </nav>
            </div>
            <div class="col-md-9">
                <!--  Promo products -->
                <?php if (count($products) > 0) { ?>
                <div class="container">
                    <div class="home-promo">
                        <h3><?= $page->Title ?></h3>
                        <div class="row">
                            <?php foreach ($products as $product) { ?>
                            <div class="product-item col-md-3">
                                <div class="product-item-inn">
                                    <div class="product-prop">
                                        <div class="product-new"><i class="fa fa-bolt"></i> <?= lang('NewLabel')?></div>                          
                                    </div>
                                    <a href="<?= site_url($product->Link) ?>">
                                        <img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="<?= $product->Name ?>" />
                                        <strong><?= $product->Name ?></strong>
                                    </a>
                                    <div class="product-bottom">
                                        <div class="product-price"><?= $product->Price ?> <?= lang('CurrencyNameLowerCase')?></div>
                                        <div class="product-panel">  
                                            <button add-to-cart="<?= $product->ProductID ?>"><?= lang('buy')?></button>                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php } ?>
                <div>
                    <?= $pagination ?>
                </div>
            </div>
        </div>
    </div>
</section>