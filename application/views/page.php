<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?= $page->Title ?></h1>
        <br />
        <div>
            <?= $page->Text ?>
        </div>
        
        <div class="row">
            <div class="col-md-<?= count($files) ? 9 : 12 ?>">
                <div class="row">
                    <?php foreach ($images as $image) { ?>
                        <div class="col-md-4">
                            <a class="fancybox" rel="page-image" href="<?= base_url('public/uploads/pages/' . $image->Image) ?>">
                                <img class="img-thumbnail" src="<?= base_url('public/uploads/pages/' . $image->Thumb) ?>" />
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if (!empty($files)) { ?>
                <div class="col-md-3">
                    <div class="list-group">
                        <?php foreach ($files as $file) { ?>
                        <a target="_blank" href="<?= base_url('public/uploads/pages/' . $file->Path) ?>" class="list-group-item"><?= $file->Name ?></a>
                        <?php } ?>
                    </div>
                </div>     
            <?php } ?>
        </div>
        
    </div>
</section>