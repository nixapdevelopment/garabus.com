<section class="content">
    <div class="container">
        <div class="home-first">
            
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <?php $main = reset($images) ?>
                        <img class="img-thumbnail" src="<?= base_url('public/uploads/products/' . $main->Image) ?>" />
                    </div>
                    <hr />
                    <div class="row">
                        <?php foreach ($images as $img) { ?>
                        <div class="col-md-3">
                            <img class="img-thumbnail" src="<?= base_url('public/uploads/products/' . $main->Thumb) ?>" />
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <h1><?= $product->Name ?></h1>
                    <hr />
                    <div style="font-size: 18px;">Pret: <?= $product->Price == $product->OldPrice ? $product->Price : $product->Price . ' <strike class="text-muted">' . $product->OldPrice . '</strike>' ?></div>
                    <br />
                    <div>Sold: <?= (int)$product->Stock ?></div>
                    <br />
                    <a add-to-cart="<?= $product->ProductID ?>" class="btn btn-success"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp; <?= lang('AddToCart') ?></a>
                    <hr />
                    <div><?= $product->ProductText ?></div>
                    <hr />
                    <br />
                    <table class="table table-striped">
                        <?php foreach ($filters as $filter) { ?>
                        <tr>
                            <td><?= $filter->Name ?></td>
                            <td><?= $filter->Value ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>