<?php ?>


<section class="search">
    <div class="container">
        <div class="search-title"><?= lang('SearchFormText') ?></div>
        <?php $this->load->view('search-route-form') ?>
    </div>
</section>

<section class="search-counter">
    <div class="container">
        <p><?= lang('OutputOfResults') ?> <strong><?= count($routes) ?> <?= lang('Results') ?></strong></p>
    </div>
</section>

<div class="modal fade" id="select-seat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div role="document" class="modal-bus">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('SeatSelect') ?>:</h4>
            </div>
            <section class="search-counter">
                <div class="container">
                    <div class="row">
                        <div id="bus-template-wrap" class="col-md-12">
                            
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-9">
                        <div class="alert alert-info pull-left">
                            Расположение мест в автобусе может отличаться от расположения в макете.
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('Close') ?></button>
                        <button onclick="selectSeats()" type="button" class="btn btn-primary"><?= lang('iSelect') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="search-results">
    <div class="container">
        <div class="si-head hidden-sm hidden-xs">
            <div class="row">
                <div class="sih-item col-md-2">
                    <?= lang('Departure') ?>
                </div>
                <div class="sih-item col-md-2">
                    <?= lang('Arrival') ?>
                </div>
                <div class="sih-item col-md-1">

                </div>
                <div class="sih-item col-md-4">
                    <?= lang('Route') ?>
                </div>
                <div class="sih-item col-md-2">
                    <?= lang('Cost') ?>
                </div>
                <div class="sih-item col-md-1">
                    <?= lang('Seats') ?>
                </div>
            </div>
        </div>

        <?php foreach ($routes as $route) { ?>
        <div class="search-id">
            <div data-route-id="<?= $route->ID ?>" onclick="return false;" class="search-row row" role="button" data-toggle="collapse" href="#result-<?= $route->ID ?>" aria-expanded="false">
                <div class="col-md-2 col-sm-2 col-xs-3">                    
                    <time><?= date('H:i', strtotime($route->FromTime)) ?></time>
                    <span><?= $route->FromName ?>, <?= $route->StationFrom ?></span>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-3">              
                    <time><?= date('H:i', strtotime($route->ToTime)) ?></time>
                    <span><?= $route->ToName ?>, <?= $route->StationTo ?></span>
                </div>
                <div class="col-md-1 hidden-sm hidden-xs">

                </div>
                <div class="col-md-4 col-sm-4 hidden-xs">
                    <strong><?= $route->FromName ?> <i class="fa fa-long-arrow-right"></i> <?= $route->ToName ?></strong>
                    <small><?= $route->CompanyName ?></small>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-6">
                    <a class="search-price"><?= $route->Price ?> MDL</a>
                </div>
                <div class="col-md-1 col-sm-1 search-locuri hidden-xs">
                    <?= $route->AvalableSeats ?>
                </div>
            </div>
            <div class="si-details collapse" id="result-<?= $route->ID ?>">
                <div style="height: 300px;">
                    
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<?php if (!empty($this->seo->Title)) { ?>
<section class="search-description">
    <div class="container">
        <div class="text-center">
            <h1><?= $this->seo->Title ?></h1>
            <p>
                <?= $this->seo->Text ?>
            </p>
        </div>
    </div>
</section>
<?php } ?>

<script>
    
    window.selectedSeats = [];

    $('.search-results div[data-route-id]').click(function(){
        var elem = $(this);
        $.post('<?= site_url('main/route_info') ?>', {id: elem.attr('data-route-id'), date: '<?= $this->input->get('date') ?>'}, function(json){
            elem.closest('.search-id').find('.si-details').html(json.info);
            $('#bus-template-wrap').html(json.template);
        }, 'json');
    });
    
    function selectSeats()
    {
        var data = $('#bus-template').serializeArray();
        if (data.length == 0)
        {
            alert('Выберите минимум 1 место');
            return false;
        }
        $('.si-details.in').find('.tickets-info .tickets-data').html('');
        window.selectedSeats = [];
        $.each(data, function(k, v){
            window.selectedSeats.push(v.value);
        });
        $('.si-details.in').find('.tickets-info .tickets-data').append($('#ticket-row-tmpl').tmpl(data));
        console.log($('.si-details.in').find('.tickets-info .tickets-data'));
        $('.si-details.in').find('.order-info').show();
        
        var sel = window.selectedSeats;
        $('.si-details.in #seats-labels').text(sel.join(', '));
        $('.si-details.in .price-seats').text(window.selectedSeats.length * window.tprice);
        $('#select-seat').modal('hide');
    }
    
    $(document).on('click', '.pay-buttons button', function(){
        $('.pay-buttons button').removeClass('btn-primary');
        $(this).addClass('btn-primary');
    });
    
    function doOrder(submit)
    {
        var btns = $(submit).closest('.tickets-info-btns');
        var form = btns.prev('form');
        var data = form.serialize();
        var payment_type = btns.find('.pay-buttons button.btn-primary').attr('data-payment-type');
        
        var valid = true;
        form.find('input[name]').removeAttr('style');
        form.find('input[name]').each(function(){
            if ($(this).val() == '')
            {
                $(this).attr('style', 'border-color:#e74c3c');
                valid = false;
            }
        });
        
        if (valid)
        {
            if (typeof payment_type == 'undefined')
            {
                alert('<?=lang('SelectPaymentType')?>');
                return;
            }
        }
        else
        {
            return;
        }
        
        $.post('<?= site_url('main/do_order') ?>', {data: data, payment_type: payment_type, date: '<?= $this->input->get('date') ?>'}, function(json){
            if (json.type == 'Cash')
            {
                window.location.href = '<?= site_url('thankyou') ?>/' + json.id;
            }
            else if (json.type == 'Card')
            {
                window.location.href = '<?= site_url('card') ?>/' + json.id;
            }
        }, 'json');
    }

</script>