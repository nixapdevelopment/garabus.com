<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="home-first">
            <h1 class="text-center"><?= lang('Registration') ?></h1>
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="text-danger">
                        <?= validation_errors() ?>
                    </div>
                    <div class="row selectUserType">
                        <div class="col-md-6">
                            <button type="button" id="client-reg" class="form-control btn btn-info active"><?= lang('Client') ?></button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="transporter-reg" class="form-control btn btn-info"><?= lang('Transporter') ?></button>
                        </div>
                    </div>
                    <br />
                    <div class="client-reg">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-6">  
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Name') ?> <span class="text-danger">*</span></label>
                                        <input name="Name" type="text" value="<?= set_value('Name') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Phone') ?> <span class="text-danger">*</span></label>
                                        <input name="Phone" type="text" value="<?= set_value('Phone') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Country') ?> <span class="text-danger">*</span></label>                                
                                        <?= form_dropdown('Country', $countries, false, 'class="form-control select2" required') ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('City') ?> <span class="text-danger">*</span></label>
                                        <select name="City" class="form-control" required>
                                            <option valu="0"><?= lang('SelectCity') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('UserEmail') ?> <span class="text-danger">*</span></label>
                                        <input name="UserName" type="text" value="<?= set_value('UserName') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Password') ?> <span class="text-danger">*</span></label>
                                        <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('PasswordConfirmation') ?> <span class="text-danger">*</span></label>
                                        <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label><input required checked type="checkbox" name="Terms" value="1"> <?= lang('Accept') ?> <a href="#"><?= lang('Terms') ?></a></label>
                                    </div>
                                </div>
                                <input type="hidden" name="UserType" value="User" />
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right"><?= lang('Send') ?></button>
                                <a href="<?= site_url('login') ?>" type="submit" class="btn btn-link pull-left"><?= lang('Login') ?></a>
                                <div class="clearfix"></div>
                            </div>
                        </form>                    
                    </div>
                    
                    <div class="transporter-reg" style="display: none">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group company-fields">
                                        <label class="control-label"><?= lang('CompanyName') ?> <span class="text-danger">*</span></label>
                                        <input name="CompanyName" type="text" value="<?= set_value('CompanyName') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group company-fields">
                                        <label class="control-label"><?= lang('IDNO') ?> <span class="text-danger">*</span></label>
                                        <input name="IDNO" type="text" value="<?= set_value('IDNO') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group company-fields">
                                        <label class="control-label"><?= lang('BankName') ?> <span class="text-danger">*</span></label>
                                        <input name="BankName" type="text" value="<?= set_value('BankName') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group company-fields">
                                        <label class="control-label"><?= lang('IBAN') ?> <span class="text-danger">*</span></label>
                                        <input name="IBAN" type="text" value="<?= set_value('IBAN') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Country') ?> <span class="text-danger">*</span></label>                                
                                        <?= form_dropdown('Country', $countries, false, 'class="form-control select2" required') ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('City') ?> <span class="text-danger">*</span></label>
                                        <select name="City" class="form-control" required>
                                            <option valu="0"><?= lang('SelectCity') ?></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><input required checked type="checkbox" value="1" name="Terms"> <?= lang('Accept') ?> <a href="#"><?= lang('Terms') ?></a></label>
                                    </div>                            
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Name') ?> <span class="text-danger">*</span></label>
                                        <input name="Name" type="text" value="<?= set_value('Name') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Phone') ?> <span class="text-danger">*</span></label>
                                        <input name="Phone" type="text" value="<?= set_value('Phone') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Address') ?> <span class="text-danger">*</span></label>
                                        <input type="text" name="Address" class="form-control" value="<?= set_value('Address') ?>" required />
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('UserEmail') ?> <span class="text-danger">*</span></label>
                                        <input name="UserName" type="text" value="<?= set_value('UserName') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Password') ?> <span class="text-danger">*</span></label>
                                        <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('PasswordConfirmation') ?> <span class="text-danger">*</span></label>
                                        <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" required />
                                    </div>                            
                                </div>
                                <input type="hidden" name="UserType" value="Transporter" />
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right"><?= lang('Send') ?></button>
                                <a href="<?= site_url('login') ?>" type="submit" class="btn btn-link pull-left"><?= lang('Login') ?></a>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    appplySelect2ToCC();// country and city selects
    
    applyLocationSelect('select[name=Country]', 'select[name=City]'); 
    //change country default to Moldova
    $('select[name=Country]').val('147');
    $('select[name=Country]').change();
    
    $('.selectUserType button').click(function(){
        $('.selectUserType button').removeClass('active');
        $(this).addClass('active');
        
        if($(this).attr('id') == 'transporter-reg'){
            $('.client-reg').hide();
            $('.transporter-reg').show();
            appplySelect2ToCC();
        }else{
            $('.transporter-reg').hide();
            $('.client-reg').show();
            appplySelect2ToCC();
        }
        
    });
    
    

    $('select[name="ClientType"]').change(function(){
        if ($(this).val() == 'Angro')
        {
            $('.company-fields input').removeAttr('disabled');
        }
        else
        {
            $('.company-fields input').attr('disabled', 'disabled');
        }
    });
});

function appplySelect2ToCC(){
    applySelect2('select[name=Country]');
    applySelect2('select[name=City]');
}

</script>