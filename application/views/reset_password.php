<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="home-first">
            <div>
                <h1 class="text-center"><?= lang('ResetPassword') ?></h1>
                <form id="login-form" method="post">
                    <div class="text-danger">
                        <?= validation_errors() ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('RecoveryToken') ?></label>
                        <input type="text" name="Token" class="form-control" value="<?= set_value('Token', isset($_GET['token']) ? $_GET['token'] : '') ?>" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Password') ?></label>
                        <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('PasswordConfirmation') ?></label>
                        <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right"><?= lang('Send') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>