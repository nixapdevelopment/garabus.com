<form id="bus-template" style="width: 1170px; margin: 0 auto;" class="bus-model si-model">
    <div class="si-model-on">
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-37" name="seat[]" value="37" type="checkbox">
                <label for="seat-37">37</label>
            </div>
            <div class="si-model-row">
                <input id="seat-38" name="seat[]" value="38" type="checkbox">
                <label for="seat-38">38</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-33" name="seat[]" value="33" type="checkbox">
                <label for="seat-33">33</label>
            </div>
            <div class="si-model-row">
                <input id="seat-34" name="seat[]" value="34" type="checkbox">
                <label for="seat-34">34</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-29" name="seat[]" value="29" type="checkbox">
                <label for="seat-29">29</label>
            </div>
            <div class="si-model-row">
                <input id="seat-30" name="seat[]" value="30" type="checkbox">
                <label for="seat-30">30</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-25" name="seat[]" value="25" type="checkbox">
                <label for="seat-25">25</label>
            </div>
            <div class="si-model-row">
                <input id="seat-22" name="seat[]" value="26" type="checkbox">
                <label for="seat-26">26</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-21" name="seat[]" value="21" type="checkbox">
                <label for="seat-21">21</label>
            </div>
            <div class="si-model-row">
                <input id="seat-22" name="seat[]" value="22" type="checkbox">
                <label for="seat-22">22</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-17" name="seat[]" value="17" type="checkbox">
                <label for="seat-17">17</label>
            </div>
            <div class="si-model-row">
                <input id="seat-18" name="seat[]" value="18" type="checkbox">
                <label for="seat-18">18</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-13" name="seat[]" value="13" type="checkbox">
                <label for="seat-13">13</label>
            </div>
            <div class="si-model-row">
                <input id="seat-14" name="seat[]" value="14" type="checkbox">
                <label for="seat-14">14</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-9" name="seat[]" value="9" type="checkbox">
                <label for="seat-9">9</label>
            </div>
            <div class="si-model-row">
                <input id="seat-10" name="seat[]" value="10" type="checkbox">
                <label for="seat-10">10</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-5" name="seat[]" value="5" type="checkbox">
                <label for="seat-5">5</label>
            </div>
            <div class="si-model-row">
                <input id="seat-6" name="seat[]" value="6" type="checkbox">
                <label for="seat-6">6</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-1" name="seat[]" value="1" type="checkbox">
                <label for="seat-1">1</label>
            </div>
            <div class="si-model-row">
                <input id="seat-2" name="seat[]" value="2" type="checkbox">
                <label for="seat-2">2</label>
            </div>
        </div>
    </div>
    <div class="si-model-on">
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-39" name="seat[]" value="39" type="checkbox">
                <label for="seat-39">39</label>
            </div>
        </div>
        <div class="si-model-col">
        </div>
        <div class="si-model-col">
        </div>
        <div class="si-model-col">
        </div>
        <div class="si-model-col">
        </div>
        <div class="si-model-col">
        </div>
        <div class="si-model-col">
        </div>
    </div>
    <div class="si-model-on">
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-40" name="seat[]" value="40" type="checkbox">
                <label for="seat-40">40</label>
            </div>
            <div class="si-model-row">
                <input id="seat-41" name="seat[]" value="41" type="checkbox">
                <label for="seat-41">41</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-35" name="seat[]" value="35" type="checkbox">
                <label for="seat-35">35</label>
            </div>
            <div class="si-model-row">
                <input id="seat-36" name="seat[]" value="36" type="checkbox">
                <label for="seat-36">36</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-31" name="seat[]" value="31" type="checkbox">
                <label for="seat-31">31</label>
            </div>
            <div class="si-model-row">
                <input id="seat-32" name="seat[]" value="32" type="checkbox">
                <label for="seat-32">32</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-27" name="seat[]" value="27" type="checkbox">
                <label for="seat-27">27</label>
            </div>
            <div class="si-model-row">
                <input id="seat-28" name="seat[]" value="28" type="checkbox">
                <label for="seat-28">28</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-23" name="seat[]" value="23" type="checkbox">
                <label for="seat-23">23</label>
            </div>
            <div class="si-model-row">
                <input id="seat-24" name="seat[]" value="24" type="checkbox">
                <label for="seat-24">24</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-19" name="seat[]" value="19" type="checkbox">
                <label for="seat-19">19</label>
            </div>
            <div class="si-model-row">
                <input id="seat-20" name="seat[]" value="20" type="checkbox">
                <label for="seat-20">20</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-15" name="seat[]" value="15" type="checkbox">
                <label for="seat-15">15</label>
            </div>
            <div class="si-model-row">
                <input id="seat-16" name="seat[]" value="16" type="checkbox">
                <label for="seat-16">16</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-11" name="seat[]" value="11" type="checkbox">
                <label for="seat-11">11</label>
            </div>
            <div class="si-model-row">
                <input id="seat-12" name="seat[]" value="12" type="checkbox">
                <label for="seat-12">12</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-7" name="seat[]" value="7" type="checkbox">
                <label for="seat-7">7</label>
            </div>
            <div class="si-model-row">
                <input id="seat-8" name="seat[]" value="8" type="checkbox">
                <label for="seat-8">8</label>
            </div>
        </div>
        <div class="si-model-col">
            <div class="si-model-row">
                <input id="seat-3" name="seat[]" value="3" type="checkbox">
                <label for="seat-3">3</label>
            </div>
            <div class="si-model-row">
                <input id="seat-4" name="seat[]" value="4" type="checkbox">
                <label for="seat-4">4</label>
            </div>
        </div>
    </div>
</form>

<!-- RESERVED SEATS -->
<script>
<?php if(isset($reserved_seats) && count($reserved_seats)>0){ foreach ($reserved_seats as $seat) { ?>
$('#bus-template input[value="<?= $seat->Seat ?>"]').attr('disabled', 'disabled').next('label').addClass('si-disable');
<?php } } ?>
</script>