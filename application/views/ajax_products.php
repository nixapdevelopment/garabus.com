<div class="row">
<?php foreach ($products as $product) { ?>
    <div class="product-item col-md-4">
        <div class="product-item-inn">
            <div class="product-prop">
                <?php if ($product->IsPromo == 1) { ?>
                <div class="product-promo"><i class="fa fa-percent"></i> Promo</div>
                <?php } ?>
            </div>
            <a href="<?= site_url($product->Link) ?>">
                <img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="" />
                <strong><?= $product->Name ?></strong>
            </a>
            <div class="product-bottom">
                <div class="product-price"><?= number_format($product->ProductPrice, 2) ?> lei</div>
                <div class="product-panel">
                    <button add-to-cart="<?= $product->ProductID ?>"><?= lang('buy') ?></button>                            
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</div>
<div class="clearfix"></div>
<?php $count_pages = ceil($count / $per_page); ?>
<?php if ($count_pages > 1) { ?>
<div class="pagination">
    <ul id="products-page" class="category-sort">
        <?php for ($i = 1; $i <= $count_pages; $i++) { ?>
        <li><a class="<?= $page == $i ? 'active' : '' ?>" data="<?= $i ?>"><?= $i ?></a></li>
        <?php } ?>
    </ul>
</div>
<?php } ?>