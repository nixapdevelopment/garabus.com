<div class="container">


    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-6">


            <div class="panel panel-success">
                <div class="panel-heading"><?= lang('LastTranzactions') ?> <a href="<?= site_url('transporter/orders') ?>" class="pull-right white-href"><i class="fa fa-shopping-cart"></i> <?= lang('SeeAllOrders') ?></a></div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Date') ?></th>
                            <th><?= lang('Amount') ?></th>
                            <!--th><?= lang('PaymentType') ?></th>
                            <th><?= lang('ClientType') ?></th>
                            <th><?= lang('Status') ?></th-->
                            <th class="text-center"></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $order) { ?>
                        <tr data-oid="<?= $order->ID ?>">
                            <td class="text-center"><a href="<?= site_url('transporter/order/' . $order->ID) ?>"><?= $order->ID ?></a></td>
                            <td><?= $order->Name ?></td>
                            <td><?= date('d.m.Y H:i', strtotime($order->Date)) ?></td>
                            <td><?= $order->Amount ?></td>
                            <!--td><?= $order->PaymentType ?></td>
                            <td><?= $order->Type ?></td>
                            <td><span data-html="true" data-toggle="popover" title="<b>Statut comenzii</b>" data-content="<div><a class='text-danger' oid='<?= $order->ID ?>'>Canceled</a></div><div><a class='text-warning' oid='<?= $order->ID ?>'>Pending</a></div><div><a class='text-success' oid='<?= $order->ID ?>'>Paid</a></div><div><a class='text-primary' oid='<?= $order->ID ?>'>Closed</a></div>"><?= $order->Status ?></span></td-->
                            <td class="text-center">
                                <a href="<?= site_url('transporter/order/' . $order->ID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                <?php if ($order->IsNew) { ?>
                                    <span class="label label-primary">NEW</span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading"><?= lang('AccountBalance') ?> <a  class="pull-right white-href"><span class="dash-balance">0 <small>LEI</small></span></a></div>
                <table class="table table-bordered">
                    <tr>
                        <td><?= lang('Name') ?></td>
                        <td>Transporter SRL</td>
                    </tr>
                    <tr>
                        <td><?= lang('TotalOrders') ?></td>
                        <td><a href="#">0</a></td>
                    </tr>
                    <tr>
                        <td><?= lang('NewOrders') ?></td>
                        <td><a href="#">0</a></td>
                    </tr>
                    <tr>
                        <td><?= lang('Finance') ?></td>
                        <td><a href="#"><?= lang('RequestForWithdrawal') ?></a></td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>