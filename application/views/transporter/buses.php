<div class="container">
    <div style="margin-bottom: 10px;">
        <a data-bus-id="0" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> <?=lang('AddNew')?></a>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div id="buses-list">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="template-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?=lang('BusTemplatePreview')?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('Close')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-bus-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=lang('EditBusData')?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button onclick="$('#edit-bus-form').submit();" type="button" class="btn btn-success"><?=lang('Save')?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('Close')?></button>
            </div>
        </div>
    </div>
</div>

<style>
    
    #template-modal form {
        zoom: 0.6;
    }
    
</style>

<script>

    $('body').on('click', 'a[data-template]', function () {
        var template = $(this).attr('data-template');
        $.post('<?= site_url('main/template') ?>', {template: template}, function (html) {
            $('#template-modal .modal-body').html(html);
            $('#template-modal').modal('show');
        });
    });

    $('body').on('click', 'a[data-bus-id]', function () {
        var id = $(this).attr('data-bus-id');
        $.post('<?= site_url('transporter/edit_bus_form') ?>', {id: id, 'userID': <?= $this->_user->ID ?>}, function (html) {
            $('#edit-bus-modal .modal-body').html(html);
            $('#edit-bus-modal').modal('show');
        });
    });

    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    
    function getBuses()
    {
        $.post('<?= site_url('transporter/transporter_buses_list/' . $this->_user->ID) ?>', {}, function(html){
            $('#buses-list').html(html);
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
    
    getBuses();

</script>