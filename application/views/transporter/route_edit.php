<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('RouteEdit') ?></h3>
            </div>
        </div>
    </div>
    <div>
        <div>

            <?= $this->session->flashdata('success') ?>
            
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#route" aria-controls="route" role="tab" data-toggle="tab"><?=lang('Route')?></a></li>
                <?php if (!empty($route->ID)) { ?>
                    <li role="presentation"><a href="#shedule" aria-controls="shedule" role="tab" data-toggle="tab"><?=lang('Schedule')?></a></li>
                    <li role="presentation"><a href="#seats" aria-controls="seats" role="tab" data-toggle="tab"><?=lang('Seats')?></a></li>
                <?php } ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <br />
                <div role="tabpanel" class="tab-pane active" id="route">
                    <form method="post">
                        <div class="location-selector">
                            <div class="row">
                                <div class="col-md-2">
                                    <h2><?= lang('WhereFrom') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Country') ?></label>
                                        <?= CountryModel::dropdown($this->langID, empty($route->CountryFrom) ? '' : $route->CountryFrom, 'class="form-control county-select-from"', 'CountryFrom') ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('City') ?></label>
                                        <?= CityModel::dropdown($this->langID, empty($route->CountryFrom) ? '' : $route->CountryFrom, 'CityFrom', empty($route->CityFrom) ? '' : $route->CityFrom, 'class="form-control select2" style="width: 100%;"') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('Station')?></label>
                                        <input style="width: 100%;" name="StationFrom" value="<?= !empty($stations) ? reset($stations)->Station : '' ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('DayFrom')?></label>
                                        <?= RouteModel::daySelect($this->langID, 'DayFrom', !empty($stations) ? reset($stations)->Day : '', 'class="form-control"') ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('TimeFrom')?></label>
                                        <input style="width: 100%;" name="TimeFrom" value="<?= !empty($stations) ? reset($stations)->Time : '' ?>" readonly class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <h2><?= lang('Whereto') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Country') ?></label>
                                        <?= CountryModel::dropdown($this->langID, empty($route->CountryTo) ? '' : $route->CountryTo, 'class="form-control county-select-to"', 'CountryTo') ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('City') ?></label>
                                        <?= CityModel::dropdown($this->langID, empty($route->CountryTo) ? '' : $route->CountryTo, 'CityTo', empty($route->CityTo) ? '' : $route->CityTo, 'class="form-control select2" style="width: 100%;"') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('Station')?></label>
                                        <input style="width: 100%;" name="StationTo" value="<?= !empty($stations) ? end($stations)->Station : '' ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('DayTo')?></label>
                                        <?= RouteModel::daySelect($this->langID, 'DayTo', empty($stations) ? '' : end($stations)->Day, 'class="form-control"') ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('TimeTo')?></label>
                                        <input style="width: 100%;" name="TimeTo" value="<?= empty($stations) ? '' : end($stations)->Time ?>" readonly class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Transport') ?></label>
                                        <?= form_dropdown('TransporterBusID', $buses, empty($route->TransporterBusID) ? '' : $route->TransporterBusID, 'class="form-control" required') ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Price') ?></label>
                                        <input type="number" min="0.01" step="0.01" required value="<?= empty($route->Price) ? '' : $route->Price ?>" name="Price" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <button id="add-middle-point" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> <?= lang('AddStation') ?></button>
                        <div style="margin-top: 10px; font-weight: bold;" class="row">
                            <div class="col-md-1">
                                
                            </div>
                            <div class="col-md-3">
                                <?=lang('Country')?>
                            </div>
                            <div class="col-md-3">
                                <?=lang('City')?>
                            </div>
                            <div class="col-md-2">
                                <?=lang('Station')?>
                            </div>
                            <div class="col-md-1">
                                <?=lang('Day')?>
                            </div>
                            <div class="col-md-1">
                                <?=lang('Time')?>
                            </div>
                        </div>
                        <div style="margin-top: 10px;" id="middle-point-wrap">
                            <?php foreach ($stations as $station) { ?>
                            <?php if ($station->Type != 'Middle') continue; ?>
                            <div class="row">
                                <div class="col-md-1 text-center">
                                    <i class="fa fa-arrows" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= CountryModel::dropdown($this->langID, $station->CountryID, 'class="form-control select2"', 'CountryID[]') ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= CityModel::dropdown($this->langID, $station->CountryID, 'CityID[]', $station->CityID, 'class="form-control select2" required') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input style="width: 100%;" name="Station[]" value="<?= $station->Station ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <?= RouteModel::daySelect($this->langID, 'Day[]', $station->Day, 'class="form-control"') ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input style="width: 100%;" name="Time[]" readonly value="<?= $station->Time ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <hr />
                        <button class="btn btn-success btn-lg"><i class="fa fa-save"></i> <?= lang('Save') ?></button>
                    </form>
                </div>
                <?php if (!empty($route->ID)) { ?>
                    <div role="tabpanel" class="tab-pane" id="shedule">                        
                        
                        <div class="btn-group" data-toggle="buttons" style="margin-top: 10px;">
                            <button id="daily-shedule-btn" class="btn btn-success">
                                <i class="fa fa-clock-o"></i> <?=lang('Daily')?>
                            </button>
                        </div>
                    
                        <div class="btn-group" data-toggle="buttons" style="margin: 10px 0 0 10px;">
                            <button class="btn btn-success week-shedule" value="1"><?=lang('cal_monday')?></button>
                            <button class="btn btn-success week-shedule" value="2"><?=lang('cal_tuesday')?></button>
                            <button class="btn btn-success week-shedule" value="3"><?=lang('cal_wednesday')?></button>
                            <button class="btn btn-success week-shedule" value="4"><?=lang('cal_thursday')?></button>
                            <button class="btn btn-success week-shedule" value="5"><?=lang('cal_friday')?></button>
                            <button class="btn btn-success week-shedule" value="6"><?=lang('cal_saturday')?></button>
                            <button class="btn btn-success week-shedule" value="0"><?=lang('cal_sunday')?></button>
                        </div>
                        <div class="btn-group" data-toggle="buttons" style="margin-top: 10px; margin-left: 10px">
                            <button id="reset-shedule" class="btn btn-danger">
                                <i class="fa fa-trash"></i> <?=lang('DeleteAll')?>
                            </button>
                        </div>
                        
                        <div id="calendar" style="min-height: 480px; overflow: hidden;"></div>
                        <button id="save-shedule" style="margin-left: 30px; margin-top: 10px;" class="btn btn-success btn-lg"><i class="fa fa-save"></i> <?= lang('Save') ?> <?=lang('Schedule')?></button>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="seats">
                        <?php $this->load->view('buses/delfin') ?>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<script id="middle-point-tmpl" type="text/x-jquery-tmpl">
<div class="row">
    <div class="col-md-1 text-center">
        <i class="fa fa-arrows" aria-hidden="true"></i>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= CountryModel::dropdown($this->langID, '', 'class="form-control select2"', 'CountryID[]') ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <select name="CityID[]" class="form-control select2"></select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <input style="width: 100%;" name="Station[]" class="form-control" />
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <?= RouteModel::daySelect($this->langID, 'Day[]', '', 'class="form-control"') ?>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <input style="width: 100%;" name="Time[]" readonly class="form-control" />
        </div>
    </div>
    <div class="col-md-1">
        <button type="button" onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </div>
</div>
</script>

<script>
var currentYear = new Date().getFullYear();
var currentMonth = new Date().getMonth();
var currentDay = new Date().getDate();
            
    $(document).ready(function(){
        $('body').on('click', '#add-middle-point', function(){
            $('#middle-point-tmpl').tmpl().appendTo('#middle-point-wrap');
            var row = $('#middle-point-wrap .row:last');
            applySelect2(row.find('.select2'));
            applyLocationSelect(row.find('select[name="CountryID[]"]'), row.find('select[name="CityID[]"]'));
            applyTimeSelect(row.find('input[name="Time[]"]'));
        });

        applySelect2($('.select2'));
        applySelect2($('.county-select-from, .county-select-to'));
        applySelect2($('select[name=UserID]'));
        
        applyLocationSelect($('.county-select-from'), $('select[name="CityFrom"]'));
        applyLocationSelect($('.county-select-to'), $('select[name="CityTo"]'));
        
        applyTimeSelect('input[name="Time[]"]');
        applyTimeSelect('input[name="TimeTo"], input[name="TimeFrom"]');

        function saveEvent(date)
        {
            var event = {
                color: '#2C8FC9',
                startDate: date,
                endDate: date,
                date: moment(date).format('YYYY-MM-DD')
            };
            
            var newDate = moment(date).format('YYYY-MM-DD');

            var dataSource = $('#calendar').data('calendar').getDataSource();
            
            var exist = false;
            var index = 0;
            $.each(dataSource, function(ind, data){
                try {
                    var oldDate = moment(data.startDate).format('YYYY-MM-DD');
                    if (newDate === oldDate)
                    {
                        exist = true;
                        index = ind;
                    }
                } catch (e) {

                }
            });
            
            if (exist)
            {
                delete dataSource[index];
            }
            else
            {
                dataSource.push(event);
            }

            $('#calendar').data('calendar').setDataSource(dataSource);
        }
                        
        var calendar = $('#calendar').calendar({
            language: 'ru',
            allowOverlap: true,
            clickDay: function(e) {
                saveEvent(e.date);
                return false;
            },
            minDate: new Date(currentYear, currentMonth, currentDay),
            style: 'background',
            enableRangeSelection: false,            
            dataSource: [
                <?php foreach ($trips as $trip) { ?>
                {
                    color: '#2C8FC9',
                    startDate: new Date(<?= $trip->y ?>, <?= $trip->m ?>, <?= $trip->d ?>),
                    endDate: new Date(<?= $trip->y ?>, <?= $trip->m ?>, <?= $trip->d ?>),
                    date: moment(new Date(<?= $trip->y ?>, <?= $trip->m ?>, <?= $trip->d ?>)).format('YYYY-MM-DD')
                },
                <?php } ?>
            ]
        });
        
        $("#daily-shedule-btn").on("click", function(){ 
            $('#calendar').data('calendar').setDataSource([
                
                <?php 
                $begin = new DateTime( date('Y-m-d') );
                $end = new DateTime( date('Y-m-d', strtotime('+1 years')) );

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                foreach ( $period as $dt ){ ?>
                    {
                        color: '#2C8FC9',
                        startDate: new Date(<?=$dt->format( "Y" )?>, <?=$dt->format( "n" )?>-1, <?=$dt->format( "j" )?>),
                        endDate: new Date(<?=$dt->format( "Y" )?>, <?=$dt->format( "n" )?>-1, <?=$dt->format( "j" )?>),
                        date: moment(new Date(<?=$dt->format( "Y" )?>, <?=$dt->format( "n" )?>-1, <?=$dt->format( "j" )?>)).format('YYYY-MM-DD')                                           
                    },
                <?php } ?>
                
                
            ]); 
        });
        
        $(".week-shedule").on("click", function(){
            var currentCalendarDays = $('#calendar').data('calendar').getDataSource();
            var dayOfweek = $(this).val();

            <?php  
                $weekNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                $endDate = strtotime(date('Y-m-d', strtotime('+1 years')));

                foreach($weekNames as $weekID => $weekName){ ?>

            if(dayOfweek == <?=$weekID?>){
                $('#calendar').data('calendar').setDataSource(currentCalendarDays.concat([ 

                <?php
                        for($i = strtotime($weekName, strtotime(date('Y-m-d'))); $i <= $endDate; $i = strtotime('+1 week', $i)) { ?>
                    {
                        color: '#2C8FC9',
                        startDate: new Date(<?=date('Y', $i);?>, <?=date('n', $i);?>-1, <?=date('j', $i);?>),
                        endDate: new Date(<?=date('Y', $i);?>, <?=date('n', $i);?>-1, <?=date('j', $i);?>),
                        date: moment(new Date(<?=date('Y', $i);?>, <?=date('n', $i);?>-1, <?=date('j', $i);?>)).format('YYYY-MM-DD')
                    },
                <?php } ?>
                ]));                    
            }
            <?php } ?>
            
        });
        
        $("#reset-shedule").on('click', function(){
            $('#calendar').data('calendar').setDataSource(); 
        });
        
        
        $('#save-shedule').click(function(){
            $.post('<?= site_url('transporter/save_shedule/' . (empty($route->ID) ? 0 : $route->ID)) ?>', {data: $('#calendar').data('calendar').getDataSource()}, function(){
                notif({
                    msg: "<?=lang('ScheduleSaved')?>",
                    type: "success",
                    position: "right"
                });
            });
        });
        
        $('#middle-point-wrap').sortable();        
                          
        $("#seats input").each(function(){
            $(this).attr('disabled', 'disabled');             
        });
        
        
        get_special_seat_prices();    
                
        $('body').on('click', '.update-price', function(){
            
            var up_seat_nr = $(this).parent('.input-group-btn').parent('.input-group').find('.custom-seat').attr('data-seat');
            var up_seat_price = $(this).parent('.input-group-btn').parent('.input-group').find('.custom-seat').val();
            
            $.post('<?= site_url('transporter/update_seat_price') ?>',{id: <?= empty($route->ID) ? 0 : $route->ID ?>, seat: up_seat_nr, price: up_seat_price}, function(success){
                
                if(success==1){
                    notif({
                        msg: "<?=lang('SeatPriceSaved')?>",
                        type: "success",
                        position: "right"
                    });
                             
                    $("label[for=seat-"+up_seat_nr+"]").popover('destroy');
                    setTimeout(function () {
                        $("label[for=seat-"+up_seat_nr+"]").popover({
                             container: 'body',
                             html: true,
                             content: '<div class="input-group"><input value="'+up_seat_price+'" type="number" class="form-control custom-seat" data-seat="'+up_seat_nr+'"><span class="input-group-btn"><button class="btn btn-success update-price" title="Save" type="button"><i class="glyphicon glyphicon-retweet"></i></button><button class="btn btn-danger delete-price" title="Delete" type="button"><i class="glyphicon glyphicon-trash"></i></button></span></div>',
                             placement: "left"            
                         });
                     }, 200);
                    //get_special_seat_prices(); 
                    
                }
            });
                
        });
        
        $('body').on('click', '.delete-price', function(){
            
            if(confirm('<?=lang('DeleteSmth')?>?')){
                var up_seat_nr = $(this).parent('.input-group-btn').parent('.input-group').find('.custom-seat').attr('data-seat');
                $(this).parent('.input-group-btn').parent('.input-group').find('input').val(price);
                
                $.post('<?= site_url('transporter/delete_seat_price') ?>',{id: <?= empty($route->ID) ? 0 : $route->ID ?>, seat: up_seat_nr}, function(success){
                    if(success==1){
                        notif({
                            msg: "<?=lang('DeleteSeatPrice')?>",
                            type: "success",
                            position: "right"
                        });

                        get_special_seat_prices();
                        
                    }
                });
            }
            return false;
                
        });
                
    });
   
    
var price = $("input[name='Price']").val();

function get_special_seat_prices(){
    
    var seat_price = [];
    
    $.post('<?= site_url('transporter/get_custom_seat_price') ?>', {id: <?= empty($route->ID) ? 0 : $route->ID ?>}, function(data){            
        for(var i in data){                
            seat_price[data[i]['Seat']] = data[i]['Price'];
        }

        var data_seat;
        $("#seats label").each(function(i,c){
                        
            if(!$(this).parent('.si-model-row').hasClass('si-disable')) {
               data_seat = $(this).text();
                var _price = price;
    console.log('p='+_price+' s='+data_seat);
                $(this).removeClass("seatselected");
                if(typeof(seat_price[data_seat]) != "undefined" && seat_price[data_seat] !== null) {
                    _price = seat_price[data_seat];
    console.log('ps='+_price+' ss='+data_seat);
                    $(this).addClass('seatselected');                    
                }  
    
                $(this).popover({
                    container: 'body',
                    html: true,
                    content: '<div class="input-group"><input value="'+_price+'" type="number" class="form-control custom-seat" data-seat="'+data_seat+'"><span class="input-group-btn"><button class="btn btn-success update-price" title="Save" type="button"><i class="glyphicon glyphicon-retweet"></i></button><button class="btn btn-danger delete-price" title="Delete" type="button"><i class="glyphicon glyphicon-trash"></i></button></span></div>',
                    placement: "left"            
                });
                
            }            
            
        });
    },"json");
}

</script>

<style>
    .in-admin .select2-container--bootstrap .select2-selection--single,
    .in-admin .select2-container--bootstrap.select2-container--focus .select2-selection, 
    .in-admin .select2-container--bootstrap.select2-container--open .select2-selection
{
    padding: 10px 15px !important;
    height: 45px !important;
    border-radius: 0 !important;
    border: 2px solid #dce4ec !important;
    box-shadow: 0 0 0 !important;
}

#middle-point-wrap .text-center i {
    font-size: 33px;
}
</style>