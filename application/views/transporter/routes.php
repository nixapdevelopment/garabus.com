<div class="container">
    <div class="dash-block">
        <h3><?= lang('Orders') ?></h3>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th><?= lang('Route') ?></th>
                    <th style="max-width: 150px; text-align: center;"><a href="<?= site_url('transporter/route_edit') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= lang('Add') ?></a></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($routes as $route) { ?>
                    <tr>
                        <td><?= $route->ID ?></td>
                        <td><?= $route->CityFromName ?> <i class="fa fa-long-arrow-right"></i> <?= $route->CityToName ?></td>
                        <td class="text-center">
                            <a href="<?= site_url('transporter/route_edit/' . $route->ID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>