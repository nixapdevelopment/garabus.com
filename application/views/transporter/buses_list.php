<table class="table table-hover table-bordered table-striped">
    <thead>
        <tr>
            <th><?=lang('BusName')?></th>
            <th><?=lang('BusTemplate')?></th>
            <th></th>
        </tr>
    </thead>
    <thead>
        <?php foreach ($buses as $bus) { ?>
            <tr>
                <td><?= $bus->Name ?></td>
                <td class="text-center">
                    <a href="#" data-template="<?= $bus->Template ?>">
                        <?= $bus->Template ?>
                    </a>
                </td>
                <td class="text-center">
                    <a data-bus-id="<?= $bus->ID ?>" data-toggle="tooltip" title="Редактировать" class="btn btn-xs btn-success">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </thead>
</table>