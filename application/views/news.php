<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- News -->
    <?php if (count($last_news) > 0) { ?>
<div class="home-news" style="background-color: inherit;">
    <div class="container">            
        <h3><i class="fa fa-newspaper-o"></i> <?= lang('NewsUpperCase')?></h3>
        <div class="row">
            <?php $i = 0; ?>
            <?php foreach ($last_news as $news) { ?>
            <?php $i++; ?>
            <div class="news-item col-md-3">
                <div class="news-item-inn">
                    <a href="<?= site_url($news->Link) ?>">
                        <img src="<?= base_url('public/uploads/news/' . $news->Thumb) ?>" alt="<?= $news->Title ?>">
                        <h4><?= $news->Title ?></h4>
                        <time><i class="fa fa-clock-o"></i> <?= date('d.m.Y', strtotime($news->Date)) ?></time>
                        <div class="news-item-short">
                            <?= mb_substr(strip_tags($news->Text), 0, 162) ?>...
                        </div>
                    </a>
                </div>
            </div>
            <?php if ($i % 4 == 0) { ?>
            <div class="clearfix"></div>
            <?php $i = 0; ?>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div><?= $pagination ?></div>
</div>
<div class="clearfix"></div>
<?php } ?>