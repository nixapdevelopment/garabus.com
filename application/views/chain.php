<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?= $page->Title ?></h1>
        <br />
        <div class="row">
            <div class="col-md-6">
                <?= $page->Text ?>
            </div>
            <div class="col-md-6">
                Map
            </div>
        </div>
    </div>
</section>