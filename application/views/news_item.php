<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?= $NewsArticle->Title ?></h1>
        <br />
        <div>
            <?= $NewsArticle->Text ?>            
        </div>

        <div class="row">
            <div class="col-md-<?= count($NewsFiles) ? 9 : 12 ?>">
                <div class="row">
                    <?php foreach ($NewsImages as $NewsImage) { ?>
                        <div class="col-md-4">
                            <a class="fancybox" rel="news-image" href="<?= base_url('public/uploads/news/' . $NewsImage->Image) ?>">
                                <img class="img-thumbnail" src="<?= base_url('public/uploads/news/' . $NewsImage->Thumb) ?>" />
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if (!empty($NewsFiles)) { ?>
                <div class="col-md-3">
                    <div class="list-group">
                        <?php foreach ($NewsFiles as $NewsFile) { ?>
                        <a target="_blank" href="<?= base_url('public/uploads/news/' . $NewsFile->Path) ?>" class="list-group-item"><?= $NewsFile->Name ?></a>
                        <?php } ?>
                    </div>
                </div>     
            <?php } ?>
        </div>
    </div>
</section>