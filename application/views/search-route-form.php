<?php

$CI = &get_instance();
$CI->load->model('CityModel');

if (isset($_GET['from']))
{
    $city_from = $CI->CityModel->getByID($_GET['from'], $this->langID);
}

if (isset($_GET['to']))
{
    $city_to = $CI->CityModel->getByID($_GET['to'], $this->langID);
}

?>
<form id="search-route-form" action="<?= site_url('search_redirect') ?>">
    <div class="search-form">
        <div class="search-col search-col-big">
            <label><?= lang('WhereFrom') ?>:</label>
            <select name="from" class="ico-destination" type="text" required placeholder="<?=lang('SelectCity')?>">
                <?php if (isset($city_from->ID)) { ?>
                <option value="<?= $city_from->ID ?>"><?= $city_from->Name ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="search-col search-col-min hidden-xs">
            <div class="search-re"><i class="fa fa-retweet"></i></div>
        </div>
        <div class="search-col search-col-big">
            <label><?= lang('Whereto') ?>:</label>
            <select name="to" class="ico-destination" type="text" required placeholder="selectează orașul">
                <?php if (isset($city_to->ID)) { ?>
                <option value="<?= $city_to->ID ?>"><?= $city_to->Name ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="search-col search-col-big">
            <label><?= lang('Departure') ?>:</label>
            <input name="date" class="ico-calendar datepicker" type="text" value="<?= isset($_GET['date']) ? $_GET['date'] : date('d.m.Y') ?>" placeholder="introdu data">
        </div>
        
        <div class="search-col search-col-med">                    
            <button><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>

<script>

    $(document).ready(function(){
        applyFullCitySelect($('#search-route-form select[name="from"]'));
        applyFullCitySelect($('#search-route-form select[name="to"]'));
        $('#search-route-form').validate({
            errorPlacement: function(error, element) {
                $(element).next('.select2-container').find('.select2-selection').css({"border":"1px solid #f35858"});
            },
            unhighlight: function (el, error, valid, _orig) {
                _orig(el, error, valid);
            }
        });
    });
    
    <?php if (isset($_GET['persons'])) { ?>
    $('#search-route-form select[name="persons"]').val(<?= $_GET['persons'] ?>);
    <?php } ?>

</script>