<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?= $page->Title ?></h1>
        <br />
        <div class="row">
            <div class="col-md-6">
                <?= $page->Text ?>
                <br />
                <form id="feedback-form">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Name')?></label>
                        <input type="text" class="form-control" name="Name" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Email')?></label>
                        <input type="email" class="form-control" name="Email" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Phone')?></label>
                        <input type="text" class="form-control" name="Phone" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Message')?></label>
                        <textarea class="form-control" name="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><?= lang('Send')?></button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <iframe width="100%" height="550" frameborder="0" src="https://point.md/ru/map/47.02485494782477/28.833403587341305/15/5874e9f6bbdb22000a070a07?embed=1"></iframe>
            </div>
        </div>
    </div>
</section>

<script>

    $('#feedback-form').submit(function(e){
        e.preventDefault();
        $.post('<?= site_url('main/feedback') ?>', $('#feedback-form').serialize(), function(){
            $('#feedback-form').trigger('reset');
            notif({
                msg: "<?= lang('FeedbackSended')?>",
                type: "success",
                position: "right"
            });
        });
    });

</script>