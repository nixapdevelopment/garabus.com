<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="search">
    <div class="container">
        <div class="search-title"><?= lang('SearchFormText') ?></div>
        <?php $this->load->view('search-route-form') ?>
    </div>
</section>
<section class="home-content">
    <div class="container hidden-xs">
        <div class="home-calitati">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="calitate">
                        <strong><?= lang('VerifiedTransporters') ?></strong>
                        <p>Curabitur sit amet pulvinar tellus.  </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="calitate">
                        <strong><?= lang('BestPrices') ?></strong>
                        <p>Aroin sed interdum neque donec.  </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="calitate">
                        <strong><?= lang('BusConfortable') ?></strong>
                        <p>Nam pharetra hendrerit neque. </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="calitate">
                        <strong><?= lang('Support24') ?></strong>
                        <p>Vestibulum vehicula sapien dolor.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-rute">
        <div class="container">
            <div class="row">
                
                <?php if(count($top_routes) > 0) { 
                    foreach($top_routes as $tp){ ?>
                <div class="col-md-4 col-sm-6">
                    <a href="<?= site_url('search/'. str_to_url($tp->CityFrom) . '-' . str_to_url($tp->CityTo), ['from'=>$tp->CityFromID, 'to'=>$tp->CityToID]);?>" class="home-rute-item">
                        <strong><?=$tp->CityFrom; ?> <i class="fa fa-long-arrow-right"></i> <?=$tp->CityTo; ?></strong>
                        <span>de la <?=$tp->MinPrice; ?> mdl</span>
                    </a>
                </div>
                <?php }                 
                    } ?>                
            </div>
        </div>
    </div> 
</section>

<section class="home-news">
    <div class="container">
        <h2 class="title"><?= lang('LastNews') ?></h2>
        <div class="row">
            <?php foreach ($last_news as $news) { ?>
                <div class="col-md-4 col-sm-6">
                    <a href="<?= site_url($news->Link) ?>" class="news-item">
                        <time><?= date('d.m.Y', strtotime($news->Date)) ?></time>
                        <strong><?= $news->Title ?></strong>
                        <p class="hidden-sm hidden-xs"><?= mb_substr(strip_tags($news->Text), 0, 80) ?> </p>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<section class="home-text">
    <div class="container">
        <h1 class="title"><?= $main_page->Title ?></h1>
        <div>
            <?= $main_page->Text ?>
        </div>
    </div> 
</section>


<section class="bottom-box">
    <?= lang('HomeThanks') ?>
</section>
