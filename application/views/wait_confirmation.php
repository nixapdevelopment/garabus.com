<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="home-first">
            <div class="text-center">
                <h1><?= lang('WaitConfirmation') ?></h1>
                <div class="text-center">
                    <?= lang('WaitConfirmationText') ?>
                </div>
            </div>
        </div>
    </div>
</section>