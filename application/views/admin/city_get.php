<?php
$active_langs = $this->config->item('languages');
?>
<form id="city-edit-form">
    <div class="row">
        <?php foreach ($active_langs as $langID => $lang) { ?>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label"><?= lang('Title') ?> (<?= $lang['Slug'] ?>) <i class="text-danger">*</i></label>
                <input required class="form-control" type="text" name="Name[<?= $langID ?>]" value="<?= isset($city->langs[$langID]->Name) ? $city->langs[$langID]->Name : '' ?>" />
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="form-group">
        <label class="control-label">Country <i class="text-danger">*</i></label>
        <?= form_dropdown('CountryID', $countries, isset($city->city->CountryID) ? $city->city->CountryID : '', 'style="width:100%;" required class="form-control"') ?>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Long <i class="text-danger">*</i></label>
                <input readonly required class="form-control" type="text" name="Long" value="<?= isset($city->city->Long) ? $city->city->Long : '' ?>" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Lat <i class="text-danger">*</i></label>
                <input readonly required class="form-control" type="text" name="Lat" value="<?= isset($city->city->Lat) ? $city->city->Lat : '' ?>" />
            </div>
        </div>
    </div>
    <div id="city-map" style="height: 350px;">
        
    </div>
    <input type="hidden" name="ID" value="<?= isset($city->city->ID) ? $city->city->ID : 0 ?>" />
</form>

<script>

    $('#city-map').locationpicker({
        location: {
            latitude: $('#city-edit-modal input[name=Lat]').val(),
            longitude: $('#city-edit-modal input[name=Long]').val()
        },
        radius: 0,
        inputBinding: {
            latitudeInput: $('#city-edit-modal input[name=Lat]'),
            longitudeInput: $('#city-edit-modal input[name=Long]')
        },
        enableAutocomplete: true,
        zoom: 7
    });
    
    $('#city-edit-form').validate({
        submitHandler: function (form, event) {
            event.preventDefault();
            var data = $(form).serialize();
            $.post('<?=site_url('/admin/city_edit')?>', data, function(){
                window.location.reload();
            });
        }
    });
    
    applySelect2($('select[name="CountryID"]'));

</script>