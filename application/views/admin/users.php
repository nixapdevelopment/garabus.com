<div class="container">
    <div class="dash-block">
        <h3><?= lang('Users') ?></h3>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Email') ?></th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Country') ?></th>
                            <th><?= lang('City') ?></th>
                            <th><?= lang('Status') ?></th>
                            <th><?= lang('RegDate') ?></th>
                            <th class="text-center"></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <form>
                            <tr>
                                <td></td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('Email') ?>" type="text" name="Email" class="form-control" />
                                    </div>
                                </td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('Name') ?>" type="text" name="Name" class="form-control" />
                                    </div>
                                </td>
                                <td>
                                    <?= form_dropdown('Country', $countries, $this->input->get('Country'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <?= form_dropdown('City', $cities, $this->input->get('City'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <?= form_dropdown('Status', ['' => lang('All'), 'Active' => lang('Active'), 'NotConfirmed' => lang('NotConfirmed')], $this->input->get('Status'), 'class="form-control select-2"') ?>
                                </td>
                                <td></td>
                                <td style="min-width: 300px; text-align: center;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-filter"></i> <?= lang('Filter') ?></button>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="<?= site_url('admin/users') ?>" type="submit" class="btn btn-xs btn-danger"><?= lang('ClearFilters') ?></a>
                                        </div>
                                    </div>  
                                    <?= ($this->input->get('Type')=='Manager') ? '<input type="hidden" name="Type" value="Manager" />' : '' ?>
                                </td>
                            </tr>
                        </form>
                        <?php foreach ($users as $user) { ?>
                        <tr>
                            <td class="text-center"><a href="<?= site_url('admin/user/' . $user->ID) ?>"><?= $user->ID ?></a></td>
                            <td><?= $user->UserName ?></td>
                            <td><?= $user->Name ?></td>
                            <td><?= $countries[$user->CountryID] ?></td>
                            <td><?= $cities[$user->CityID] ?></td>                            
                            <td><?= $user->Status ?></td>
                            <td><?= date('d.m.Y', strtotime($user->RegDate)) ?></td>
                            <td class="text-center">
                                <a href="<?= site_url('admin/user/' . $user->ID, $_GET) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?= $pagination ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(function() {
    appplySelect2ToCC();// country and city selects
    applyLocationSelect('select[name=Country]', 'select[name=City]'); 
});

function appplySelect2ToCC(){
    applySelect2('select[name=Country]');
    applySelect2('select[name=City]');
}

</script>