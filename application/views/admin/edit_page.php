<?php
$active_langs = $this->config->item('languages');
?>
<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-5">
                <h3 class="upper-case-title"><?= lang('Pages') ?></h3>
            </div>
            <div class="col-md-7 text-right">
                <?php if ($this->input->get('history') > 0) { ?>
                <b><?=lang('PreviewVersion')?> № <big><code><?= $this->input->get('history') ?></code></big> <?= lang('From')?> <big><code><?= date('d.m.Y H:i:s', strtotime($article->Date)) ?></code></big></b>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?= site_url('admin/edit_page', ['id' => $this->input->get('id')]) ?>" class="btn btn-sm btn-info"><i class="fa fa-upload" aria-hidden="true"></i> <?=lang('ToPublicVersion')?></a>&nbsp;&nbsp;&nbsp;
                <?php } ?>
                <a id="add-filter-btn" href="<?= site_url('admin/edit_page') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddPage') ?></a>
            </div>
        </div>
    </div>
    <div>
        <form id="edit-page-form" method="post" enctype="multipart/form-data">
            <?= $this->session->flashdata('success') ?>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?= lang('GeneralData') ?></a></li>
                    <li role="presentation"><a href="#tab-images" aria-controls="tab-images" role="tab" data-toggle="tab"><?= lang('Images') ?></a></li>
                    <?php if (!empty($article->ID) && $article->ID == 1) { ?>
                    <li role="presentation"><a href="#tab-additional" aria-controls="tab-additional" role="tab" data-toggle="tab"><?= lang('ContentAdditional')?></a></li>
                    <?php } ?>
                    <?php if (!empty($article->ID)) { ?>
                    <li role="presentation"><a href="#tab-history" aria-controls="tab-history" role="tab" data-toggle="tab"><?=lang('ArticlePageVersions')?></a></li>
                    <?php } ?>
                </ul>
                
                <div class="tab-content">
                    <br />
                    <div role="tabpanel" class="tab-pane active" id="tab-main">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Link') ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><?= site_url() ?></span>
                                        <?php if (!empty($article->ID) && $article->ID == 1) { ?>
                                        <input name="Link" value="" type="text" class="form-control" readonly />
                                        <?php } else { ?>
                                        <input name="Link" value="<?= isset($article_link) ? $article_link : '' ?>" type="text" class="form-control" />
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Status') ?></label>
                                    <select <?= !empty($article->ID) && $article->ID == 1 ? 'disabled' : '' ?> class="form-control" name="Status">
                                        <option <?= @$article->Status == 'Active' ? 'selected' : '' ?> value="Active"><?= lang('ProdStatusActive') ?></option>
                                        <option <?= @$article->Status == 'Disabled' ? 'selected' : '' ?> value="Disabled"><?= lang('ProdStatusDisabled') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#tab-<?= $lang['LangFile'] ?>" aria-controls="tab-<?= $lang['LangFile'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
                                <?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br />
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="tab-<?= $lang['LangFile'] ?>">
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> <i class="text-danger">*</i></label>
                                            <input required class="form-control" type="text" name="Title[<?= $langID ?>]" value="<?= isset($article_langs[$langID]->Title) ? $article_langs[$langID]->Title : '' ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Text') ?></label>
                                            <textarea class="form-control ckeditor" name="Text[<?= $langID ?>]"><?= isset($article_langs[$langID]->Text) ? $article_langs[$langID]->Text : '' ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> SEO</label>
                                            <input class="form-control" type="text" name="TitleSeo[<?= $langID ?>]" value="<?= isset($article_langs[$langID]->TitleSeo) ? $article_langs[$langID]->TitleSeo : '' ?>" />
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Keywords') ?></label>
                                                    <textarea class="form-control" name="Keywords[<?= $langID ?>]"><?= isset($article_langs[$langID]->Keywords) ? $article_langs[$langID]->Keywords : '' ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Description') ?></label>
                                                    <textarea class="form-control" name="Description[<?= $langID ?>]"><?= isset($article_langs[$langID]->Description) ? $article_langs[$langID]->Description : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-images">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Images') ?></label>
                                    <input class="form-control" accept=".png,.jpg" type="file" name="Images[]" multiple />
                                </div>
                                <table id="product-images" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th><?= lang('ImagePhoto')?></th>
                                            <th><?= lang('GeneralPhoto')?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($images) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($images as $img) { ?>
                                        <?php $i++; ?>
                                        <tr data-image-id="<?= $img['ID'] ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a rel="photos" href="<?= base_url('public/uploads/pages/' . $img['Image']) ?>" class="fancybox">
                                                    <img style="height: 90px;" src="<?= base_url('public/uploads/pages/' . $img['Thumb']) ?>" class="img-thumbnail" />
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="PrimaryImage" <?= $img['IsMain'] == 1 ? 'checked' : '' ?> value="<?= $img['ID'] ?>">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" onclick="return deleteImage(<?= $img['ID'] ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> <?= lang('DeleteSmth')?></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="4"class="text-center">
                                                <?= lang('NoImageUploaded')?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Files') ?></label>
                                    <input class="form-control" accept=".doc,.docx,.rar,.zip,.pdf,.xls,.xlsx" type="file" name="Files[]" multiple />
                                </div>
                                <table id="product-files" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th><?= lang('FileName') ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($files) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($files as $file) { ?>
                                        <?php $i++; ?>
                                        <tr data-file-id="<?= $file->ID ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?= base_url('public/uploads/pages/' . $file->Path) ?>" target="_blank">
                                                    <?= $file->Name ?>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" onclick="return deleteFile(<?= $file->ID ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> <?= lang('DeleteSmth')?></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="3"class="text-center">
                                                <?= lang('NoFileUploaded')?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($article->ID) && $article->ID == 1) { ?>
                    <div role="tabpanel" class="tab-pane" id="tab-additional">
                        <div>
                            <button id="ac-add-row" type="button" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus-sign"></i> <?= lang('AddContent')?></button>
                        </div>
                        <hr />
                        <table id="ac-table" style="max-width: 100%;" class="table table-striped table-bordered">
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($additional_content as $acID => $ac_item) { ?>
                                <tr data-i="<?= $i ?>">
                                    <td style="width: 5%; text-align: center; vertical-align: middle; font-size: 22px;">
                                        <i style="cursor: move;" class="glyphicon glyphicon-menu-hamburger"></i>
                                    </td>
                                    <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <td style="width: 30%;">
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title')?> (<?= $lang['Slug'] ?>)</label>
                                            <input type="text" name="ACTitle[<?= $i ?>][<?= $langID ?>]" value="<?= @$ac_item[$langID]->Title ?>" class="form-control" required />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Text')?> (<?= $lang['Slug'] ?>)</label>
                                            <textarea class="form-control" name="ACText[<?= $i ?>][<?= $langID ?>]" required><?= @$ac_item[$langID]->Text ?></textarea>
                                        </div>
                                    </td>
                                    <?php } ?>
                                    <td style="width: 5%; text-align: center; vertical-align: middle; font-size: 22px;">
                                        <i style="cursor: pointer;" onclick="$(this).closest('tr').remove();" class="glyphicon glyphicon-trash text-danger"></i>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>
                    <?php if (!empty($article->ID)) { ?>
                    <div role="tabpanel" class="tab-pane" id="tab-history">
                        <?= $history ?>
                    </div>
                    <?php } ?>
                </div>
                
            </div>
            
            <div>
                <button type="submit" name="publish" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> <?=lang('SaveNPublish')?></button>&nbsp;&nbsp;&nbsp;
                <?php if (!empty($article->ID)) { ?>
                <button type="submit" name="save" class="btn btn-success btn-lg"><i class="fa fa-bookmark-o" aria-hidden="true"></i> <?=lang('SaveHistoryVersions')?></button>&nbsp;
                <input placeholder="<?=lang('CommentToVersion')?>" style="display: inline-block; max-width: 500px;" type="text" name="Comment" value="<?= isset($article->Comment) ? $article->Comment : '' ?>" class="form-control" />
                <?php } ?>
            </div>
        </form>
    </div>
</div>

<script id="ac-row-tmpl" type="text/x-jquery-tmpl">
<tr data-i="${key}">
    <td style="width: 5%; text-align: center; vertical-align: middle; font-size: 22px;">
        <i style="cursor: move;" class="glyphicon glyphicon-menu-hamburger"></i>
    </td>
    <?php foreach ($active_langs as $langID => $lang) { ?>
    <td style="width: 30%;">
        <div class="form-group">
            <label class="control-label"><?= lang('Title')?> (<?= $lang['Slug'] ?>)</label>
            <input type="text" name="ACTitle[${key}][<?= $langID ?>]" value="" class="form-control" required />
        </div>
        <div class="form-group">
            <label class="control-label"><?= lang('Text')?> (<?= $lang['Slug'] ?>)</label>
            <textarea class="form-control" name="ACText[${key}][<?= $langID ?>]" required></textarea>
        </div>
    </td>
    <?php } ?>
    <td style="width: 5%; text-align: center; vertical-align: middle; font-size: 22px;">
        <i style="cursor: pointer;" onclick="$(this).closest('tr').remove();" class="glyphicon glyphicon-trash text-danger"></i>
    </td>
</tr>
</script>

<script>

    $('input.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true
    });
    
    $("#edit-page-form").validate({
        errorClass: "text-danger",
        validClass: "text-success",
        errorElement: "span",
        focusCleanup: false,
        focusInvalid: true,
        onsubmit: true,
        ignore: "",
        invalidHandler: function ()
        {
            notif({
                msg: "<?= lang('InvalidForm') ?>",
                type: "error",
                position: "right"
            });
        },
        submitHandler: function()
        {
            var valid = true;
            $('#ac-table input, #ac-table textarea').each(function(){
                if ($.trim($(this).val()) == '')
                {
                    $(this).closest('.form-group').addClass('has-error');
                    valid = false;
                }
            });
            return valid;
        }
    });
    
    $('a.fancybox').fancybox();
    
    function deleteFile(fileID)
    {
        if (confirm('<?=lang('Confirm?')?>'))
        {
            $.post('<?= site_url('admin/delete_file') ?>', {fileID: fileID}, function(){
                $('tr[data-file-id=' + fileID + ']').remove();
            });
        }
        return false;
    }
    
    function deleteImage(imgID)
    {
        if (confirm('<?=lang('Confirm?')?>'))
        {
            $.post('<?= site_url('admin/delete_page_image') ?>', {imgID: imgID}, function(){
                $('tr[data-image-id=' + imgID + ']').remove();
            });
        }
        return false;
    }
    
    $('#ac-add-row').click(function(){
         $('#ac-row-tmpl').tmpl([{key: parseInt($('#ac-table tbody tr:last').attr('data-i')) + 1}]).appendTo('#ac-table tbody');
    });

</script>