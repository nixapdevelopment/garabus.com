<div class="container">

    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('TopRoutes') ?></h3>
            </div>
        </div>
    </div>
    <div class="row">
        <?= $this->session->flashdata('success') ?>
        <div class="col-md-6">
            <form id="toproutes-edit-form" method="post">
                <div class="form-group col-xs-4">
                    <label class="control-label"><?= lang('CityFrom') ?> <i class="text-danger">*</i></label>
                    <?= form_dropdown('CityFromID', $cities, '', 'style="width:100%;" required class="form-control full-city-select"') ?>
                </div>  
                <div class="form-group col-xs-1">
                    <br /><br />
                    <i class="fa fa-long-arrow-right"></i>
                </div>
                <div class="form-group col-xs-4">
                    <label class="control-label"><?= lang('CityTo') ?> <i class="text-danger">*</i></label>
                    <?= form_dropdown('CityToID', $cities, '', 'style="width:100%;" required class="form-control  full-city-select"') ?>                    
                </div> 
                
                <div class="form-group col-xs-3">
                    <br />
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('Add') ?></button>
                </div>
                
           </form>
            
            <hr />
            <div class="clearfix"></div>
            <div>
                <ul id="toproutes">
                <?php foreach ($toproutes as $toproute) { ?>
                    <li class="ui-state-default" data-item="<?= $toproute->ID ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <?= $toproute->CityFrom ?> <i class="fa fa-long-arrow-right"></i> <?= $toproute->CityTo ?>
                        <a class="pull-right" onclick="return confirm('Confirm?')" href="<?= site_url('admin/toproutes', ['delID' => $toproute->ID], true) ?>"><i class="glyphicon glyphicon-trash text-danger"></i></a>                    
                    </li>
                <?php } ?>
                </ul>
            </div>
            
        </div>
        <div class="col-md-6">
            
        </div>
    </div>
</div>

  <style>
  #toproutes { list-style-type: none; margin: 0; padding: 0; cursor: move; }
  #toproutes li { margin: 5px; padding: 7px 10px 5px 5px; font-size: 1.4em;  }
  </style>

<script>
$( function() {
  $( "#toproutes" ).sortable({
      update: function( event, ui ) {
        var data = $("#toproutes").sortable('toArray', {attribute: "data-item"});  
        // return ids in order after update
        //alert(JSON.stringify(data));
        $.post('<?= site_url('admin/sort_toproutes') ?>', {sortedIDs: JSON.stringify(data)}, function(){});
      },
      opacity: 0.7
    });
  $( "#toproutes" ).disableSelection();
  
  applyFullCitySelect('.full-city-select');
} );
</script>