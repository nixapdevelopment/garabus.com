<div class="container">
    <div class="dash-block">
        <h3><?= lang('EditTransporter') ?></h3>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#main-data" aria-controls="main-data" role="tab" data-toggle="tab"><?=lang('GeneralData')?></a></li>
                <?php if (isset($user->ID)) { ?>
                <li role="presentation"><a href="#routes" aria-controls="routes" role="tab" data-toggle="tab"><?=lang('Routes')?></a></li>
                <li onclick="getBuses()" role="presentation"><a href="#buses" aria-controls="buses" role="tab" data-toggle="tab"><?=lang('Buses')?></a></li>
                <?php } ?>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <br />
                <div role="tabpanel" class="tab-pane active" id="main-data">
                    <form id="registration-form" method="post" class="col-md-offset-2 col-md-8" enctype="multipart/form-data">
                        <div class="text-danger">
                            <?= validation_errors() ?>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group company-fields">
                                    <label class="control-label"><?= lang('CompanyName') ?> <span class="text-danger">*</span></label>
                                    <input name="CompanyName" type="text" value="<?= set_value('CompanyName', isset($user->CompanyName) ? $user->CompanyName : '') ?>" class="form-control" />
                                </div>
                                <div class="form-group company-fields">
                                    <label class="control-label"><?= lang('IDNO') ?> <span class="text-danger">*</span></label>
                                    <input name="IDNO" type="text" value="<?= set_value('IDNO', isset($user->IDNO) ? $user->IDNO : '') ?>" class="form-control" required />
                                </div>
                                <div class="form-group company-fields">
                                    <label class="control-label"><?= lang('BankName') ?> <span class="text-danger">*</span></label>
                                    <input name="BankName" type="text" value="<?= set_value('BankName', isset($user->BankName) ? $user->BankName : '') ?>" class="form-control" required />
                                </div>
                                <div class="form-group company-fields">
                                    <label class="control-label"><?= lang('IBAN') ?> <span class="text-danger">*</span></label>
                                    <input name="IBAN" type="text" value="<?= set_value('IBAN', isset($user->IBAN) ? $user->IBAN : '') ?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Country') ?> <span class="text-danger">*</span></label>                                
                                    <?= form_dropdown('Country', $countries, isset($user->CountryID) ? $user->CountryID : false, 'class="form-control select2" required') ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= lang('City') ?> <span class="text-danger">*</span></label>
                                    <?= form_dropdown('City', $cities, isset($user->CityID) ? $user->CityID : false, 'class="form-control select2" required') ?>                                    
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Name') ?> <span class="text-danger">*</span></label>
                                    <input name="Name" type="text" value="<?= set_value('Name', isset($user->Name) ? $user->Name : '') ?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Phone') ?> <span class="text-danger">*</span></label>
                                    <input name="Phone" type="text" value="<?= set_value('Phone', isset($user->Phone) ? $user->Phone : '') ?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Address') ?> <span class="text-danger">*</span></label>
                                    <input type="text" name="Address" class="form-control" value="<?= set_value('Address', isset($user->Address) ? $user->Address : '') ?>" required />
                                </div> 
                                <div class="form-group">
                                    <label class="control-label"><?= lang('UserEmail') ?> <span class="text-danger">*</span></label>
                                    <input name="UserName" type="text" value="<?= set_value('UserName', isset($user->UserName) ? $user->UserName : '') ?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Password') ?> <?= count($user) == 0 ? '<span class="text-danger">*</span>' : '' ?></label>
                                    <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" <?= count($user) == 0 ? 'required' : '' ?> />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= lang('PasswordConfirmation') ?> <?= count($user) == 0 ? '<span class="text-danger">*</span>' : '' ?></label>
                                    <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" <?= count($user) == 0 ? 'required' : '' ?> />
                                </div>  
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Status') ?> <span class="text-danger">*</span></label>
                                    <select name="Status" class="form-control" required>
                                        <option value="Active"><?=lang('Active')?></option>
                                        <option value="NotConfirmed"><?=lang('NotConfirmed')?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?= lang('Save') ?></button>
                        </div>
                    </form>
                </div>
                <?php if (isset($user->ID)) { ?>
                <div role="tabpanel" class="tab-pane" id="routes">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th><?= lang('Route') ?></th>
                                <th style="max-width: 150px; text-align: center;"><a href="<?= site_url('admin/route_edit') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= lang('Add') ?></a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($routes as $route) { ?>
                            <tr>
                                <td><?= $route->ID ?></td>
                                <td><?= $route->CityFromName ?> <i class="fa fa-long-arrow-right"></i> <?= $route->CityToName ?></td>
                                <td class="text-center">
                                    <a href="<?= site_url('admin/route_edit/' . $route->ID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="buses">
                    <div style="margin-bottom: 10px;">
                        <a data-bus-id="0" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> <?= lang('AddNew')?></a>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div id="buses-list">

                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="route-edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('EditRoute') ?></h4>
            </div>
            <div class="modal-body">
                <form method="post">
                    <div class="location-selector">
                        <div class="row">
                            <div class="col-md-3">
                                <h2><?= lang('WhereFrom') ?></h2>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Country') ?></label>
                                    <select style="width: 100%;" class="form-control select2">
                                        <option>-</option>
                                        <option>Молдова</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('City') ?></label>
                                    <select style="width: 100%;" class="form-control select2">
                                        <option>-</option>
                                        <option>Кишинёв</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h2><?= lang('Whereto') ?></h2>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Country') ?></label>
                                    <select style="width: 100%;" class="form-control select2">
                                        <option>-</option>
                                        <option>Молдова</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('City') ?></label>
                                    <select style="width: 100%;" class="form-control select2">
                                        <option>-</option>
                                        <option>Кишинёв</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?= lang('Transport') ?></label>
                            <select style="width: 100%" class="form-control select2">
                                <option>-</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                    </div>
                    <hr />
                    <button id="add-middle-point" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> <?= lang('AddStation') ?></button>
                    <div style="margin-top: 10px;" id="middle-point-wrap">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('Close') ?></button>
                <button type="button" class="btn btn-primary"><?= lang('Save') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="template-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?=lang('BusTemplatePreview')?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('Close')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-bus-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=lang('EditBusData')?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button onclick="$('#edit-bus-form').submit();" type="button" class="btn btn-success"><?=lang('Save')?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('Close')?></button>
            </div>
        </div>
    </div>
</div>

<style>
    
    #template-modal form {
        zoom: 0.6;
    }
    
</style>

<script id="middle-point-tmpl" type="text/x-jquery-tmpl">
    <div class="row">
    <div class="col-md-4">
    <div class="form-group">
    <select class="form-control select2">
    <option>-</option>
    <option>Молдова</option>
    </select>
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
    <select class="form-control select2">
    <option>-</option>
    <option>Кишинёв</option>
    </select>
    </div>
    </div>
    <div class="col-md-3">
    <button type="button" onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </div>
    </div>
</script>

<script>

    $(document).ready(function () {
        $('#route-edit-modal').on('click', '#add-middle-point', function () {
            $('#middle-point-tmpl').tmpl().appendTo('#middle-point-wrap');
            applySelect2($('#middle-point-wrap .row:last .select2'));
        });

        applySelect2($('.select2'));
        
    
        appplySelect2ToCC();// country and city selects
        applyLocationSelect('select[name=Country]', 'select[name=City]'); 
        
        $('body').on('click', 'a[data-template]', function () {
            var template = $(this).attr('data-template');
            $.post('<?= site_url('main/template') ?>', {template: template}, function (html) {
                $('#template-modal .modal-body').html(html);
                $('#template-modal').modal('show');
            });
        });
        
        $('body').on('click', 'a[data-bus-id]', function () {
            var id = $(this).attr('data-bus-id');
            $.post('<?= site_url('admin/edit_bus_form') ?>', {id: id, 'userID': <?= $user->ID ?>}, function (html) {
                $('#edit-bus-modal .modal-body').html(html);
                $('#edit-bus-modal').modal('show');
            });
        });
        
        $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

    });
    
function appplySelect2ToCC(){
    applySelect2('select[name=Country]');
    applySelect2('select[name=City]');
}


function getBuses()
{
    $.post('<?= site_url('admin/transporter_buses_list/' . $user->ID) ?>', {}, function(html){
        $('#buses-list').html(html);
        $('[data-toggle="tooltip"]').tooltip();
    });
}

</script>