<div class="row product-filter-item">
    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon"><?= lang('FilterName') ?></span>
            <select style="width: 100%;" class="form-control filter-select">
                <?php for ($i = 0; $i < 500; $i++) { ?>
                    <option value="<?= uniqid() ?>"><?= uniqid() ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="input-group">
            <span class="input-group-addon">RO</span>
            <select style="width: 100%;" class="form-control filter-value-select" multiple name="er" placeholder="Value"></select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="input-group">
            <span class="input-group-addon">RU</span>
            <select style="width: 100%;" class="form-control filter-value-select" multiple name="er" placeholder="Value"></select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="input-group">
            <span class="input-group-addon">EN</span>
            <select style="width: 100%;" class="form-control filter-value-select" multiple name="er" placeholder="Value"></select>
        </div>
    </div>
    <div class="col-md-1">
        <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </div>
</div>