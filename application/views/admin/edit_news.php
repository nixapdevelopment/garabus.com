<?php
$active_langs = $this->config->item('languages');
?>
<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-5">
                <h3 class="upper-case-title"><?= lang('News') ?></h3>
            </div>
            <div class="col-md-7 text-right">
                <?php if ($this->input->get('history') > 0) { ?>
                <b><?=lang('PreviewVersion')?> № <big><code><?= $this->input->get('history') ?></code></big> <?=lang('From')?> <big><code><?= date('d.m.Y H:i:s', strtotime($article->Date)) ?></code></big></b>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?= site_url('admin/edit_news', ['id' => $this->input->get('id')]) ?>" class="btn btn-sm btn-info"><i class="fa fa-upload" aria-hidden="true"></i> <?=lang('ToPublicVersion')?></a>&nbsp;&nbsp;&nbsp;
                <?php } ?>
                <a id="add-filter-btn" href="<?= site_url('admin/edit_news') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddNews') ?></a>
            </div>
        </div>
    </div>
    <div>
        <form id="edit-news-form" method="post" enctype="multipart/form-data">
            <?= $this->session->flashdata('success') ?>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?= lang('GeneralData') ?></a></li>
                    <li role="presentation"><a href="#tab-images" aria-controls="tab-images" role="tab" data-toggle="tab"><?= lang('Images') ?></a></li>
                    <?php if (!empty($article->ID)) { ?>
                    <li role="presentation"><a href="#tab-history" aria-controls="tab-history" role="tab" data-toggle="tab"><?=lang('ArticleNewsVersions')?></a></li>
                    <?php } ?>
                </ul>
                
                <div class="tab-content">
                    <br />
                    <div role="tabpanel" class="tab-pane active" id="tab-main">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Link') ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><?= site_url() ?></span>
                                        <input name="Link" value="<?= isset($article_link) ? $article_link : '' ?>" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Date') ?></label>
                                    <input onkeydown="return false" class="form-control datepicker" type="text" name="Date" value="<?= !empty($article->DiscountPriceStart) ? date('d.m.Y', strtotime($article->Date)) : date('d.m.Y') ?>" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Status') ?></label>
                                    <select class="form-control" name="Status">
                                        <option <?= @$article->Status == 'Active' ? 'selected' : '' ?> value="Active"><?= lang('ProdStatusActive') ?></option>
                                        <option <?= @$article->Status == 'Disabled' ? 'selected' : '' ?> value="Disabled"><?= lang('ProdStatusDisabled') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#tab-<?= $lang['LangFile'] ?>" aria-controls="tab-<?= $lang['LangFile'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
                                <?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br />
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="tab-<?= $lang['LangFile'] ?>">
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> <i class="text-danger">*</i></label>
                                            <input required class="form-control" type="text" name="Title[<?= $langID ?>]" value="<?= isset($article_langs[$langID]->Title) ? $article_langs[$langID]->Title : '' ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Text') ?></label>
                                            <textarea class="form-control ckeditor" name="Text[<?= $langID ?>]"><?= isset($article_langs[$langID]->Text) ? $article_langs[$langID]->Text : '' ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> SEO</label>
                                            <input class="form-control" type="text" name="TitleSeo[<?= $langID ?>]" value="<?= isset($article_langs[$langID]->TitleSeo) ? $article_langs[$langID]->TitleSeo : '' ?>" />
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Keywords') ?></label>
                                                    <textarea class="form-control" name="Keywords[<?= $langID ?>]"><?= isset($article_langs[$langID]->Keywords) ? $article_langs[$langID]->Keywords : '' ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Description') ?></label>
                                                    <textarea class="form-control" name="Description[<?= $langID ?>]"><?= isset($article_langs[$langID]->Description) ? $article_langs[$langID]->Description : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-images">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Images') ?> <?= count($images) > 0 ? '' : '<i class="text-danger">*</i>' ?></label>
                                    <input <?= count($images) > 0 ? '' : 'required' ?> class="form-control" accept=".png,.jpg" type="file" name="Images[]" multiple />
                                </div>
                                <table id="product-images" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th><?=lang('ImagePhoto')?></th>
                                            <th><?= lang('GeneralPhoto')?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($images) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($images as $img) { ?>
                                        <?php $i++; ?>
                                        <tr data-image-id="<?= $img['ID'] ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a rel="photos" href="<?= base_url('public/uploads/news/' . $img['Image']) ?>" class="fancybox">
                                                    <img style="height: 90px;" src="<?= base_url('public/uploads/news/' . $img['Thumb']) ?>" class="img-thumbnail" />
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="PrimaryImage" <?= $img['IsMain'] == 1 ? 'checked' : '' ?> value="<?= $img['ID'] ?>">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" onclick="return deleteImage(<?= $img['ID'] ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> <?= lang('DeleteSmth')?></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="4"class="text-center">
                                                <?= lang('NoImageUploaded')?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Files') ?></label>
                                    <input class="form-control" accept=".doc,.docx,.rar,.zip,.pdf,.xls,.xlsx" type="file" name="Files[]" multiple />
                                </div>
                                <table id="product-files" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th><?= lang('FileName') ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($files) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($files as $file) { ?>
                                        <?php $i++; ?>
                                        <tr data-file-id="<?= $file->ID ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?= base_url('public/uploads/news/' . $file->Path) ?>" target="_blank">
                                                    <?= $file->Name ?>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" onclick="return deleteFile(<?= $file->ID ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> <?= lang('DeleteSmth')?></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="3"class="text-center">
                                                <?= lang('NoFileUploaded')?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($article->ID)) { ?>
                    <div role="tabpanel" class="tab-pane" id="tab-history">
                        <?= $history ?>
                    </div>
                    <?php } ?>
                </div>
                
            </div>
            
            <div>
                <button type="submit" name="publish" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> <?=lang('SaveNPublish')?></button>&nbsp;&nbsp;&nbsp;
                <?php if (!empty($article->ID)) { ?>
                <button type="submit" name="save" class="btn btn-success btn-lg"><i class="fa fa-bookmark-o" aria-hidden="true"></i> <?=lang('SaveHistoryVersions')?></button>&nbsp;
                <input placeholder="<?=lang('CommentToVersion')?>" style="display: inline-block; max-width: 500px;" type="text" name="Comment" value="<?= isset($article->Comment) ? $article->Comment : '' ?>" class="form-control" />
                <?php } ?>
            </div>
        </form>
    </div>
</div>

<script>

    $('input.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true
    });
    
    $("#edit-news-form").validate({
        errorClass: "text-danger",
        validClass: "text-success",
        errorElement: "span",
        focusCleanup: false,
        focusInvalid: true,
        onsubmit: true,
        ignore: "",
        invalidHandler: function ()
        {
            notif({
                msg: "<?= lang('InvalidForm') ?>",
                type: "error",
                position: "right"
            });
        }
    });
    
    $('a.fancybox').fancybox();
    
    function deleteFile(fileID)
    {
        if (confirm('<?= lang('Confirm?')?>'))
        {
            $.post('<?= site_url('admin/delete_file') ?>', {fileID: fileID}, function(){
                $('tr[data-file-id=' + fileID + ']').remove();
            });
        }
        return false;
    }
    
    function deleteImage(imgID)
    {
        if (confirm('<?= lang('Confirm?')?>'))
        {
            $.post('<?= site_url('admin/delete_news_image') ?>', {imgID: imgID}, function(){
                $('tr[data-image-id=' + imgID + ']').remove();
            });
        }
        return false;
    }

</script>