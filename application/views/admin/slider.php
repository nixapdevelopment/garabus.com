<div class="container">
    <h1><?= lang('Slider')?></h1>
    <div>
        <form method="post" class="form-inline" enctype="multipart/form-data">
            <div class="form-group">
                <input required type="file" name="image" class="form-control" />
            </div>
            <button name="submit" class="btn btn-primary" type="submit"><?= lang('Save')?></button>
            <a class="btn btn-link">( JPG sau PNG )</a>
        </form>
        <hr />
        <div>
            <?= $this->session->flashdata('success'); ?>
            <?php if (isset($errors)) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $errors ?>
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <form id="slider-form" class="col-md-4">
                <table class="table table-striped table-bordered">
                    <tbody>
                        <?php foreach ($sliders as $slide) { ?>
                        <tr>
                            <td style="width: 40px; font-size: 25px;">
                                <i style="cursor: move;" class="glyphicon glyphicon-menu-hamburger"></i>
                            </td>
                            <td class="text-center">
                                <a class="fancybox" href="<?= base_url('public/uploads/sliders/' . $slide->Image) ?>">
                                    <img style="height: 100px;" src="<?= base_url('public/uploads/sliders/' . $slide->Image) ?>" class="img-thumbnail" />
                                </a>
                                <input type="hidden" name="sid[]" value="<?= $slide->ID ?>" />
                            </td>
                            <td class="text-center">
                                <a href="?delID=<?= $slide->ID ?>"><i style="cursor: pointer;" class="glyphicon glyphicon-trash text-danger"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<style>
    
    table.table tr td {
        vertical-align: middle;
    }
    
    table.table tr.ui-sortable-helper
    {
        height: 100px;
    }
    
</style>

<script>

    var sort = [];
    $( "table tbody" ).sortable({
        placeholder: "ui-state-highlight",
	update: function( event, ui ) {
            $.post('<?= site_url('admin/slider_sort') ?>', $('#slider-form').serialize(), function(){
                notif({
                    msg: "<?= lang('SortSaved')?>",
                    type: "success",
                    position: "right"
                });
            });
        },
        start: function(e, ui) {
            ui.placeholder.height(ui.item.height());
        }
    });

</script>