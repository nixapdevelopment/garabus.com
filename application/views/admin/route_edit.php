<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('RouteEdit') ?></h3>
            </div>
        </div>
    </div>
    <div>
        <div>

            <?= $this->session->flashdata('success') ?>
            
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#route" aria-controls="route" role="tab" data-toggle="tab"><?=lang('Route')?></a></li>
                <?php if (!empty($route->ID)) { ?>
                    <li role="presentation"><a href="#shedule" aria-controls="shedule" role="tab" data-toggle="tab"><?=lang('Schedule')?></a></li>
                    <li role="presentation"><a href="#seats" aria-controls="seats" role="tab" data-toggle="tab"><?=lang('Seats')?></a></li>
                <?php } ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <br />
                <div role="tabpanel" class="tab-pane active" id="route">
                    <form method="post">
                        <div class="location-selector">
                            <div class="row">
                                <div class="col-md-2">
                                    <h2><?= lang('WhereFrom') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Country') ?></label>
                                        <?= CountryModel::dropdown($this->langID, empty($route->CountryFrom) ? '' : $route->CountryFrom, 'class="form-control county-select-from"', 'CountryFrom') ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('City') ?></label>
                                        <?= CityModel::dropdown($this->langID, empty($route->CountryFrom) ? '' : $route->CountryFrom, 'CityFrom', empty($route->CityFrom) ? '' : $route->CityFrom, 'class="form-control select2" style="width: 100%;"') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('Station')?></label>
                                        <input style="width: 100%;" name="StationFrom" value="<?= !empty($stations) ? reset($stations)->Station : '' ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('DayFrom')?></label>
                                        <?= RouteModel::daySelect($this->langID, 'DayFrom', !empty($stations) ? reset($stations)->Day : '', 'class="form-control"') ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('TimeFrom')?></label>
                                        <input style="width: 100%;" name="TimeFrom" value="<?= !empty($stations) ? reset($stations)->Time : '' ?>" readonly class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <h2><?= lang('Whereto') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Country') ?></label>
                                        <?= CountryModel::dropdown($this->langID, empty($route->CountryTo) ? '' : $route->CountryTo, 'class="form-control county-select-to"', 'CountryTo') ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('City') ?></label>
                                        <?= CityModel::dropdown($this->langID, empty($route->CountryTo) ? '' : $route->CountryTo, 'CityTo', empty($route->CityTo) ? '' : $route->CityTo, 'class="form-control select2" style="width: 100%;"') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('Station')?></label>
                                        <input style="width: 100%;" name="StationTo" value="<?= !empty($stations) ? end($stations)->Station : '' ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?=lang('DayTo')?></label>
                                        <?= RouteModel::daySelect($this->langID, 'DayTo', empty($stations) ? '' : end($stations)->Day, 'class="form-control"') ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('TimeTo')?></label>
                                        <input style="width: 100%;" name="TimeTo" value="<?= empty($stations) ? '' : end($stations)->Time ?>" readonly class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Transport') ?></label>
                                        <?= form_dropdown('TransporterBusID', $buses, empty($route->TransporterBusID) ? '' : $route->TransporterBusID, 'class="form-control" required') ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang('Price') ?></label>
                                        <input type="number" min="0.01" step="0.01" required value="<?= empty($route->Price) ? '' : $route->Price ?>" name="Price" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label"><?=lang('Transporter')?></label>
                            <?= UserModel::dropdown($this->langID, empty($route) ? '' : $route->UserID, 'class="form-control" required', 'Transporter') ?>
                        </div>
                        <hr />
                        <button id="add-middle-point" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> <?= lang('AddStation') ?></button>
                        <div style="margin-top: 10px; font-weight: bold;" class="row">
                            <div class="col-md-1">
                                
                            </div>
                            <div class="col-md-3">
                                <?=lang('Country')?>
                            </div>
                            <div class="col-md-3">
                                <?=lang('City')?>
                            </div>
                            <div class="col-md-2">
                                <?= lang('Station')?>
                            </div>
                            <div class="col-md-1">
                                <?=lang('Day')?>
                            </div>
                            <div class="col-md-1">
                                <?= lang('Time')?>
                            </div>
                        </div>
                        <div style="margin-top: 10px;" id="middle-point-wrap">
                            <?php foreach ($stations as $station) { ?>
                            <?php if ($station->Type != 'Middle') continue; ?>
                            <div class="row">
                                <div class="col-md-1 text-center">
                                    <i class="fa fa-arrows" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= CountryModel::dropdown($this->langID, $station->CountryID, 'class="form-control select2"', 'CountryID[]') ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= CityModel::dropdown($this->langID, $station->CountryID, 'CityID[]', $station->CityID, 'class="form-control select2" required') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input style="width: 100%;" name="Station[]" value="<?= $station->Station ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <?= RouteModel::daySelect($this->langID, 'Day[]', $station->Day, 'class="form-control"') ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input style="width: 100%;" name="Time[]" readonly value="<?= $station->Time ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <hr />
                        <button class="btn btn-success btn-lg"><i class="fa fa-save"></i> <?= lang('Save') ?></button>
                    </form>
                </div>
                <?php if (!empty($route->ID)) { ?>
                    <div role="tabpanel" class="tab-pane" id="shedule">
                        <div id="calendar" style="min-height: 480px; overflow: hidden;"></div>
                        <button id="save-shedule" style="margin-left: 30px; margin-top: 10px;" class="btn btn-success btn-lg"><i class="fa fa-save"></i> <?= lang('Save') ?> расписание</button>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="seats">
                        <?php $this->load->view('buses/delfin') ?>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<script id="middle-point-tmpl" type="text/x-jquery-tmpl">
<div class="row">
    <div class="col-md-1 text-center">
        <i class="fa fa-arrows" aria-hidden="true"></i>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= CountryModel::dropdown($this->langID, '', 'class="form-control select2"', 'CountryID[]') ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <select name="CityID[]" class="form-control select2"></select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <input style="width: 100%;" name="Station[]" class="form-control" />
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <?= RouteModel::daySelect($this->langID, 'Day[]', '', 'class="form-control"') ?>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <input style="width: 100%;" name="Time[]" readonly class="form-control" />
        </div>
    </div>
    <div class="col-md-1">
        <button type="button" onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </div>
</div>
</script>

<script>

    $(document).ready(function(){
        $('body').on('click', '#add-middle-point', function(){
            $('#middle-point-tmpl').tmpl().appendTo('#middle-point-wrap');
            var row = $('#middle-point-wrap .row:last');
            applySelect2(row.find('.select2'));
            applyLocationSelect(row.find('select[name="CountryID[]"]'), row.find('select[name="CityID[]"]'));
            applyTimeSelect(row.find('input[name="Time[]"]'));
        });

        applySelect2($('.select2'));
        applySelect2($('.county-select-from, .county-select-to'));
        applySelect2($('select[name=UserID]'));
        
        applyLocationSelect($('.county-select-from'), $('select[name="CityFrom"]'));
        applyLocationSelect($('.county-select-to'), $('select[name="CityTo"]'));
        
        applyTimeSelect('input[name="Time[]"]');
        applyTimeSelect('input[name="TimeTo"], input[name="TimeFrom"]');

        function saveEvent(date)
        {
            var event = {
                color: '#2C8FC9',
                startDate: date,
                endDate: date,
                date: moment(date).format('YYYY-MM-DD')
            };
            
            var newDate = moment(date).format('YYYY-MM-DD');

            var dataSource = $('#calendar').data('calendar').getDataSource();
            
            var exist = false;
            var index = 0;
            $.each(dataSource, function(ind, data){
                try {
                    var oldDate = moment(data.startDate).format('YYYY-MM-DD');
                    if (newDate === oldDate)
                    {
                        exist = true;
                        index = ind;
                    }
                } catch (e) {

                }
            });
            
            if (exist)
            {
                delete dataSource[index];
            }
            else
            {
                dataSource.push(event);
            }

            $('#calendar').data('calendar').setDataSource(dataSource);
        }
        
        var calendar = $('#calendar').calendar({
            allowOverlap: true,
            clickDay: function(e) {
                saveEvent(e.date);
                return false;
            },
            minDate: new Date(),
            style: 'background',
            enableRangeSelection: false,
            dataSource: [
                <?php foreach ($trips as $trip) { ?>
                {
                    color: '#2C8FC9',
                    startDate: new Date(<?= $trip->y ?>, <?= $trip->m ?>, <?= $trip->d ?>),
                    endDate: new Date(<?= $trip->y ?>, <?= $trip->m ?>, <?= $trip->d ?>),
                    date: moment(new Date(<?= $trip->y ?>, <?= $trip->m ?>, <?= $trip->d ?>)).format('YYYY-MM-DD')
                },
                <?php } ?>
            ]
        });
        
        $('#save-shedule').click(function(){
            $.post('<?= site_url('admin/save_shedule/' . (empty($route->ID) ? 0 : $route->ID)) ?>', {data: $('#calendar').data('calendar').getDataSource()}, function(){
                notif({
                    msg: "<?=lang('ScheduleSaved')?>",
                    type: "success",
                    position: "right"
                });
            });
        });
        
        $('#middle-point-wrap').sortable();
        
    });

</script>

<style>
    .in-admin .select2-container--bootstrap .select2-selection--single,
    .in-admin .select2-container--bootstrap.select2-container--focus .select2-selection, 
    .in-admin .select2-container--bootstrap.select2-container--open .select2-selection
{
    padding: 10px 15px !important;
    height: 45px !important;
    border-radius: 0 !important;
    border: 2px solid #dce4ec !important;
    box-shadow: 0 0 0 !important;
}

#middle-point-wrap .text-center i {
    font-size: 33px;
}
</style>