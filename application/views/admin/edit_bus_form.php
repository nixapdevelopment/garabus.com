<form id="edit-bus-form">
    
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?=lang('GeneralData')?></a></li>
          <li role="presentation"><a href="#tab-images" aria-controls="tab-images" role="tab" data-toggle="tab"><?= lang('Images')?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <br />
            <div role="tabpanel" class="tab-pane active" id="tab-main">
                <div class="form-group">
                    <label class="control-label"><?=lang('BusName')?></label>
                    <input type="text" name="Name" value="<?= isset($bus->Name) ? $bus->Name : '' ?>" class="form-control" required />
                </div>
                <br />
                <div class="form-group">
                    <label class="control-label"><?=lang('BusTemplate')?></label>
                    <div class="row">
                        <?php foreach ($templates as $key => $template) { ?>
                        <div  class="col-md-3 text-center">
                            <div style="font-size: 18px;  border: 1px solid #ccc; padding: 20px 0;">
                                <div><?= $template['name'] ?> <a data-template="<?= $key ?>" style="cursor: pointer;"><i class="fa fa-eye"></i></a></div>
                                <div>Мест: <?= $template['seats'] ?></div>
                                <input type="radio" name="Template" value="<?= $key ?>" <?= isset($bus->Template) && $bus->Template == $key ? 'checked' : '' ?> required />
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab-images">
                <div class="form-group">
                    <label class="control-label"><?=lang('UploadImages')?></label>
                    <input type="file" name="Images[]" value="" class="form-control" multiple />
                </div>
                <div>
                    <?php foreach ($bus_images as $img) { ?>
                    <div class="bus-image-item pull-left text-center" style="margin-right: 5px; margin-bottom: 15px;">
                        <div>
                            <a href="<?= base_url('public/uploads/buses/' . $img->Image) ?>" class="fancybox" rel="bus-images">
                                <img style="height: 110px;" class="img-thumbnail img-responsive" src="<?= base_url('public/uploads/buses/' . $img->Image) ?>" />
                            </a>
                        </div>
                        <div>
                            <a data-delete-id="<?= $img->ID ?>" style="cursor: pointer;"><i class="fa fa-trash text-danger"></i></a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="ID" value="<?= isset($bus->ID) ? $bus->ID : 0 ?>" />
    <input type="hidden" name="UserID" value="<?= isset($bus->UserID) ? $bus->UserID : $this->input->post('userID') ?>" />
</form>

<script>

$('#edit-bus-form').validate({
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        
    },
    submitHandler: function (form, event) {
        var data = new FormData(document.getElementById('edit-bus-form'));
        $.ajax({
            url: '<?= site_url('admin/bus_save') ?>',
            data: data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function()
            {
                $('#edit-bus-modal').modal('hide');
                getBuses();
            }
        });
        return false;
    }
});

$('#edit-bus-form a[data-delete-id]').click(function(){
    if (confirm('Confirm?'))
    {
        var control = $(this);
        $.post('<?= site_url('admin/delete_bus_image') ?>', {id: control.attr('data-delete-id')}, function(){
            control.closest('.bus-image-item').remove();
        });
    }
});

</script>