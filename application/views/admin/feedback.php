<div class="container">
    <div class="dash-block">
        <h1><?= lang('Feedback') ?></h1>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th><?= lang('Name')?></th>
                    <th><?= lang('Email')?></th>
                    <th><?= lang('Phone')?></th>
                    <th><?= lang('Message')?></th>
                    <th><?= lang('Date')?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($feedbacks as $feedback) { ?>
                <tr>
                    <td><?= $feedback->Name ?></td>
                    <td><?= $feedback->Email ?></td>
                    <td><?= $feedback->Phone ?></td>
                    <td style="width: 40%;"><?= $feedback->Message ?></td>
                    <td><?= date('d.m.Y H:i', strtotime($feedback->Date)) ?></td>
                    <td style="vertical-align: middle;" class="text-center"><a onclick="return confirm('<?= lang('DeleteFeedbackQ')?>')" href="?delID=<?= $feedback->ID ?>"><i class="glyphicon glyphicon-trash text-danger"></i></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>