<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('Products') ?></h3>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-filter-btn" href="<?= site_url('admin/edit_product') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddProduct') ?></a>
            </div>
        </div>
    </div>
    <div>
        <form id="search-product-form">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label"><?= lang('SKU') ?></label>
                        <input type="text" name="Sku" value="<?= $this->input->get('Sku') ?>" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Categories') ?></label>
                        <?= form_dropdown('Category[]', $all_categories, $this->input->get('Category[]'), 'class="form-control select2" multiple') ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label"><?= lang('ProductName') ?></label>
                        <input type="text" name="Name" value="<?= $this->input->get('Name') ?>" class="form-control" />
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Sorting') ?></label>
                        <?= form_dropdown('Sort', [
                            '0' => '-',
                            'az' => 'A-Z',
                            'za' => 'Z-A',
                            'price_d' => lang('Price') . ' &darr;',
                            'price_a' => lang('Price') . ' &uarr;',
                        ], $this->input->get('Sort'), 'class="form-control"') ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="row">
                            <div class="col-sm-2"><input style="height: 34px; width: 34px; position: relative; top: -4px;" type="checkbox" name="IsPromo" /></div>
                            <div class="col-sm-10" style="line-height: 32px; font-size: 18px;"><?= lang('IsPromo') ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label"><a href="<?= site_url('admin/products') ?>"><?= lang('ClearFilters') ?></a></label>
                        <button class="btn btn-info btn-block"><?= lang('Filter') ?></button>
                    </div>
                </div>
            </div>
        </form>
        <hr />
        <table id="product-table" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th><input type="checkbox" name="select_all" /></th>
                    <th><?= lang('Image') ?></th>
                    <th><?= lang('SKU') ?></th>
                    <th><?= lang('ProductName') ?></th>
                    <th><?= lang('Price') ?></th>
                    <th><?= lang('Stock') ?></th>
                    <th><?= lang('IsPromo') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product) { ?>
                <tr>
                    <td><input type="checkbox" name="select_one" /></td>
                    <td>
                        <?php if (!empty($product->Thumb)) { ?>
                        <a class="fancybox" href="<?= base_url('public/uploads/products/' . $product->Image) ?>">
                            <img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" class="img-thumbnail" style="height: 35px;" />
                        </a>
                        <?php } ?>
                    </td>
                    <td><?= $product->Sku ?></td>
                    <td style="min-width: 200px;"><?= $product->Name ?></td>
                    <td>
                        <input <?= $product->DiscountID > 0 ? 'readonly data-toggle="tooltip" data-placement="top" title="Acest produs are promo pret activ"' : '' ?> style="max-width: 100px; margin-right: 5px;" class="form-control input-sm pull-left" type="number" step="0.01" min="0.01" value="<?= (float)$product->Price ?>" />
                        <?php if (!($product->DiscountID > 0)) { ?>
                        <button pid="<?= $product->ID ?>" class="btn btn-success btn-sm pull-left update-product-price"><i class="fa fa-refresh"></i></button>
                        <?php } ?>
                    </td>
                    <td>
                        <input style="max-width: 100px; margin-right: 5px;" class="form-control input-sm pull-left" type="number" step="1" min="0" value="<?= (float)$product->Stock ?>" />
                        <button pid="<?= $product->ID ?>" class="btn btn-success btn-sm pull-left update-product-stock"><i class="fa fa-refresh"></i></button>
                    </td>
                    <td><?= $product->IsPromo ? '' : '' ?></td>
                    <td>
                        <a style="position: relative; top: 1px;" href="<?= site_url('admin/edit_product', ['id' => $product->ProductID], true) ?>"><i class="fa fa-edit text-success"></i></a>&nbsp;&nbsp;
                        <a href="#"><i class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="text-center">
            <?= $pagination ?>
        </div>
    </div>
</div>

<style>
    #product-table td {
        vertical-align: middle;
    }
</style>

<script>

    $('.select2').select2({
        theme: 'bootstrap'
    });
    
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    
    $('.update-product-price').click(function(){
        var pid = $(this).attr('pid');
        var price = $(this).closest('td').find('input').val();
        $.post('<?= site_url('admin/update_product_price') ?>', {pid: pid, price: price}, function(){
            notif({
                msg: "Prețul produsului este actualizat",
                type: "success",
                position: "right"
            });
        });
    });
    
    $('.update-product-stock').click(function(){
        var pid = $(this).attr('pid');
        var stock = $(this).closest('td').find('input').val();
        $.post('<?= site_url('admin/update_product_stock') ?>', {pid: pid, stock: stock}, function(){
            notif({
                msg: "Stocul produsului este actualizat",
                type: "success",
                position: "right"
            });
        });
    });

</script>