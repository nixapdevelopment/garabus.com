<?php
$active_langs = $this->config->item('languages');
?>
<div class="container">
    <div class="dash-block">
        <h3><?= lang('Seo') ?></h3>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <form id="edit-seo-form" method="post">
            <br />
            <div class="row">
                <div class="col-md-1">
                    <h2><?= lang('WhereFrom')?></h2>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><?=lang('Country')?></label>
                        <?= CountryModel::dropdown($this->langID, isset($route_seo->data->CountryFrom) ? $route_seo->data->CountryFrom : '', 'class="form-control" style="width:100%;"', 'CountryFrom') ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><?= lang('City')?></label>
                        <?= CityModel::dropdown($this->langID, isset($route_seo->data->CountryFrom) ? $route_seo->data->CountryFrom : '', 'CityFrom', isset($route_seo->data->CityFrom) ? $route_seo->data->CityFrom : '', 'class="form-control" style="width:100%;"') ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <h2><?= lang('Whereto')?></h2>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><?=lang('Country')?></label>
                        <?= CountryModel::dropdown($this->langID, isset($route_seo->data->CountryTo) ? $route_seo->data->CountryTo : '', 'class="form-control" style="width:100%;"', 'CountryTo') ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><?= lang('City')?></label>
                        <?= CityModel::dropdown($this->langID, isset($route_seo->data->CountryTo) ? $route_seo->data->CountryTo : '', 'CityTo', isset($route_seo->data->CityTo) ? $route_seo->data->CityTo : '', 'class="form-control" style="width:100%;"') ?>
                    </div>
                </div>
            </div>
            <br />
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?= lang('GeneralData') ?></a></li>
                    <?php if (!empty($route_seo->data->ID)) { ?>
                    <li role="presentation"><a href="#tab-history" aria-controls="tab-history" role="tab" data-toggle="tab"><?=lang('Versions')?></a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <br />
                    <div role="tabpanel" class="tab-pane active" id="tab-main">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#tab-<?= $lang['LangFile'] ?>" aria-controls="tab-<?= $lang['LangFile'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
                                <?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br />
                                <div>
                                    <?php if ($this->input->get('history') > 0) { ?>
                                    <b><?=lang('PreviewVersion')?> № <big><code><?= $this->input->get('history') ?></code></big> <?=lang('From')?> <big><code><?= date('d.m.Y H:i:s', strtotime($route_seo->data->Date)) ?></code></big></b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="<?= site_url('admin/edit_seo', ['id' => $this->input->get('id')]) ?>" class="btn btn-sm btn-info"><i class="fa fa-upload" aria-hidden="true"></i> <?=lang('ToPublicVersion')?></a>&nbsp;&nbsp;&nbsp;
                                    <?php } ?>
                                </div>
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="tab-<?= $lang['LangFile'] ?>">
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> <i class="text-danger">*</i></label>
                                            <input required class="form-control" type="text" name="Title[<?= $langID ?>]" value="<?= isset($route_seo->langs[$langID]->Title) ? $route_seo->langs[$langID]->Title : '' ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Text') ?></label>
                                            <textarea class="form-control ckeditor" name="Text[<?= $langID ?>]"><?= isset($route_seo->langs[$langID]->Text) ? $route_seo->langs[$langID]->Text : '' ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> SEO<i class="text-danger">*</i></label>
                                            <input required class="form-control" type="text" name="TitleSeo[<?= $langID ?>]" value="<?= isset($route_seo->langs[$langID]->TitleSeo) ? $route_seo->langs[$langID]->TitleSeo : '' ?>" />
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Keywords') ?></label>
                                                    <textarea required class="form-control" name="Keywords[<?= $langID ?>]"><?= isset($route_seo->langs[$langID]->Keywords) ? $route_seo->langs[$langID]->Keywords : '' ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Description') ?></label>
                                                    <textarea required class="form-control" name="Description[<?= $langID ?>]"><?= isset($route_seo->langs[$langID]->Description) ? $route_seo->langs[$langID]->Description : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($route_seo->data->ID)) { ?>
                    <div role="tabpanel" class="tab-pane" id="tab-history">
                        <?= $history ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            
            <button type="submit" name="publish" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> <?=lang('SaveNPublish')?></button>&nbsp;&nbsp;&nbsp;
            <?php if (!empty($route_seo->data->ID)) { ?>
            <button type="submit" name="save" class="btn btn-success btn-lg"><i class="fa fa-bookmark-o" aria-hidden="true"></i> <?=lang('SaveHistoryVersions')?></button>&nbsp;
            <input placeholder="<?=lang('CommentToVersion')?>" style="display: inline-block; max-width: 500px;" type="text" name="Comment" value="<?= isset($route_seo->data->Comment) ? $route_seo->data->Comment : '' ?>" class="form-control" />
            <?php } ?>
        </form>
    </div>
</div>

<script>

    $('#edit-seo-form').validate({
        errorClass: "text-danger",
        validClass: "text-success",
        errorElement: "span",
        focusCleanup: false,
        focusInvalid: true,
        onsubmit: true,
        ignore: "",
        lang: 'ru',
        invalidHandler: function (event, validator)
        {
            $('.nav-tabs a').removeClass('text-danger');
            
            $.each(validator.invalid, function(key, val){
                var id = $('*[name="' + key + '"]').closest('.tab-pane').attr('id');
                $('.nav-tabs a[href="#' + id + '"]').addClass('text-danger');
            });
            
            notif({
                msg: "<?= lang('InvalidForm') ?>",
                type: "error",
                position: "right"
            });
        }
    });
    
    $(document).ready(function(){
        
        applySelect2($('select[name=CountryFrom]'));
        applySelect2($('select[name=CityFrom]'));
        applySelect2($('select[name=CountryTo]'));
        applySelect2($('select[name=CityTo]'));
        applyLocationSelect($('select[name=CountryFrom]'), $('select[name=CityFrom]'));
        applyLocationSelect($('select[name=CountryTo]'), $('select[name=CityTo]'));
        
    });

</script>

