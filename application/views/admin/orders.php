<div class="container">
    <div class="dash-block">
        <h3><?= lang('Orders') ?></h3>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Date') ?></th>
                            <th><?= lang('Address') ?></th>
                            <th><?= lang('Amount') ?></th>
                            <th><?= lang('PaymentType') ?></th>
                            <!--th><?= lang('ClientType') ?></th-->
                            <th><?= lang('Status') ?></th>
                            <th class="text-center"></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <form>
                            <tr>
                                <td></td>
                                <td>
                                    <?= form_dropdown('ClientID', $clients, $this->input->get('ClientID'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('Date') ?>" onchange="return false;" type="text" name="Date" class="form-control datepicker" />
                                        <span class="input-group-btn">
                                            <button onclick="$('input[name=Date]').val('')" style="margin-top: 1px;" class="btn btn-danger" type="button">Clear</button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?= form_dropdown('PaymentType', ['' => lang('All'), 'Cash' => lang('Cash'), 'Card' => lang('Card')], $this->input->get('PaymentType'), 'class="form-control select-2"') ?>
                                </td>
                                <!--td>
                                    <?= form_dropdown('Type', ['' => lang('All'), 'Angro' => lang('Angro'), 'Retail' => lang('Retail')], $this->input->get('Type'), 'class="form-control select-2"') ?>
                                </td-->
                                <td>
                                    <?= form_dropdown('Status', ['' => lang('All'), 'Pending' => lang('Pending'), 'Paid' => lang('Paid')], $this->input->get('Status'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> <?= lang('Filter') ?></button>
                                    <a href="<?= site_url('admin/orders') ?>" type="submit" class="btn btn-danger"><?= lang('ClearFilters') ?></a>
                                </td>
                            </tr>
                        </form>
                        <?php foreach ($orders as $order) { ?>
                        <tr data-oid="<?= $order->ID ?>">
                            <td class="text-center"><a href="<?= site_url('admin/order/' . $order->ID) ?>"><?= $order->ID ?></a></td>
                            <td><a target="<?= empty($order->UserID) ? '' : '_blank' ?>" style="<?= empty($order->UserID) ? 'color:inherit;' : '' ?>" href="<?= empty($order->UserID) ? '#' : site_url('admin/user/' . $order->UserID) ?>"><?= $order->Name ?></a></td>
                            <td><?= date('d.m.Y', strtotime($order->Date)) ?></td>
                            <td><?= $order->Address ?></td>
                            <td><?= $order->Amount ?></td>
                            <td><?= $order->PaymentType ?></td>
                            <td><?= $order->Type ?></td>
                            <td><span data-html="true" data-toggle="popover" title="<b>Statut comenzii</b>" data-content="<div><a class='text-danger' oid='<?= $order->ID ?>'>Canceled</a></div><div><a class='text-warning' oid='<?= $order->ID ?>'>Pending</a></div><div><a class='text-success' oid='<?= $order->ID ?>'>Paid</a></div><div><a class='text-primary' oid='<?= $order->ID ?>'>Closed</a></div>"><?= $order->Status ?></span></td>
                            <td class="text-center">
                                <a href="<?= site_url('admin/order/' . $order->ID, $_GET) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                <?php if ($order->IsNew) { ?>
                                <span class="label label-primary">NEW</span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="text-center hidden">
                    <?= $pagination ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$(document).on('click', 'a[oid]', function () {

    var orderID = $(this).attr('oid');
    var status = '';
    
    alert(orderID);

    switch ($(this).attr('class'))
    {
        case 'text-warning':
            status = 'Pending';
            break;
        case 'text-danger':
            status = 'Canceled';
            break;
        case 'text-success':
            status = 'Paid';
            break;
        case 'text-primary':
            status = 'Closed';
            break;
    }

    $.post('/admin/change_order_status', {orderID: orderID, status: status}, function () {
        notif({
            msg: 'Status schimbat',
            type: 'success',
            position: "right"
        });
        $('tr[data-oid=' + orderID + '] td span[data-html="true"]').text(status);
    });
    
});

</script>