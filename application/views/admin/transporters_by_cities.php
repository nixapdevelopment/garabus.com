<table class="table table-hover table-bordered table-striped">
    <thead>
        <tr>
            <th><?=lang('Transporter')?></th>
            <th><?=lang('ContactPerson')?></th>
            <th><?=lang('Email')?></th>
            <th><?=lang('Phone')?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($transporters as $transporter) { ?>
        <tr>
            <td><?= $transporter->CompanyName ?></td>
            <td><?= $transporter->Name ?></td>
            <td><?= $transporter->UserName ?></td>
            <td><?= $transporter->Phone ?></td>
            <td>
                <a href="<?= site_url('admin/edit_transporter/' . $transporter->ID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>