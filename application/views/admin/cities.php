<div class="container">

    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('Cities') ?></h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <a data-edit-city="0" data-toggle="modal" data-target="#city-edit-modal" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddCity') ?></a>
            <hr />
            <table class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>
                            <?= lang('Title') ?>
                        </th>
                        <th style="width: 100px;">

                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cities as $city) { ?>
                        <tr>
                            <td><?= $city->Name ?></td>
                            <td>
                                <a data-edit-city="<?= $city->ID ?>" data-toggle="modal" data-target="#city-edit-modal" href="#"><i class="glyphicon glyphicon-pencil"></i></a>                                
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">

        </div>
    </div>
</div>

<div id="city-edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= lang('EditCity') ?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('Close')?></button>
                <button onclick="$('#city-edit-form').submit()" type="button" class="btn btn-primary"><?= lang('Save')?></button>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).on('click', 'a[data-edit-city]', function(){
        var id = $(this).attr('data-edit-city');
        $.post('<?= site_url('admin/city_get') ?>', {id: id}, function(html){
            $('#city-edit-modal .modal-body').html(html);
        });
    });

</script>