<div class="container">
    <div class="dash-block">
        <div>
            <h3 class="pull-left"><?= lang('Transporters') ?></h3>
            <div class="pull-right">
                <br />
                <a href="<?= site_url('admin/edit_transporter') ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= lang('Add') ?></a>
                <button class="btn btn-primary"><i class="fa fa-envelope-o"></i> <?= lang('SendEmail') ?></button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <hr />
        <table id="transporters-table" class="table table-striped table-bordered datatable">
            <thead>
                <tr>
                    <th></th>
                    <th><?= lang('Name') ?></th>
                    <th><?= lang('ContactPerson') ?></th>
                    <th><?= lang('Email') ?></th>
                    <th><?= lang('Phone') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($transporters as $transporter) { ?>
                <tr>
                    <td class="text-center"><input type="checkbox" name="UserID" value="<?= $transporter->ID ?>" /></td>
                    <td><?= $transporter->CompanyName ?></td>
                    <td><?= $transporter->Name ?></td>
                    <td><?= $transporter->UserName ?></td>
                    <td><?= $transporter->Phone ?></td>
                    <td class="text-center">
                        <a href="<?= site_url('admin/edit_transporter/' . $transporter->ID) ?>"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;
                        <a onclick="return confirm('Confirm?')" href="<?= site_url('admin/delete_transporter/' . $transporter->ID) ?>"><i class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>