<div class="container">
    <div class="dash-block">
        <div>
            <h3 class="pull-left"><?= lang('Seo') ?></h3>
            <div class="pull-right">
                <br />
                <a href="<?= site_url('admin/edit_seo') ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= lang('Add') ?></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <br />
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        
        <table class="table table-striped table-bordered datatable">
            <thead>
                <tr>
                    <th><?= lang('Route') ?></th>
                    <th><?= lang('Title') ?></th>
                    <th><?= lang('Title') ?> SEO</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($route_seos as $rs) { ?>
                <tr>
                    <td><?= $rs->CityFromName ?> <i class="fa fa-long-arrow-right"></i> <?= $rs->CityToName ?></td>
                    <td><?= $rs->Title ?></td>
                    <td><?= $rs->TitleSeo ?></td>
                    <td class="text-center">
                        <a href="<?= site_url('admin/edit_seo', ['id' => $rs->ID]) ?>"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;
                        <a onclick="return confirm('<?=lang('ConfirmSmth')?>');" href="<?= site_url('admin/delete_seo/' . $rs->ID) ?>"><i class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>