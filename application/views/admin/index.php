<div class="container">
    <!--div class="row">
        <div class="col-md-4">
            <a href="<?= site_url('admin/products') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-shopping-bag"></i>
                    <h4><?= $product_count ?> - produse total</h4>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?= site_url('admin/categories') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-list-ul"></i>
                    <h4><?= $category_count ?> - categorii</h4>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?= site_url('admin/users/clients') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-users"></i>
                    <h4><?= $client_count ?> - clienți</h4>
                </div>
            </a>
        </div>
    </div-->
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-success">
                <div class="panel-heading"><?= lang('LastTranzactions') ?> <a href="<?= site_url('admin/orders') ?>" class="pull-right white-href"><i class="fa fa-shopping-cart"></i> <?= lang('SeeAllOrders') ?></a></div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Date') ?></th>
                            <th><?= lang('Amount') ?></th>
                            <!--th><?= lang('PaymentType') ?></th>
                            <th><?= lang('ClientType') ?></th>
                            <th><?= lang('Status') ?></th-->
                            <th class="text-center"></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $order) { ?>
                        <tr data-oid="<?= $order->ID ?>">
                            <td class="text-center"><a href="<?= site_url('admin/order/' . $order->ID) ?>"><?= $order->ID ?></a></td>
                            <td><a style="<?= empty($order->UserID) ? 'color:inherit;' : '' ?>" href="<?= empty($order->UserID) ? '#' : site_url('admin/user/' . $order->UserID) ?>"><?= $order->Name ?></a></td>
                            <td><?= date('d.m.Y H:i', strtotime($order->Date)) ?></td>
                            <td><?= $order->Amount ?></td>
                            <!--td><?= $order->PaymentType ?></td>
                            <td><?= $order->Type ?></td>
                            <td><span data-html="true" data-toggle="popover" title="<b>Statut comenzii</b>" data-content="<div><a class='text-danger' oid='<?= $order->ID ?>'>Canceled</a></div><div><a class='text-warning' oid='<?= $order->ID ?>'>Pending</a></div><div><a class='text-success' oid='<?= $order->ID ?>'>Paid</a></div><div><a class='text-primary' oid='<?= $order->ID ?>'>Closed</a></div>"><?= $order->Status ?></span></td-->
                            <td class="text-center">
                                <a href="<?= site_url('admin/order/' . $order->ID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                <?php if ($order->IsNew) { ?>
                                    <span class="label label-primary">NEW</span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>



        </div>
        <div class="col-md-6">

            <div class="panel panel-success">
                <div class="panel-heading"><?= lang('LastCarriers') ?> <a href="<?= site_url('admin/transporters') ?>" class="pull-right white-href"><i class="fa fa-bus"></i> <?= lang('SeeAllCarriers') ?></a></div>

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('ContactPerson') ?></th>
                            <th><?= lang('Email') ?></th>
                            <th><?= lang('Phone') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($transporters as $tr) { ?>
                        <tr>
                            <td><?= $tr->CompanyName ?></td>
                            <td><?= $tr->Name ?></td>
                            <td><?= $tr->UserName ?></td>
                            <td><?= $tr->Phone ?></td>
                            <td class="text-center">
                                <a href="<?= site_url('admin/edit_transporter/' . $tr->ID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>                
            </div>

        </div>
        <div class="col-md-12">
            <div class="dash-block">
                <h3><?= lang('Feedback') ?></h3>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Email') ?></th>
                            <th><?= lang('Phone') ?></th>
                            <th><?= lang('Message') ?></th>
                            <th><?= lang('Date') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Vladimir Cioban</td>
                            <td>info@appss.eu</td>
                            <td>78301555</td>
                            <td style="width: 40%;">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In id magna orci. Nunc quis nulla diam. </td>
                            <td>05.12.2016 17:02</td>
                            <td style="vertical-align: middle;" class="text-center"><a onclick="return confirm('<?= lang('DeleteFeedbackQ') ?>')" href="?delID=4"><i class="glyphicon glyphicon-trash text-danger"></i></a></td>
                        </tr>
                        <tr>
                            <td>Nicolai Railean</td>
                            <td>info@tiptophotel.md</td>
                            <td>69004444</td>
                            <td style="width: 40%;">Nunc id libero egestas, tincidunt nunc vel, molestie lacus.</td>
                            <td>05.12.2016 17:02</td>
                            <td style="vertical-align: middle;" class="text-center"><a onclick="return confirm('<?= lang('DeleteFeedbackQ') ?>')" href="?delID=3"><i class="glyphicon glyphicon-trash text-danger"></i></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>