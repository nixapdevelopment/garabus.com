<?php
$active_langs = $this->config->item('languages');
?>

<div class="container">

    <div>
        <h3 class="upper-case-title"><?= lang('Menus') ?></h3>
        <br />
        <div>
            <form class="form-inline" method="get">
                <div class="form-group">
                    <label class="control-label"><?= lang('Menu')?></label>&nbsp;
                    <?= form_dropdown('id', $all_menus, $this->input->get('id'), 'class="form-control" style="width: 200px;"') ?>&nbsp;
                    <input type="submit" class="btn btn-primary" value="OK" />
                </div>
            </form>
        </div>
        <hr />
        <?= $this->session->flashdata('success') ?>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><?= lang('MenuItems')?></div>
                    <form method="post" class="panel-body">
                        <ul id="menu-items" class="list-group">
                            <?php foreach ($menu_items as $item) { ?>
                            <li class="list-group-item">
                                <i style="cursor: move;" class="fa fa-bars"></i>&nbsp;&nbsp;&nbsp;<?= $item['langs'][1]->Name ?><i style="cursor: pointer; font-size: 18px;" class="fa fa-trash text-danger pull-right"></i><a style="cursor: pointer;" class="pull-right"><i class="glyphicon glyphicon-collapse-down"></i></a>
                                <input type="hidden" name="Entity[<?= $item['data']->EntityType ?><?= $item['data']->EntityID ?>][ID]" value="<?= $item['data']->EntityID ?>" />
                                <input type="hidden" name="Entity[<?= $item['data']->EntityType ?><?= $item['data']->EntityID ?>][Type]" value="<?= $item['data']->EntityType ?>" />
                                
                                <div class="mi-langs" style="display: none; margin-top: 4px;">
                                    <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><?= $lang['Slug'] ?></span>
                                            <input name="Name[<?= $item['data']->EntityType ?>][<?= $item['data']->EntityID ?>][<?= $langID ?>]" required type="text" class="form-control" value="<?= $item['langs'][$langID]->Name ?>" name="">
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <div>
                            <button type="submit" name="submit" class="btn btn-success"><?= lang('Save')?></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?= lang('Pages')?></a></li>
                        <!--li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><?= lang('Categories')?></a></li-->
                    </ul>

                    <!-- Tab panes -->
                    <div id="items-list" class="tab-content">
                        <br />
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div id="page-list">
                                <?php foreach ($pages as $pID => $item) { ?>
                                <li class="list-group-item">
                                    <i style="cursor: move;" class="fa fa-bars"></i>&nbsp;&nbsp;&nbsp;<?= $item['langs'][1]->Name ?><i style="cursor: pointer; font-size: 18px;" class="fa fa-trash text-danger pull-right"></i><a style="cursor: pointer;" class="pull-right"><i class="glyphicon glyphicon-collapse-down"></i></a>
                                    <input type="hidden" name="Entity[Page<?=$pID?>][ID]" value="<?=$pID?>" />
                                    <input type="hidden" name="Entity[Page<?=$pID?>][Type]" value="Page" />

                                    <div class="mi-langs" style="display: none; margin-top: 4px;">
                                        <?php foreach ($active_langs as $langID => $lang) { ?>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><?= $lang['Slug'] ?></span>
                                                <input name="Name[Page][<?=$pID?>][<?= $langID ?>]" required type="text" class="form-control" value="<?= $item['langs'][$langID]->Name ?>" name="">
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </li>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages">
                            <div id="category-list">
                                <?php foreach ($categories as $cID => $item) { ?>
                                <li class="list-group-item">
                                    <i style="cursor: move;" class="fa fa-bars"></i>&nbsp;&nbsp;&nbsp;<?= $item['langs'][1]->Name ?><i style="cursor: pointer; font-size: 18px;" class="fa fa-trash text-danger pull-right"></i><a style="cursor: pointer;" class="pull-right"><i class="glyphicon glyphicon-collapse-down"></i></a>
                                    <input type="hidden" name="Entity[Category<?=$cID?>][ID]" value="<?=$cID?>" />
                                    <input type="hidden" name="Entity[Category<?=$cID?>][Type]" value="Category" />

                                    <div class="mi-langs" style="display: none; margin-top: 4px;">
                                        <?php foreach ($active_langs as $langID => $lang) { ?>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><?= $lang['Slug'] ?></span>
                                                <input name="Name[Category][<?=$cID?>][<?= $langID ?>]" required type="text" class="form-control" value="<?= $item['langs'][$langID]->Name ?>" name="">
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </li>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script>

    $('#menu-items').on('click', '.glyphicon-collapse-down', function (){
        $(this).removeClass('glyphicon-collapse-down').addClass('glyphicon-collapse-up');
        $(this).closest('li').find('.mi-langs').slideDown();
    });

    $('#menu-items').on('click', '.glyphicon-collapse-up', function (){
        $(this).addClass('glyphicon-collapse-down').removeClass('glyphicon-collapse-up');
        $(this).closest('li').find('.mi-langs').slideUp();
    });
    
    $('body').on('keyup', 'input[required]', function(){
        var val = $.trim($(this).val());
       
        if (val == '')
        {
            $(this).closest('.form-group').addClass('has-error');
        }
        else
        {
            $(this).closest('.form-group').removeClass('has-error');
        }
    });
    
    $('#menu-items').on('click', '.fa-trash', function(){
       $(this).closest('.list-group-item').remove(); 
    });
    
    $( "#page-list li" ).draggable({
        connectToSortable: "#menu-items",
        helper: "clone",
        handle: ".fa-bars",
        revert: "invalid"
    });
    
    $( "#category-list li" ).draggable({
        connectToSortable: "#menu-items",
        helper: "clone",
        handle: ".fa-bars",
        revert: "invalid"
    });
    
    function makeSortable()
    {
        $( "#menu-items" ).sortable({
            placeholder: "ui-state-highlight",
            handle: ".fa-bars",
            revert: true
        });
        $( "#menu-items" ).disableSelection();
    }
    
    makeSortable();

</script>

<style>
    
    #menu-items .ui-state-highlight
    {
        height: 42px;
        width: 100%;
    }
    
    #items-list .list-group-item > .pull-right {
        display: none;
    }
    
    #menu-items .list-group-item
    {
        width: 100% !important;
        
    }
    
    #menu-items
    {
        min-height: 100px;
    }
    
    #menu-items .ui-draggable {
        height: auto !important;
    }
    
</style>