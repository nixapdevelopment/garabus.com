<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('Route') ?></h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php $i = 0; ?>
                <?php foreach ($routes as $route) { ?>
                    <?php $i++; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading<?= $i ?>">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $route->CityFromName ?> <i class="fa fa-long-arrow-right"></i> <?= $route->CityToName ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $route->Count ?>
                                    </div>
                                    <div class="col-md-3">
                                        <a role="button" data-from="<?= $route->CityFrom ?>" data-to="<?= $route->CityTo ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i ?>" aria-expanded="true" aria-controls="collapse<?= $i ?>">
                                            <?=lang('ListofTransporters')?>
                                        </a>
                                    </div>
                                </div>
                            </h4>
                        </div>
                        <div id="collapse<?= $i ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $i ?>">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>

    $('a[data-from]').click(function () {
        var elem = $(this);
        elem.closest('.panel').find('.panel-body').html(LOADER);
        $.get('/admin/transporters_by_cities', {from: elem.attr('data-from'), to: elem.attr('data-to')}, function (html) {
            elem.closest('.panel').find('.panel-body').html(html);
        });
    });

</script>

<style>
    
    .panel-collapse img {
        height: 80px;
    }
    
</style>