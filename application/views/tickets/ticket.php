Билет № <?= $ticket->ID ?>
<hr />
<table border="1" cellpadding="5">
    <tr>
        <td style="width: 40%"><?=lang('Route')?></td>
        <td style="width: 60%"><b><?= $route->CityFromName ?> - <?= $route->CityToName ?></b></td>
    </tr>
    <tr>
        <td style="width: 40%"><?=lang('Passenger')?></td>
        <td style="width: 60%"><b><?= $ticket->FirstName ?> <?= $ticket->LastName ?></b></td>
    </tr>
    <tr>
        <td style="width: 40%"><?=lang('Seat')?></td>
        <td style="width: 60%">№ <b><?= $ticket->Seat ?></b></td>
    </tr>
    <tr>
        <td style="width: 40%"><?=lang('Departure')?></td>
        <td style="width: 60%"><b><?= date('d.m.Y', strtotime($ticket->Date)) ?> <?= date('H:i', strtotime($from_station->Time)) ?></b></td>
    </tr>
    <tr>
        <td style="width: 40%"><?=lang('Arrival')?></td>
        <td style="width: 60%"><b><?= date('d.m.Y', strtotime($ticket->Date) + (($to_station->Day - 1) * 86400)) ?> <?= date('H:i', strtotime($to_station->Time)) ?></b></td>
    </tr>
    <tr>
        <td style="width: 40%"><?=lang('Transport')?></td>
        <td style="width: 60%"><b>Delfin</b></td>
    </tr>
</table>