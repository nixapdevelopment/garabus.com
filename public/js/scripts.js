
$(document).ready(function(){
    
    $('.footer h3').click(function() {
        $(this).next('.footer-nav').toggle();
    });
    $('body').on('click', '#si-traseu', function() {
        $(this).next('.si-traseu').toggle();
    });
    
    $('#subscribe-form').submit(function(event){
        event.preventDefault();
        
        var form = $(this);
        
        $.post('/main/subscribe', form.serialize(), function(html){
            form.append(html);
            form.trigger('reset');
        });
    });
    
    $('a.fancybox').fancybox();
    
    $('#slideshow').owlCarousel({
        singleItem : true
    });
    
    var owl = $("#search-slide");

    owl.owlCarousel({
        navigation: false, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        slideSpeed: 200,
        paginationSpeed: 800,
        autoPlay: true
    });
    
});

function applySelect2(elem, options = {})
{
    var defaults = {
        language: "ru",
        theme: "bootstrap"
    };
    
    $.extend(defaults, options);
    
    return $(elem).select2(defaults);
}

$.validator.setDefaults({
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {

    }
});

function applyLocationSelect(countrySelect, citySelect)
{
    $(countrySelect).on('change', function(){
        $.post('/' + SITE_LANG + '/main/ajaxCities', {country: $(this).val()}, function (options){
            $(citySelect).html(options);
            applySelect2($(citySelect));
        });
    });    
}

function applyTimeSelect(select)
{
    $(select).clockpicker({
        donetext: 'OK'
    });
}

function applyFullCitySelect(select)
{
    applySelect2(select, {
        minimumInputLength: 1,
        //tags: [],
        ajax: {
            url: '/' + SITE_LANG + '/main/ajax_cities',
            dataType: 'json',
            type: "GET",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.items, function (item) {
                        return {
                            text: item.text,
                            id: item.id
                        };
                    })
                };
            }
        }
    });
}