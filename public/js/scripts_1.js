$(document).ready(function () {

    var owl = $("#search-slide");

    owl.owlCarousel({
        navigation: false, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        slideSpeed: 200,
        paginationSpeed: 800,
        autoPlay: true
    });


});